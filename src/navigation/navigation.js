import React from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createDrawerNavigator } from "react-navigation-drawer";

// Auth
import LoginScreen from "root/src/containers/auth/login";
import AuthLoadingScreen from "root/src/containers/auth/loading";
import ProfileScreen from "root/src/containers/profile/profile";
import SignUpScreen from "root/src/containers/auth/signUp";

// Home
import HomeScreen from "root/src/containers/home/home";
import CalendarScreen from "root/src/containers/home/calendar";
import ExploreEventScreen from "root/src/containers/events/events";
import ExploreEventDetailsScreen from "root/src/containers/events/eventDetails";
import ExploreEventParticipantsScreen from "root/src/containers/events/eventParticipants";

// Settings
import SettingsScreen from "root/src/containers/settings/settings";

// Club
import ClubScreen from "root/src/containers/club/club";
import DashboardScreen from "root/src/containers/club/dashboard/dashboard";
import EventsScreen from "root/src/containers/club/events/events";
import EventsDetailScreen from "root/src/containers/club/events/eventDetails";
import AddEventScreen from "root/src/containers/club/events/addEvent";
import AddTaskScreen from "root/src/containers/club/events/addTask";
import SelectUserScreen from "root/src/containers/club/events/selectUser";
import staffClubScreen from "root/src/containers/club/staff/staffClub";

import MeetingScreen from "root/src/containers/club/meeting/meeting";
import AddMeetingScreen from "root/src/containers/club/meeting/addMeeting";
import TaskScreen from "root/src/containers/club/task/listTask";

// Club Profile
import ClubProfileScreen from "root/src/containers/clubProfile/clubProfile";

// Academics
import AcademicsScreen from "root/src/containers/academics/academics";
import TimetableScreen from "root/src/containers/academics/timetable/timetable";
import ProgressScreen from "root/src/containers/academics/progress/progress";
import AttendanceScreen from "root/src/containers/academics/attendance/attendance";

// ICheck-in
import ICheckInScreen from "root/src/containers/iCheckIn/iCheckIn";
import ICheckInStaffScreen from "root/src/containers/iCheckIn/iCheckInStaff";
import AnnouncementScreen from "root/src/containers/academics/announcements/announcements";

// Information
import InformationHomeScreen from "root/src/containers/information/information";
import InfoAccomadationScreen from "root/src/containers/information/student_services/infoAccomadation";
import InfoCareerScreen from "root/src/containers/information/student_services/infoCareer";
import InfoClubsScreen from "root/src/containers/information/student_services/infoClubs";
import InfoCounsellingScreen from "root/src/containers/information/student_services/infoCounselling";
import InfoInternationalScreen from "root/src/containers/information/student_services/infoInternational";
import InfoScholarshipScreen from "root/src/containers/information/student_services/infoScholarship";

import InfoFinanceScreen from "root/src/containers/information/uni_supp_services/infoFinance";
import InfoHealthScreen from "root/src/containers/information/uni_supp_services/infoHealth";
import InfoITScreen from "root/src/containers/information/uni_supp_services/infoIT";
import InfoLibraryScreen from "root/src/containers/information/uni_supp_services/infoLibrary";
import InfoSecurityScreen from "root/src/containers/information/uni_supp_services/infoSecurity";

import FaciAcademicsScreen from "root/src/containers/information/facilities/faciAcademics";
import FaciCafeScreen from "root/src/containers/information/facilities/faciCafe";
import FaciEmergencyScreen from "root/src/containers/information/facilities/faciEmergency";
import FaciOfficesScreen from "root/src/containers/information/facilities/faciOffices";
import FaciServicesScreen from "root/src/containers/information/facilities/faciServices";
import FaciSportsScreen from "root/src/containers/information/facilities/faciSports";

import StaffCAEScreen from "root/src/containers/information/staff_directories/staffCAE";
import StaffCELSScreen from "root/src/containers/information/staff_directories/staffCELS";
import StaffSHMSScreen from "root/src/containers/information/staff_directories/staffSHMS";
import StaffSMSScreen from "root/src/containers/information/staff_directories/staffSMS";
import StaffSOAScreen from "root/src/containers/information/staff_directories/staffSOA";
import StaffSOHScreen from "root/src/containers/information/staff_directories/staffSOH";
import StaffSSTScreen from "root/src/containers/information/staff_directories/staffSST";
import StaffSUBSScreen from "root/src/containers/information/staff_directories/staffSUBS";

// Notifications
import NotificationScreen from "root/src/containers/notification/listNotification";

// custom
import CustomTabBar from "./components/customTabBar";
import CustomDrawer from "./components/customDrawer";

// icons
import { Feather } from "@expo/vector-icons";

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: {
        screen: createDrawerNavigator(
          {
            Home: {
              screen: createBottomTabNavigator(
                {
                  Home: {
                    screen: HomeScreen,
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather name="home" color={tintColor} size={20} />
                      )
                    })
                  },
                  Notifications: {
                    screen: NotificationScreen,
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather name="bell" color={tintColor} size={20} />
                      )
                    })
                  },
                  Calendar: {
                    screen: CalendarScreen,
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather name="calendar" color={tintColor} size={20} />
                      )
                    })
                  },
                  Events: {
                    screen: HomeScreen,
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather name="flag" color={tintColor} size={20} />
                      )
                    })
                  }
                },
                {
                  initialRouteName: "Home",
                  tabBarComponent: CustomTabBar
                }
              ),
              navigationOptions: () => ({
                drawerIcon: ({ tintColor }) => (
                  <Feather name="home" color={tintColor} size={20} />
                )
              })
            },
            "Club Management": {
              screen: createBottomTabNavigator(
                {
                  Club: {
                    screen: ClubProfileScreen,
                    navigationOptions: () => ({
                      tabBarLabel: "Club",
                      tabBarIcon: ({ tintColor }) => (
                        <Feather name="grid" color={tintColor} size={20} />
                      )
                    })
                  },
                  Dashboard: {
                    screen: DashboardScreen,
                    navigationOptions: () => ({
                      tabBarLabel: "Dashboard",
                      tabBarIcon: ({ tintColor }) => (
                        <Feather name="grid" color={tintColor} size={20} />
                      )
                    })
                  },
                  Meetings: {
                    screen: createStackNavigator(
                      {
                        MeetingList: MeetingScreen,
                        AddMeeting: AddMeetingScreen
                      },
                      {
                        initialRouteName: "MeetingList",
                        defaultNavigationOptions: {
                          header: null
                        }
                      }
                    ),
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather name="users" color={tintColor} size={20} />
                      )
                    })
                  },
                  Tasks: {
                    screen: TaskScreen,
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather name="clipboard" color={tintColor} size={20} />
                      )
                    })
                  },
                  Events: {
                    screen: createStackNavigator(
                      {
                        EventList: EventsScreen,
                        AddEvent: AddEventScreen,
                        EventDetail: EventsDetailScreen,
                        AddTask: AddTaskScreen,
                        SelectUser: SelectUserScreen
                      },
                      {
                        initialRouteName: "EventList",
                        defaultNavigationOptions: {
                          header: null
                        }
                      }
                    ),
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather name="flag" color={tintColor} size={20} />
                      )
                    })
                  },
                  staffClub: {
                    screen: staffClubScreen,
                    navigationOptions: () => ({
                      tabBarLabel: "Staff",
                      tabBarIcon: ({ tintColor }) => (
                        <Feather name="anchor" color={tintColor} size={20} />
                      )
                    })
                  }
                },
                {
                  initialRouteName: "Dashboard",
                  tabBarComponent: CustomTabBar,
                  tabBarOptions: {
                    activeTintColor: "#414853"
                  }
                }
              ),
              navigationOptions: () => ({
                drawerIcon: ({ tintColor }) => (
                  <Feather name="grid" color={tintColor} size={20} />
                )
              })
            },
            Information: {
              screen: createStackNavigator(
                {
                  InformationHome: InformationHomeScreen,
                  InfoAccomadation: InfoAccomadationScreen,
                  InfoCareer: InfoCareerScreen,
                  InfoClubs: {
                    screen: createStackNavigator(
                      {
                        InfoClubs: InfoClubsScreen,
                        ClubProfile: ClubProfileScreen
                      },
                      {
                        initialRouteName: "InfoClubs",
                        defaultNavigationOptions: {
                          header: null
                        }
                      }
                    )
                  },
                  InfoCounselling: InfoCounsellingScreen,
                  InfoInternational: InfoInternationalScreen,
                  InfoScholarship: InfoScholarshipScreen,
                  InfoFinance: InfoFinanceScreen,
                  InfoHealth: InfoHealthScreen,
                  InfoIT: InfoITScreen,
                  InfoLibrary: InfoLibraryScreen,
                  InfoSecurity: InfoSecurityScreen,
                  FaciAcademics: FaciAcademicsScreen,
                  FaciCafe: FaciCafeScreen,
                  FaciEmergency: FaciEmergencyScreen,
                  FaciOffices: FaciOfficesScreen,
                  FaciServices: FaciServicesScreen,
                  FaciSports: FaciSportsScreen,
                  StaffCAE: StaffCAEScreen,
                  StaffCELS: StaffCELSScreen,
                  StaffSHMS: StaffSHMSScreen,
                  StaffSMS: StaffSMSScreen,
                  StaffSOA: StaffSOAScreen,
                  StaffSOH: StaffSOHScreen,
                  StaffSST: StaffSSTScreen,
                  StaffSUBS: StaffSUBSScreen
                },
                {
                  initialRouteName: "InformationHome",
                  defaultNavigationOptions: {
                    header: null
                  }
                }
              ),
              navigationOptions: () => ({
                drawerIcon: ({ tintColor }) => (
                  <Feather name="info" color={tintColor} size={20} />
                )
              })
            },
            Events: {
              screen: createStackNavigator(
                {
                  Events: ExploreEventScreen,
                  "Event Details": ExploreEventDetailsScreen,
                  "Event Participants": ExploreEventParticipantsScreen
                },
                {
                  initialRouteName: "Events",
                  defaultNavigationOptions: {
                    header: null
                  }
                }
              ),
              navigationOptions: () => ({
                drawerIcon: ({ tintColor }) => (
                  <Feather name="calendar" color={tintColor} size={20} />
                )
              })
            },
            Settings: {
              screen: SettingsScreen,
              navigationOptions: () => ({
                drawerIcon: ({ tintColor }) => (
                  <Feather name="settings" color={tintColor} size={20} />
                )
              })
            },
            Academics: {
              screen: createBottomTabNavigator(
                {
                  Announcement: {
                    screen: AnnouncementScreen,
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather name="mic" color={tintColor} size={20} />
                      )
                    })
                  },
                  Timetable: {
                    screen: TimetableScreen,
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather name="calendar" color={tintColor} size={20} />
                      )
                    })
                  },
                  Attendance: {
                    screen: AttendanceScreen,
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather
                          name="check-circle"
                          color={tintColor}
                          size={20}
                        />
                      )
                    })
                  },
                  Progress: {
                    screen: ProgressScreen,
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather
                          name="bar-chart-2"
                          color={tintColor}
                          size={20}
                        />
                      )
                    })
                  }
                },
                {
                  initialRouteName: "Progress",
                  tabBarComponent: CustomTabBar
                }
              ),
              navigationOptions: () => ({
                drawerIcon: ({ tintColor }) => (
                  <Feather name="book" color={tintColor} size={20} />
                )
              })
            },
            Profile: {
              screen: ProfileScreen,
              navigationOptions: () => ({
                drawerIcon: ({ tintColor }) => (
                  <Feather name="user" color={tintColor} size={20} />
                )
              })
            },
            "iCheck-in": {
              screen: createBottomTabNavigator(
                {
                  iCheckIn: {
                    screen: ICheckInScreen,
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather
                          name="check-square"
                          color={tintColor}
                          size={20}
                        />
                      )
                    })
                  },
                  iCheckInStaff: {
                    screen: ICheckInStaffScreen,
                    navigationOptions: () => ({
                      tabBarIcon: ({ tintColor }) => (
                        <Feather
                          name="check-square"
                          color={tintColor}
                          size={20}
                        />
                      )
                    })
                  }
                },
                {
                  initialRouteName: "iCheckInStaff",
                  tabBarComponent: CustomTabBar
                }
              ),
              navigationOptions: () => ({
                drawerIcon: ({ tintColor }) => (
                  <Feather name="check-square" color={tintColor} size={20} />
                )
              })
            }
          },
          {
            contentComponent: CustomDrawer,
            initialRouteName: "iCheck-in",
            contentOptions: {
              labelStyle: {
                fontFamily: "roboto-slab-bold"
              }
            }
          }
        )
      },
      Auth: {
        screen: createStackNavigator(
          { Login: LoginScreen, SignUp: SignUpScreen },
          {
            defaultNavigationOptions: {
              header: null
            }
          }
        )
      }
    },
    {
      initialRouteName: "App"
    }
  )
);
