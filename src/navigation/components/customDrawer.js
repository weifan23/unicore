import { DrawerItems } from "react-navigation-drawer";
import React from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  Alert
} from "react-native";
import { useSelector } from "react-redux";
// import { ScrollView } from "root/src/styles/components";
import { Feather } from "@expo/vector-icons";
import { Separator } from "../../styles/components";

const logos = [
  {
    path: require("root/src/assets/img/logo/lg-black.png")
  },
  {
    path: require("root/src/assets/img/logo/lg-white.png")
  }
];

const customTabBar = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  const logout = () => {
    Alert.alert(
      "Logout",
      "Are you sure?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => props.navigation.navigate("Auth") }
      ],
      { cancelable: false }
    );
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: theme.mode === "light" ? "#F1F1F1" : "#363D48",
        paddingBottom: 30
      }}
    >
      <ScrollView
        style={{
          backgroundColor: theme.mode === "light" ? "#F1F1F1" : "#363D48",
          paddingTop: 20
        }}
      >
        <Image
          style={{ width: 250, height: 150 }}
          source={theme.mode === "light" ? logos[0].path : logos[1].path}
        />
        <DrawerItems
          activeTintColor={theme.mode === "light" ? "#303A49" : "#F9FBFF"}
          inactiveTintColor="#939393"
          {...props}
          labelStyle={{
            fontFamily: "roboto-slab-bold",

            color: theme.mode === "light" ? "#303A49" : "#F1F1F1"
          }}
        />
        <Separator />
        <Separator />
      </ScrollView>
      <TouchableOpacity
        style={{
          paddingLeft: 20,
          flexDirection: "row",
          alignItems: "center"
        }}
        onPress={() => logout()}
      >
        <Feather
          name="log-out"
          color={theme.mode === "light" ? "#303A49" : "#F9FBFF"}
          size={20}
        />

        <Text
          style={{
            color: theme.mode === "light" ? "#303A49" : "#F1F1F1",
            fontFamily: "roboto-slab-bold",
            marginLeft: 30
          }}
        >
          Logout
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default customTabBar;
