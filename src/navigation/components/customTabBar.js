import React from "react";
import { View, Text } from "react-native";
import { useSelector } from "react-redux";
import { BottomTabBar } from "react-navigation-tabs";

const customTabBar = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  return (
    <BottomTabBar
      {...props}
      activeTintColor={theme.mode === "light" ? "#303A49" : "#F9FBFF"}
      inactiveTintColor="#939393"
      labelStyle={{
        fontFamily: "roboto-slab-bold",
        margin: 5
      }}
      style={{
        backgroundColor: theme.mode === "light" ? "#F9FBFF" : "#363D48",
        borderTopWidth: 0,
        paddingTop: 5,
        shadowColor: "rgba(0, 0, 0, 0.5)",
        shadowOffset: {
          width: 0,
          height: 7
        },
        shadowOpacity: 0.43,
        shadowRadius: 15,

        elevation: 15
      }}
    />
  );
};

export default customTabBar;
