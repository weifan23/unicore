import styled from "styled-components/native";
import { space, layout, typography, color } from "styled-system";
import React from "react";
import { View } from "react-native";

export const ScrollView = styled.ScrollView`
  flex: 1;
  background-color: ${props => props.theme.PRIMARY_BACKGROUND_COLOR};
  padding: 20px 20px;
`;

export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.PRIMARY_BACKGROUND_COLOR};
`;

export const Separator = styled.View`
  padding: 20px;
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Col = styled.View`
  flex-direction: column;
  align-items: center;
`;

// typography
export const H1 = styled.Text`
  color: ${props => props.theme.PRIMARY_TEXT_COLOR};
  font-size: 32px;
  font-family: roboto-slab-bold;
  ${color}
`;
export const H2 = styled.Text`
  color: ${props => props.theme.PRIMARY_TEXT_COLOR};
  font-size: 20px;
  font-family: roboto-slab-bold;
`;

export const H3 = styled.Text`
  color: ${props => props.theme.PRIMARY_TEXT_COLOR};
  font-size: 20px;
  font-family: roboto-slab-bold;
`;

export const H4 = styled.Text`
  color: ${props => props.theme.PRIMARY_TEXT_COLOR};
  font-size: 16px;
  font-family: roboto-slab-bold;
`;

export const S1 = styled.Text`
  color: ${props => props.theme.SECONDARY_TEXT_COLOR};
  font-size: 12px;
  font-family: roboto-bold;
`;

export const S2 = styled.Text`
  color: ${props => props.theme.SECONDARY_TEXT_COLOR};
  font-size: 10px;
  font-family: roboto-bold;
`;

export const P1 = styled.Text`
  color: ${props => props.theme.SECONDARY_TEXT_COLOR};
  font-size: 12px;
  font-family: roboto;
`;

export const P2 = styled.Text`
  color: ${props => props.theme.SECONDARY_TEXT_COLOR};
  font-size: 10px;
  font-family: roboto;
`;

// button

export const PreButton = styled.TouchableOpacity`
  margin-vertical: 10px;
  border-width: 2px;
  border-color: ${props => props.theme.PRIMARY_BUTTON_COLOR};
  background-color: ${props => props.theme.PRIMARY_BUTTON_COLOR};
  border-radius: 8px;
  padding: 8px;
  align-items: center;
  ${layout}
`;

export const Button = props => {
  return (
    <PreButton {...props}>
      <ButtonText>{props.children}</ButtonText>
    </PreButton>
  );
};

export const PreButtonGhost = styled.TouchableOpacity`
  margin-vertical: 10px;
  border-width: 2px;
  border-color: rgba(0, 0, 0, 0);
  background-color: rgba(0, 0, 0, 0);
  border-radius: 8px;
  padding: 8px;
  align-items: center;
  ${space}
  ${layout}
`;

export const ButtonGhost = props => {
  return (
    <PreButtonGhost {...props}>
      <OutlineButtonText>{props.children}</OutlineButtonText>
    </PreButtonGhost>
  );
};

export const PreButtonSuccess = styled.TouchableOpacity`
  margin-vertical: 10px;
  border-width: 2px;
  border-color: #baebc2;
  background-color: #baebc2;
  border-radius: 8px;
  padding: 8px;
  align-items: center;
  ${space}
  ${layout}
`;

export const ButtonSuccess = props => {
  return (
    <PreButtonSuccess {...props}>
      <DarkButtonText>{props.children}</DarkButtonText>
    </PreButtonSuccess>
  );
};

export const PreButtonDanger = styled.TouchableOpacity`
  margin-vertical: 10px;
  border-width: 2px;
  border-color: #ff7c7c;
  background-color: #ff7c7c;
  border-radius: 8px;
  padding: 8px;
  align-items: center;
  ${space}
  ${layout}
`;

export const ButtonDanger = props => {
  return (
    <PreButtonDanger {...props}>
      <DarkButtonText>{props.children}</DarkButtonText>
    </PreButtonDanger>
  );
};

export const PreButtonOutline = styled.TouchableOpacity`
  margin-vertical: 10px;
  border-width: 2px;
  border-color: ${props => props.theme.PRIMARY_BUTTON_COLOR};
  background-color: rgba(0, 0, 0, 0);
  border-radius: 8px;
  padding: 8px;
  align-items: center;
  ${space}
  ${layout}
`;

export const ButtonOutline = props => {
  return (
    <PreButtonOutline {...props}>
      <OutlineButtonText>{props.children}</OutlineButtonText>
    </PreButtonOutline>
  );
};

export const PreInput = styled.TextInput`
  background-color: ${props => props.theme.INPUT_COLOR};
  font-family: roboto-slab-bold;
  font-size: 10px;
  color: ${props => props.theme.PRIMARY_TEXT_COLOR};
  padding: 8px;
  border-radius: 8px;
  margin-vertical: 10px;
  padding-left: 20px;

  align-items: center;
  ${space}
  ${layout}
`;

export const Input = props => {
  return <PreInput placeholderTextColor="rgba(100,100,100,0.5)" {...props} />;
};

export const PreLargeInput = styled.TextInput`
  background-color: ${props => props.theme.INPUT_COLOR};
  font-family: roboto-slab-bold;
  color: ${props => props.theme.PRIMARY_TEXT_COLOR};
  font-size: 16px;
  padding: 8px;
  padding-left: 20px;
  border-radius: 8px;
  padding-left: 20px;

  margin-vertical: 10px;
  align-items: center;
  ${space}
  ${layout}
`;

export const LargeInput = props => {
  return (
    <PreLargeInput placeholderTextColor="rgba(100,100,100,0.5)" {...props} />
  );
};

// Button Text
export const ButtonText = styled.Text`
  color: ${props => props.theme.BUTTON_TEXT_COLOR};
  font-family: roboto-slab-bold;
  font-size: 12px;
`;

export const LightButtonText = styled.Text`
  color: ${props => props.theme.LIGHT_BUTTON_TEXT_COLOR};
  font-family: roboto-slab-bold;
  font-size: 12px;
`;

export const DarkButtonText = styled.Text`
  color: ${props => props.theme.DARK_BUTTON_TEXT_COLOR};
  font-family: roboto-slab-bold;
  font-size: 12px;
`;

export const OutlineButtonText = styled.Text`
  color: ${props => props.theme.OUTLINE_BUTTON_TEXT_COLOR};
  font-family: roboto-slab-bold;
  font-size: 12px;
`;

export const Card = styled.View`
  background-color: ${props =>
    props.theme.mode === "light" ? "rgba(0, 0, 0, 0.08)" : "rgba(0, 0, 0, 0.3)"}
  border-radius: 10px;
`;

export const ErrorMessage = styled.Text`
  color: #ff7c7c;
  font-size: 12px;
  font-family: roboto;
`;

export const HorizontalLine = props => {
  const backgroundColor = props.color || "black";
  const height = props.height || 1;
  const opacity = props.opacity || 1;
  const width = props.width || "100%";
  return (
    <View
      style={{
        backgroundColor: backgroundColor,
        height: height,
        opacity: opacity,
        width: width,
        alignSelf: "center"
      }}
    />
  );
};

export const HorizontalLineSeparator = props => {
  const backgroundColor = props.color || "black";
  const height = props.height || 1;
  const opacity = props.opacity || 1;
  const width = props.width || "100%";
  const margin = props.margin || 10;

  return (
    <View
      style={{
        backgroundColor: backgroundColor,
        height: height,
        opacity: opacity,
        width: width,
        alignSelf: "center",
        marginVertical: margin
      }}
    />
  );
};
