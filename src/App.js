import React, { useEffect, useState } from "react";
import { StatusBar } from "react-native";
import { ThemeProvider } from "styled-components/native";
import { useSelector } from "react-redux";

import * as Font from "expo-font";

import Navigation from "./navigation/navigation";

export default function App() {
  useEffect(() => {
    loadFont();
  });
  const [fontLoaded, setLoadingFont] = useState(true);

  async function loadFont() {
    await Font.loadAsync({
      "roboto-slab-bold": require("./assets/fonts/RobotoSlab-Bold.ttf"),
      "roboto-bold": require("./assets/fonts/Roboto-Bold.ttf"),
      roboto: require("./assets/fonts/Roboto-Regular.ttf"),
      "roboto-italic": require("./assets/fonts/Roboto-Italic.ttf")
    });
    setLoadingFont(false);
  }
  const theme = useSelector(state => state.themeReducer.theme);
  return (
    <ThemeProvider theme={theme}>
      <StatusBar
        backgroundColor={theme.PRIMARY_BACKGROUND_COLOR}
        barStyle={theme.mode === "light" ? "dark-content" : "light-content"}
      />
      {!fontLoaded && <Navigation />}
    </ThemeProvider>
  );
}
