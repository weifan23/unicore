import React, { Fragment } from "react";
import { View, StyleSheet, ScrollView, Dimensions } from "react-native";
import { useSelector } from "react-redux";
import {
  Container,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import moment from "moment";
import { EventListItem } from "../club/events/components/eventListItem";

const data = [
  {
    id: "2111",
    image: require("root/src/assets/img/temp/poster2.png"),
    name: "Hacktoberfest 2019",
    date: "29/12/2019",
    time: "18:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: false,
    club: "Sunway Tech Club"
  },
  {
    id: "232",
    image: require("root/src/assets/img/temp/poster3.png"),
    name: "Career Tech Talk",
    date: "30/12/2019",
    time: "18:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: true,
    club: "Sunway Tech Club"
  },
  {
    id: "323",
    image: require("root/src/assets/img/temp/poster2.png"),
    name: "React Workshop",
    date: "30/1/2020",
    time: "18:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: false,
    club: "Sunway Tech Club"
  },
  {
    id: "325",
    image: require("root/src/assets/img/temp/poster.png"),
    name: "WebLaunch 2019",
    date: "25/12/2019",
    time: "18:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: true,
    club: "Sunway Tech Club"
  },
  {
    id: "125",
    image: require("root/src/assets/img/temp/poster2.png"),
    name: "Android Workshop",
    date: "31/12/2019",
    time: "18:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: false,
    club: "Sunway Tech Club"
  }
];
const club = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const styles = StyleSheet.create({
    height: { height: Dimensions.get("window").height - 205 }
  });
  return (
    <ScrollView nestedScrollEnabled={true}>
      {data
        .sort(
          (a, b) => moment(a.date, "DD/MM/YYYY") - moment(b.date, "DD/MM/YYYY")
        )
        .map(item => EventListItem(item, theme, props))}
    </ScrollView>
  );
};

export default club;
