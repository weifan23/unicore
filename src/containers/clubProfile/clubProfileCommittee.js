import React from "react";
import { View, ScrollView, StyleSheet, Dimensions } from "react-native";
import { useSelector } from "react-redux";
import {
  Container,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";

const data = [
  {
    title: "President",
    name: "Brocco Lee"
  },
  {
    title: "Vice President",
    name: "Ram Lee"
  },
  {
    title: "Secretary",
    name: "Ken Lee"
  },
  {
    title: "Treasurer",
    name: "Sudden Lee"
  },
  {
    title: "Head of Fish",
    name: "Bear Lee"
  },
  {
    title: "Head of Tortilla",
    name: "Lone Lee"
  },
  {
    title: "Head or Salsa",
    name: "Fela Lee"
  }
];

const club = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const styles = StyleSheet.create({
    height: {
      height: Dimensions.get("window").height - 205
    },
    titleStyle: {
      fontSize: 16,
      paddingLeft: 10
    },
    nameStyle: {
      fontSize: 16,
      paddingTop: 3,
      paddingBottom: 15,
      paddingLeft: 25
    }
  });
  return (
    <ScrollView nestedScrollEnabled={true}>
      {data.map(item => (
        <View key={item.title}>
          <S1 style={styles.titleStyle}>{item.title}</S1>
          <P1 style={styles.nameStyle}>{item.name}</P1>
        </View>
      ))}
    </ScrollView>
  );
};

export default club;
