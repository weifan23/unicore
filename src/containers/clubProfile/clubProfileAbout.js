import React, { Fragment } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import { View, StyleSheet, Dimensions, ScrollView } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import {
  Container,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import { Feather } from "@expo/vector-icons";

const data = [
  {
    isEmpty: false,
    label: "Overview",
    icon: "home",
    content:
      "Fish Taco Club is a club that feeds everyone with free fish taco everyday"
  },
  {
    isEmpty: false,
    label: "Mission",
    icon: "target",
    content:
      "Fish Taco Club aims to eliminate all tacos that is not fish filled"
  },
  {
    isEmpty: false,
    label: "Vision",
    icon: "search",
    content: "Mankind will only eat fish taco in the future"
  },
  {
    isEmpty: false,
    label: "Founding Year",
    icon: "flag",
    content: "Since taco is invented"
  },
  {
    isEmpty: false,
    label: "Club Advisor",
    icon: "user",
    content: "Taco God"
  },
  {
    isEmpty: false,
    label: "Email",
    icon: "mail",
    content: "fishtacodabest@taco.com"
  },
  {
    isEmpty: false,
    label: "Website",
    icon: "globe",
    content: "www.youtube.com"
  }
];

const club = props => {
  const styles = StyleSheet.create({
    height: {
      height: Dimensions.get("window").height - 205
    },
    aboutTitleStyle: {
      fontSize: 16,
      paddingLeft: 10
    },
    aboutContentStyle: {
      fontSize: 14,
      paddingTop: 3,
      paddingBottom: 15,
      paddingLeft: 25
    }
  });
  const theme = useSelector(state => state.themeReducer.theme);
  return (
    <ScrollView nestedScrollEnabled={true}>
      {data.map(item =>
        item.isEmpty ? (
          <Fragment />
        ) : (
          <View key={item.label}>
            <Row>
              <Feather
                name={item.icon}
                color={theme.PRIMARY_TEXT_COLOR}
                size={16}
              />
              <S1 style={styles.aboutTitleStyle}>{item.label}</S1>
            </Row>
            <P1 style={styles.aboutContentStyle}>{item.content}</P1>
          </View>
        )
      )}
    </ScrollView>
  );
};

export default club;
