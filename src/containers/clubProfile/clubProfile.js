import React, { useState } from "react";
import { Image, View, Dimensions, StyleSheet } from "react-native";
import { useSelector } from "react-redux";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { ClubProfileStack } from "./component/clubProfileStack";

const data = {
  clubProfilePic:
    "https://logopond.com/logos/e981dec654f6bf182dd4fcf707901508.png",
  clubName: "Fish Taco Club",
  activityDone: 20,
  member: 30,
  clubCategory: "Food & Beverage",
  isFollow: true
};

const club = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const clubHeaderHeight = 210;
  const [isFollow, setIsFollow] = useState(data.isFollow);
  const [scrollable, setScrollable] = useState(true);

  const styles = StyleSheet.create({
    height: {
      height: Dimensions.get("window").height - 140
    },
    innerScrollViewStyle: {
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
      paddingTop: 0
    },
    clubHeaderInnerTopContainerStyle: {
      justifyContent: "space-around",
      marginBottom: 10
    },
    clubHeaderContainerStyle: { height: clubHeaderHeight },
    clubPhotoStyle: {
      width: 100,
      height: 100,
      borderRadius: 100,
      borderColor: theme.PRIMARY_TEXT_COLOR,
      borderWidth: 1
    },
    followButtonContainerStyle: {
      alignSelf: "center"
    }
  });
  const isCloseToBottom = ({
    layoutMeasurement,
    contentOffset,
    contentSize
  }) => {
    const paddingToBottom = 0;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  const FollowButton = ({ _isFollow, _setIsFollow }) => {
    if (_isFollow) {
      return (
        <Button width={1 / 4} onPress={() => _setIsFollow(false)}>
          Following
        </Button>
      );
    } else {
      return (
        <ButtonOutline width={1 / 4} onPress={() => _setIsFollow(true)}>
          Follow
        </ButtonOutline>
      );
    }
  };

  return (
    <Container>
      <Header title="Home" nav={props.navigation} />

      <ScrollView
        scrollEnabled={scrollable}
        // onScroll={({ nativeEvent }) => {
        //   if (isCloseToBottom(nativeEvent)) {
        //     setScrollable(false);
        //   } else {
        //     setScrollable(true);
        //   }
        // }}
      >
        <View style={styles.clubHeaderContainerStyle}>
          <Row style={styles.clubHeaderInnerTopContainerStyle}>
            <Col>
              <H1>{data.activityDone}</H1>
              <S1>Activities</S1>
            </Col>
            <Image
              style={styles.clubPhotoStyle}
              source={{ uri: data.clubProfilePic }}
            />
            <Col>
              <H1>{data.member}</H1>
              <S1>Members</S1>
            </Col>
          </Row>

          <Col>
            <H2>{data.clubName}</H2>
            <P1>{data.clubCategory}</P1>
          </Col>
          <Row style={styles.followButtonContainerStyle}>
            <FollowButton
              _isFollow={isFollow}
              _setIsFollow={event => setIsFollow(event)}
            />
          </Row>
        </View>

        <View style={styles.height}>
          <ClubProfileStack />
        </View>
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default club;
