import React, { useState } from "react";
import { Image, View, StyleSheet } from "react-native";
import { useSelector } from "react-redux";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";

// navigation
import { createAppContainer } from "react-navigation";
import {
  createMaterialTopTabNavigator,
  MaterialTopTabBar
} from "react-navigation-tabs";

import ClubProfileCommitteeScreen from "root/src/containers/clubProfile/clubProfileCommittee";
import ClubProfileAboutScreen from "root/src/containers/clubProfile/clubProfileAbout";
import ClubProfileEventScreen from "root/src/containers/clubProfile/clubProfileEvent";
import { paddingBottom } from "styled-system";

const data = {
  clubProfilePic:
    "https://logopond.com/logos/e981dec654f6bf182dd4fcf707901508.png",
  clubName: "Fish Taco Club",
  activityDone: 20,
  member: 30,
  clubCategory: "Food & Beverage",
  isFollow: true
};

const customTabBar = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  return (
    <MaterialTopTabBar
      {...props}
      activeTintColor={theme.PRIMARY_TEXT_COLOR}
      inactiveTintColor={"#939393"}
      labelStyle={{
        fontFamily: "roboto-slab-bold"
      }}
      style={{
        backgroundColor: "transparent",
        marginBottom: 15
      }}
      indicatorStyle={{
        backgroundColor: theme.PRIMARY_TEXT_COLOR
      }}
    />
  );
};

export const ClubProfileStack = createAppContainer(
  createMaterialTopTabNavigator(
    {
      About: ClubProfileAboutScreen,
      Event: ClubProfileEventScreen,
      Committee: ClubProfileCommitteeScreen
    },
    {
      initialRouteName: "About",
      tabBarComponent: customTabBar
    }
  )
);
