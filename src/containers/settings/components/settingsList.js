import React, { useState } from "react";
import {
  Container,
  ScrollView,
  PreButton,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  Card
} from "root/src/styles/components";
import { View, StyleSheet } from "react-native";
import styled from "styled-components/native";
import { useSelector, useDispatch } from "react-redux";
import { Feather } from "@expo/vector-icons";

const settingsList = props => {
  const [listItems, setListItems] = useState([
    {
      id: 1,
      name: "Notifications",
      desc: "Block, allow, prioritise",
      icon: "bell"
    },
    {
      id: 2,
      name: "Activity log",
      desc: "View your app usage, Manage information",
      icon: "activity"
    },
    {
      id: 3,
      name: "Location",
      desc: "Enable location access, Location history",
      icon: "map-pin"
    },
    {
      id: 4,
      name: "Help",
      desc: "Report a problem, Help Centre, Privacy and security help",
      icon: "help-circle"
    },
    {
      id: 5,
      name: "Updates",
      desc: "Download updates, Last update, Auto update",
      icon: "clock"
    }
  ]);

  const theme = useSelector(state => state.themeReducer.theme);

  const styles = StyleSheet.create({
    button: {
      opacity: 0.8,
      flex: 1,
      backgroundColor: theme.mode === "light" ? "#eee" : "rgba(0, 0, 0, 0.08)",
      borderColor: theme.mode === "light" ? "#eee" : "rgba(0, 0, 0, 0.08)",
      flexDirection: "row-reverse",
      marginVertical: 0,
      paddingTop: 12,
      paddingBottom: 12
    },
    icon: {
      paddingHorizontal: 5
    },
    desc: {
      flex: 1,
      paddingLeft: 5
    },
    card: {
      flexDirection: "row",
      marginBottom: 15,
      backgroundColor: theme.mode === "light" ? "#eee" : "rgba(0, 0, 0, 0.08)"
    }
  });

  function Item({ name, desc, icon }) {
    return (
      <Card style={styles.card}>
        <PreButton style={styles.button}>
          <View style={styles.desc}>
            <H4>{name}</H4>
            <P2>{desc}</P2>
          </View>
          <View style={styles.icon}>
            <Feather name={icon} color={theme.PRIMARY_TEXT_COLOR} size={20} />
          </View>
        </PreButton>
      </Card>
    );
  }

  return (
    <View>
      {listItems.map(item => (
        <Item
          key={item.id}
          name={item.name}
          desc={item.desc}
          icon={item.icon}
        />
      ))}
    </View>
  );
};

export default settingsList;
