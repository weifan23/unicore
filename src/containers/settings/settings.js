import React from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import SettingItems from "./components/settingsList";
import { View } from "react-native";

const settings = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const dispatch = useDispatch();
  return (
    <Container>
      <Header title="Settings" nav={props.navigation} />

      <ScrollView>
        <SettingItems />
        <View style={{ alignItems: "center" }}>
          <Button width={1 / 4}>Log Out</Button>
        </View>
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default settings;
