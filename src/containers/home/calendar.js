import React from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import Calendar from "./components/calendar";

const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const dispatch = useDispatch();
  return (
    <Container>
      <Header title="Calendar" nav={props.navigation} />
      <ScrollView>
        <Calendar />
      </ScrollView>
    </Container>
  );
};

export default login;
