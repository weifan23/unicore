import React, { useState } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Col,
  Row,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import { Image, View, StyleSheet } from "react-native";
import FlatItems from "./components/profileFlat";
import styled from "styled-components/native";
import Header from "root/src/components/header/header";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import { Feather } from "@expo/vector-icons";
import { useSelector, useDispatch } from "react-redux";

const data = {
  userName: "Ding Nick Hong",
  userCourse: "BSc. Software Engineering",
  userProfilePic: "default"
};

const ImageUploadButton = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 175px;
`;

const styles = StyleSheet.create({
  photo: {
    borderRadius: 100,
    opacity: 0.5,
    backgroundColor: "grey",
    height: 150,
    width: 150,
    alignItems: "center",
    justifyContent: "center"
  },
  image: {
    width: 300,
    height: 200
  }
});

const profile = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const [photoUri, setPhotoUri] = useState("");

  const uploadPicture = async () => {
    const status = await getPermissionAsync();
    if (status === "granted") {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1
      });
      setPhotoUri(result.uri);
    }
  };

  const getPermissionAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== "granted") {
      alert(
        "Please go to Privacy Settings to Allow Permission for Photo Album."
      );
      return status;
    }
    return "granted";
  };

  return (
    <Container>
      <Header title="Profile" nav={props.navigation} />
      <ScrollView>
        <ImageUploadButton onLongPress={() => uploadPicture()}>
          {photoUri ? (
            <Image style={styles.image} source={{ uri: photoUri }} />
          ) : (
            <View style={styles.photo}>
              <Feather name="user" size={30} color={theme.BUTTON_COLOR} />
            </View>
          )}
        </ImageUploadButton>

        <Col>
          <H2>{data.userName}</H2>
          <S1>{data.userCourse}</S1>
        </Col>
        <Separator />

        <FlatItems theme={theme} />

        <Separator />
      </ScrollView>
    </Container>
  );
};

export default profile;
