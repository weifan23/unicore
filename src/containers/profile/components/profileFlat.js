import React from "react";
import { View, StyleSheet } from "react-native";
import {
  ScrollView,
  Container,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import { Feather } from "@expo/vector-icons";
import { useSelector } from "react-redux";

const data = [
  {
    id: 1,
    icon: "mail",
    label: "Email",
    content: "helloimstudent@imail.sunway.edu.my"
  },
  {
    id: 2,
    icon: "smartphone",
    label: "Phone number",
    content: "012-345678"
  },
  {
    id: 3,
    icon: "file-text",
    label: "Clubs Involvement",
    content: ["Sunway Tech Club", "Emo Kid Club", "Sunway Lagoon Club"]
  },
  {
    id: 4,
    icon: "calendar",
    label: "Events Attended",
    content: [
      "Far Far Beach Trip",
      "Near Near Lagoon Swim",
      "Tall Tall Building Forum",
      "Big Big House Party"
    ]
  }
];

const profileFlat = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  const styles = StyleSheet.create({
    profileTitleStyle: {
      fontSize: 16,
      paddingLeft: 10
    },
    profileContentTextStyle: {
      fontSize: 14,
      paddingTop: 3
    },
    profileContentContainerStyle: {
      paddingBottom: 15,
      paddingLeft: 25
    }
  });

  return (
    <ScrollView>
      {data.map(item => (
        <View>
          <Row key={item.id}>
            <Feather
              name={item.icon}
              size={16}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            <S1 style={styles.profileTitleStyle}>{item.label}</S1>
          </Row>
          <View style={styles.profileContentContainerStyle}>
            {Array.isArray(item.content) ? (
              item.content.map(content => (
                <P1 style={styles.profileContentTextStyle}>{content}</P1>
              ))
            ) : (
              <P1 style={styles.profileContentTextStyle}>{item.content}</P1>
            )}
          </View>
        </View>
      ))}
    </ScrollView>
  );
};

export default profileFlat;
