import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import moment from "moment";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  Card
} from "root/src/styles/components";
import { Feather } from "@expo/vector-icons";
import styled from "styled-components/native";
import { TouchableOpacity } from "react-native-gesture-handler";

const DataContainer = styled.View`
  width: 80%;
`;

const styles = StyleSheet.create({
  card: {
    marginBottom: 25,
    borderRadius: 15,
    flexShrink: 1
  },
  month: {
    marginBottom: -7
  },
  map: {
    marginTop: 3,
    marginLeft: 10,
    marginRight: 3
  },
  clock: {
    marginTop: 3,
    marginLeft: 10,
    marginRight: 3
  },
  calendar: {
    marginTop: 3,
    marginRight: 4
  },
  icons: {
    marginTop: 3
  },
  image: {
    height: 150,
    width: "100%",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  dataContainer: {
    padding: 10,
    justifyContent: "center"
  },
  details: {
    marginTop: 2,
    fontFamily: "roboto-italic"
  }
});

export const EventListItem = (item, theme, props) => {
  return (
    <Card style={styles.card}>
      <TouchableOpacity
        key={item.id}
        onPress={() =>
          props.navigation.navigate("Event Details", {
            item: item
          })
        }
      >
        <Image style={styles.image} source={item.image} />
        <Row style={styles.dataContainer}>
          <DataContainer>
            <H4>{item.name}</H4>

            <P1>{item.description}</P1>
            <Row style={styles.icons}>
              <Feather
                name="calendar"
                size={10}
                style={styles.calendar}
                color={theme.PRIMARY_TEXT_COLOR}
              />
              <P2 style={styles.details}>{item.date}</P2>
              <Feather
                name="clock"
                size={10}
                style={styles.clock}
                color={theme.PRIMARY_TEXT_COLOR}
              />
              <P2 style={styles.details}>
                {item.startTime} ~ {item.endTime}
              </P2>
              <Feather
                name="map-pin"
                size={10}
                style={styles.map}
                color={theme.PRIMARY_TEXT_COLOR}
              />
              <P2 style={styles.details}>{item.venue}</P2>
            </Row>
          </DataContainer>
        </Row>
      </TouchableOpacity>
    </Card>
  );
};
