import React from "react";
import moment from "moment";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H3,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import { EventListItem } from "./components/eventListItem";
import { View, Dimensions } from "react-native";
import Carousel from "react-native-snap-carousel";

const data = [
  {
    id: "2111",
    image: require("root/src/assets/img/temp/poster2.png"),
    name: "Hacktoberfest 2019",
    date: "29/12/2019",
    startTime: "18:00",
    endTime: "20:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: false,
    host: "Sunway Tech Club",
    venue: "Graduate Hall 2",
    address:
      "1, Jalan A, Jalan AAA, Jalan A, Jalan AAA, Jalan A, Jalan AAAA, Jalan A, Jalan AAA,",
    participants: [
      {
        id: "1",
        name: "Ding Dong",
        photo: null
      },
      {
        id: "2",
        name: "Ling Long",
        photo: null
      },
      {
        id: "3",
        name: "Ping Pong",
        photo: null
      },
      {
        id: "4",
        name: "King Kong",
        photo: null
      },
      {
        id: "5",
        name: "Sing Song",
        photo: null
      },
      {
        id: "6",
        name: "Bing Bong",
        photo: null
      },
      {
        id: "1",
        name: "Ding Dong",
        photo: null
      },
      {
        id: "2",
        name: "Ling Long",
        photo: null
      },
      {
        id: "3",
        name: "Ping Pong",
        photo: null
      },
      {
        id: "4",
        name: "King Kong",
        photo: null
      },
      {
        id: "5",
        name: "Sing Song",
        photo: null
      },
      {
        id: "6",
        name: "Bing Bong",
        photo: null
      }
    ]
  },
  {
    id: "232",
    image: require("root/src/assets/img/temp/poster3.png"),
    name: "Career Tech Talk",
    date: "30/12/2019",
    startTime: "18:00",
    endTime: "20:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: true,
    host: "Sunway Tech Club",
    venue: "Makerspace",
    participants: [
      {
        id: "1",
        name: "Ding Dong",
        photo: null
      },
      {
        id: "2",
        name: "Ling Long",
        photo: null
      },
      {
        id: "3",
        name: "Ping Pong",
        photo: null
      }
    ]
  },
  {
    id: "323",
    image: require("root/src/assets/img/temp/poster2.png"),
    name: "React Workshop",
    date: "30/1/2020",
    startTime: "18:00",
    endTime: "20:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: false,
    host: "Sunway Tech Club",
    venue: "Graduate Hall 1",
    participants: [
      {
        id: "1",
        name: "Ding Dong",
        photo: null
      },
      {
        id: "2",
        name: "Ling Long",
        photo: null
      },
      {
        id: "3",
        name: "Ping Pong",
        photo: null
      }
    ]
  },
  {
    id: "325",
    image: require("root/src/assets/img/temp/poster.png"),
    name: "WebLaunch 2019",
    date: "25/12/2019",
    startTime: "18:00",
    endTime: "20:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: true,
    host: "Sunway Tech Club",
    venue: "Graduate Hall 2",
    participants: [
      {
        id: "1",
        name: "Ding Dong",
        photo: null
      },
      {
        id: "2",
        name: "Ling Long",
        photo: null
      },
      {
        id: "3",
        name: "Ping Pong",
        photo: null
      }
    ]
  },
  {
    id: "125",
    image: require("root/src/assets/img/temp/poster2.png"),
    name: "Android Workshop",
    date: "31/12/2019",
    startTime: "18:00",
    endTime: "20:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: false,
    host: "Sunway Tech Club",
    venue: "Graduate Hall 2",
    participants: [
      {
        id: "1",
        name: "Ding Dong",
        photo: null
      },
      {
        id: "2",
        name: "Ling Long",
        photo: null
      },
      {
        id: "3",
        name: "Ping Pong",
        photo: null
      }
    ]
  }
];

const events = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const token = useSelector(state => state.tokenReducer.token);

  const _renderItem = ({ item, index }) => {
    return EventListItem(item, theme, props);
  };

  return (
    <Container>
      <Header title="Explore Event" nav={props.navigation} />
      <ScrollView>
        <View>
          <H2 style={{ marginBottom: 10 }}>Popular</H2>
          <Carousel
            data={data}
            renderItem={_renderItem}
            sliderWidth={Dimensions.get("window").width - 40}
            itemWidth={Dimensions.get("window").width - 100}
          />
        </View>
        <View>
          <H2 style={{ marginBottom: 10 }}>Suggested For You</H2>
          {data
            .filter(a => !a.complete)
            .sort(
              (a, b) =>
                moment(a.date, "DD/MM/YYYY") - moment(b.date, "DD/MM/YYYY")
            )
            .map(item => EventListItem(item, theme, props))}
        </View>
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default events;
