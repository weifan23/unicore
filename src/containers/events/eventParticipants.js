import React, { useEffect, useState } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  ErrorMessage,
  HorizontalLineSeparator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import {
  Image,
  Modal,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions
} from "react-native";
import moment from "moment";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import { Feather } from "@expo/vector-icons";
import { isEmpty } from "root/src/components/isEmpty";
import ImageViewer from "react-native-image-zoom-viewer";
import { FlatList } from "react-native-gesture-handler";

const styles = StyleSheet.create({
  defaultPhoto: {
    borderRadius: 100,
    backgroundColor: "gray",
    height: 50,
    width: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  profilePic: {
    width: 50,
    height: 50,
    borderRadius: 100
  },
  participantContainer: {
    marginVertical: 10
  },
  name: {
    marginHorizontal: 20
  },
  container: {
    padding: 20
  }
});

const eventParticipants = props => {
  let item = props.navigation.getParam("item", "NO-ID");
  const theme = useSelector(state => state.themeReducer.theme);

  const Participant = ({ participant }) => {
    return (
      <TouchableOpacity style={styles.participantContainer}>
        <Row>
          {isEmpty(participant.photo) ? (
            <View style={styles.defaultPhoto}>
              <Feather name="user" size={20} color={theme.BUTTON_COLOR} />
            </View>
          ) : (
            <Image
              style={styles.profilePic}
              source={{ uri: participant.photo }}
            />
          )}
          <H4 style={styles.name}>{participant.name}</H4>
        </Row>
      </TouchableOpacity>
    );
  };

  return (
    <>
      <Header title="Guest List" nav={props.navigation} back={true} />
      <Container style={styles.container}>
        <FlatList
          data={item.participants}
          renderItem={({ item }) => <Participant participant={item} />}
        />
      </Container>
    </>
  );
};

export default eventParticipants;
