import React, { useEffect, useState } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  ErrorMessage,
  HorizontalLineSeparator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import {
  Image,
  Modal,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions
} from "react-native";
import moment from "moment";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import { Feather } from "@expo/vector-icons";
import { isEmpty } from "root/src/components/isEmpty";
import ImageViewer from "react-native-image-zoom-viewer";

const styles = StyleSheet.create({
  map: {
    marginTop: 3,
    marginLeft: 10,
    marginRight: 4
  },
  header: {
    paddingTop: Platform.OS === "ios" ? 40 : 10
  },
  coverImage: {
    height: 400,
    width: "100%"
  },
  container: {
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    marginTop: -50,
    minHeight: Dimensions.get("window").height,
    padding: 20
  },
  description: {
    marginTop: 10,
    marginBottom: 10,
    fontSize: 14
  },
  details: {
    marginLeft: 10,
    marginVertical: 5
  },
  detailsText: {
    marginHorizontal: 15,
    fontSize: 16
  },
  address: {
    marginLeft: 50,
    marginRight: 20
  },
  participantContainer: {
    marginRight: 10,
    marginTop: 10
  },
  defaultPhoto: {
    borderRadius: 100,
    backgroundColor: "gray",
    height: 30,
    width: 30,
    alignItems: "center",
    justifyContent: "center"
  },
  profilePic: {
    width: 30,
    height: 30,
    borderRadius: 100
  }
});

// - Short Description
// - Time Date
// - Venue
// - Participants
// - Host
// - Price
// - Dress Code
// -  Note / Additional Details

const eventDetails = props => {
  let item = props.navigation.getParam("item", "NO-ID");
  const [imgVisible, setImgVisible] = useState(false);
  const theme = useSelector(state => state.themeReducer.theme);

  const images = [
    {
      props: {
        source: item.image
      }
    }
  ];

  return (
    <>
      <ParallaxScrollView
        parallaxHeaderHeight={400}
        stickyHeaderHeight={Platform.OS === "ios" ? 80 : 60}
        backgroundColor={theme.PRIMARY_BACKGROUND_COLOR}
        renderStickyHeader={() => (
          <Header
            styles={styles.header}
            title={item.name}
            nav={props.navigation}
          />
        )}
        renderBackground={() => (
          <TouchableOpacity onPress={() => setImgVisible(true)}>
            <Image style={styles.coverImage} source={item.image} />
          </TouchableOpacity>
        )}
      >
        <Container style={styles.container}>
          {/* Title & Host */}
          <H1>{item.name}</H1>
          <Row>
            <P1>by </P1>
            <TouchableOpacity>
              <H4> {item.host}</H4>
            </TouchableOpacity>
          </Row>

          {/* Description */}
          <P1 style={styles.description}> {item.description}</P1>

          {/* Date */}
          <Row style={styles.details}>
            <Feather
              name="calendar"
              size={18}
              color={theme.PRIMARY_TEXT_COLOR}
            />

            <S1 style={styles.detailsText}>
              {moment(item.date, "D/M/YYYY").format("dddd, D MMM YYYY")}
            </S1>
          </Row>

          {/* Time */}
          <Row style={styles.details}>
            <Feather name="clock" size={18} color={theme.PRIMARY_TEXT_COLOR} />
            <S1 style={styles.detailsText}>
              {item.startTime} ~ {item.endTime}
            </S1>
          </Row>

          {/* Venue */}
          <Row style={styles.details}>
            <Feather
              name="map-pin"
              size={18}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            <S1 style={styles.detailsText}>{item.venue}</S1>
          </Row>
          {item.address && item.venue ? (
            <TouchableOpacity style={styles.address}>
              <P1>{item.address}</P1>
            </TouchableOpacity>
          ) : (
            <View />
          )}

          <HorizontalLineSeparator
            margin={20}
            opacity={0.2}
            color={theme.PRIMARY_TEXT_COLOR}
          />

          {/* Participants */}
          <View>
            <H4>Participants</H4>
            <Row>
              {item.participants.slice(0, 4).map(participant =>
                isEmpty(participant.photo) ? (
                  <Col style={styles.participantContainer}>
                    <TouchableOpacity style={styles.defaultPhoto}>
                      <Feather
                        name="user"
                        size={15}
                        color={theme.BUTTON_COLOR}
                      />
                    </TouchableOpacity>
                    {participant.name.length > 10 ? (
                      <S1>{participant.name.substring(0, 7)}...</S1>
                    ) : (
                      <S1>{participant.name}</S1>
                    )}
                  </Col>
                ) : (
                  <Col style={styles.participantContainer}>
                    <TouchableOpacity>
                      <Image
                        style={styles.profilePic}
                        source={{ uri: participant.photo }}
                      />
                    </TouchableOpacity>
                    <S1>{participant.name}</S1>
                  </Col>
                )
              )}
              {item.participants.length > 4 ? (
                <TouchableOpacity
                  style={{ padding: 15 }}
                  onPress={() => {
                    props.navigation.navigate("Event Participants", {
                      item: item
                    });
                  }}
                >
                  <Row>
                    <Feather
                      name="users"
                      size={15}
                      color={theme.BUTTON_COLOR}
                    />
                    <S1
                      style={{
                        textAlign: "center",
                        marginLeft: 10,
                        fontSize: 15
                      }}
                    >
                      {item.participants.length - 4} others
                    </S1>
                  </Row>
                </TouchableOpacity>
              ) : (
                <View />
              )}
            </Row>
          </View>
          <Row style={{ justifyContent: "space-around" }}>
            <Button width={1 / 2.2}>Interested</Button>
            <ButtonSuccess width={1 / 2.2}>Going</ButtonSuccess>
          </Row>
        </Container>
      </ParallaxScrollView>
      <Modal visible={imgVisible} transparent={true}>
        <ImageViewer
          enableSwipeDown
          onSwipeDown={() => setImgVisible(false)}
          imageUrls={images}
          renderIndicator={() => <></>}
        />
      </Modal>
    </>
  );
};

export default eventDetails;
