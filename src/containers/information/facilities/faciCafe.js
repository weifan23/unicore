import React from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { View } from "react-native";
import { switchTheme } from "root/src/redux/actions";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";

const faciCafeScreen = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const dispatch = useDispatch();

  return (
    <Container>
			<Header title="Facilities" nav={props.navigation}/>
      <View style={{alignItems: 'center'}} >
        <H4 style={{paddingTop: 0, paddingBottom: 0}}>Cafeteria</H4>
      </View>
      


      <ScrollView>
        <H1>H1 Home 32px</H1>
        <H2>H2 Headline 24px</H2>
        <H4>H4 Headline 16px</H4>
        <S1>S1 Subtitle 12px</S1>
        <S2>S2 Subtitle 10px</S2>
        <P1>P1 Paragraph 12px</P1>
        <P2>P2 Paragraph 10px</P2>
      </ScrollView>
    </Container>
  );
};

export default faciCafeScreen;
