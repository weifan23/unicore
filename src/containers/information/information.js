import React, { useState } from "react";
import Accordion from "react-native-collapsible/Accordion";
import { View, StyleSheet } from "react-native";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector } from "react-redux";
import { TouchableOpacity } from "react-native-gesture-handler";

const data = [
  {
    title: "Student Services",
    content: [
      {
        label: "Clubs & Societies",
        nav: "InfoClubs"
      },
      {
        label: "Career Service",
        nav: "InfoCareer"
      },
      {
        label: "International Office ",
        nav: "InfoInternational"
      },
      {
        label: "Counselling Service",
        nav: "InfoCounselling"
      },
      {
        label: "Accomadation",
        nav: "InfoAccomadation"
      },
      {
        label: "Scholarship",
        nav: "InfoScholarship"
      }
    ]
  },
  {
    title: "University Support Services",
    content: [
      {
        label: "IT Service",
        nav: "InfoIT"
      },
      {
        label: "Health & Safety",
        nav: "InfoHealth"
      },
      {
        label: "Security",
        nav: "InfoSecurity"
      },
      {
        label: "Library",
        nav: "InfoLibrary"
      },
      {
        label: "Finance & Payment",
        nav: "InfoFinance"
      }
    ]
  },
  {
    title: "Facilities",
    content: [
      {
        label: "Academics",
        nav: "FaciAcademics"
      },
      {
        label: "Offices",
        nav: "FaciOffices"
      },
      {
        label: "Services",
        nav: "FaciServices"
      },
      {
        label: "Emergency Services",
        nav: "FaciEmergency"
      },
      {
        label: "Cafeteria",
        nav: "FaciCafe"
      },
      {
        label: "Sports",
        nav: "FaciSports"
      }
    ]
  },
  {
    title: "Staff Directories",
    content: [
      {
        label: "Centre of American Education",
        nav: "StaffCAE"
      },
      {
        label: "Centre of English Language Studies",
        nav: "StaffCELS"
      },
      {
        label: "School of Arts",
        nav: "StaffSOA"
      },
      {
        label: "School of Healthcare and Medical Sciences",
        nav: "StaffSHMS"
      },
      {
        label: "School of Mathematical Sciences",
        nav: "StaffSMS"
      },
      {
        label: "School of Hospitality",
        nav: "StaffSOH"
      },
      {
        label: "School of Science and Technology",
        nav: "StaffSST"
      },
      {
        label: "Sunway University Business School",
        nav: "StaffSUBS"
      }
    ]
  },
  {
    title: "Hotlines",
    content: [
      {
        label: "Facilities",
        number: "03-8899 7766"
      },
      {
        label: "IT",
        number: "00-1122 3344"
      }
    ]
  }
];

const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const [activeSections, setActiveSections] = useState([]);
  const _renderHeader = section => {
    return (
      <View>
        <H2 style={styles.titleStyle}>{section.title}</H2>
      </View>
    );
  };

  const _renderContent = section => {
    return (
      <View>
        {section.title === "Hotlines"
          ? section.content.map((item, index) => (
              <View key={index} style={styles.contentStyle}>
                <S1 style={styles.hotlineTitleStyle}>{item.label}</S1>
                <P1 style={styles.hotlineNumberStyle}>{item.number}</P1>
              </View>
            ))
          : section.content.map((item, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  props.navigation.navigate(item.nav);
                }}
              >
                <S1 style={styles.accordionContentStyle}>{item.label}</S1>
              </TouchableOpacity>
            ))}
      </View>
    );
  };

  const _updateSections = _activeSections => {
    setActiveSections(_activeSections);
  };

  const styles = StyleSheet.create({
    hotlineTitleStyle: {
      fontSize: 18
    },
    hotlineNumberStyle: {
      fontSize: 16,
      paddingBottom: 10
    },
    accordionContentStyle: {
      paddingLeft: 25,
      paddingBottom: 8,
      fontSize: 18
    },
    sectionContainerStyle: {
      marginBottom: 10,
      paddingBottom: 5,
      paddingTop: 5,
      borderRadius: 10,
      backgroundColor:
        theme.mode === "light" ? "rgba(0, 0, 0, 0.08)" : "rgba(0, 0, 0, 0.3)"
    },
    titleStyle: {
      paddingBottom: 10,
      paddingTop: 10,
      paddingLeft: 15
    },
    contentStyle: {
      paddingLeft: 22
    }
  });

  return (
    <Container>
      <Header title="Information" nav={props.navigation} />
      <ScrollView>
        <Accordion
          sections={data}
          activeSections={activeSections}
          renderHeader={_renderHeader}
          renderContent={_renderContent}
          onChange={_updateSections}
          sectionContainerStyle={styles.sectionContainerStyle}
          underlayColor={theme.INPUT_COLOR}
        />
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default login;
