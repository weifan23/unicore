import React, { useState } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import Accordion from "react-native-collapsible/Accordion";
import { TouchableOpacity } from "react-native-gesture-handler";
import { switchTheme } from "root/src/redux/actions";
import { View } from "react-native";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";

const data = [
  {
    category: "Organization",
    content: [
      {
        label: "Sunway University Student Councils",
        nav: ""
      },
      {
        label: "Sunway University Sports Council",
        nav: ""
      }
    ]
  }
];

const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const [activeSections, setActiveSections] = useState([]);

  const _renderHeader = section => {
    return (
      <View>
        <H2 style={{ paddingBottom: 10, paddingTop: 10, paddingLeft: 15 }}>
          {section.category}
        </H2>
      </View>
    );
  };

  const _renderContent = section => {
    return (
      <View>
        {section.content.map((item, index) => (
          <TouchableOpacity
            key={index}
            style={{ paddingLeft: 22 }}
            onPress={() => {
              props.navigation.navigate(item.nav);
            }}
          >
            <S1 style={{ fontSize: 18 }}>{item.label}</S1>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  const _updateSections = _activeSections => {
    setActiveSections(_activeSections);
  };
  return (
    <Container>
      <Header title="Career Services" nav={props.navigation} />
      <ScrollView style={{ paddingTop: 20 }}>
        <Button>Jobs for Sunway Students</Button>
        <Button>Local & International Jobs</Button>
      </ScrollView>
    </Container>
  );
};

export default login;
