import React, { useState } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import Accordion from 'react-native-collapsible/Accordion';
import { TouchableOpacity } from "react-native-gesture-handler";
import { switchTheme } from "root/src/redux/actions";
import { View } from "react-native";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";

const data = [
  {
    category: 'Student Leader Bodies',
    content: [
      {
        label: 'Sunway University Student Councils',
        nav: ''
      },
      {
        label: 'Sunway University Sports Council',
        nav: ''
      }
    ]
  },
  {
    category: 'Others',
    content:[
      {
        label: 'Fish Taco Club',
        nav: 'ClubProfile'
      },
      {
        label: 'Sunway Tech Club',
        nav: ''
      },
      {
        label: 'Sunway Yeet Club',
        nav: ''
      }
    ]
  }
]

const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const [activeSections ,setActiveSections] = useState([]);

  const _renderHeader = section => {
    return (
      <View>
        <H2 style={{paddingBottom: 10, paddingTop: 10, paddingLeft: 15}}>{section.category}</H2>
      </View>
    );
  };

  const _renderContent = section => {
    return (
      <View>
        {section.content.map((item, index) => (
					item.label === 'Hotlines' ?
					<TouchableOpacity key={index} style={{paddingLeft: 22}}>
						<S1 style={{ fontSize: 18}}>{item.label}</S1>
					</TouchableOpacity>
					:
					<TouchableOpacity
						key={index} 
						style={{paddingLeft: 22}} 
						onPress={() => {
						props.navigation.navigate(item.nav) }}
					>
						<S1 style={{ fontSize: 18}}>{item.label}</S1>
					</TouchableOpacity>
				))}
      </View>
    );
  };

  const _updateSections = _activeSections => {
    setActiveSections(_activeSections)
	};
  return (
    <Container>
      <Header title="Clubs and Societies" nav={props.navigation}/>
      <ScrollView style={{paddingTop: 20}}>
				<View style={{paddingBottom: 25}}>
					<Accordion
						sections={data}
						activeSections={activeSections}
						renderHeader={_renderHeader}
						renderContent={_renderContent}
						onChange={_updateSections}
						sectionContainerStyle={{
							marginBottom: 10,
							paddingBottom: 5,
							paddingTop: 5,
							borderRadius: 10,
							backgroundColor:
							theme.mode === "light" ? "rgba(0, 0, 0, 0.08)" : "rgba(0, 0, 0, 0.3)"
						}}
						underlayColor={theme.INPUT_COLOR}
					/>
				</View>
			</ScrollView>
    </Container>
  );
};

export default login;
