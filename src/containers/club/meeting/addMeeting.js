import React, { useState, useEffect } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { View, StyleSheet, Platform, Text } from "react-native";
import Modal from "react-native-modal";
import { storeToken, switchTheme } from "root/src/redux/actions";
import DateTimePicker from "@react-native-community/datetimepicker";
import styled from "styled-components/native";
import { space, layout, typography, color } from "styled-system";
import moment from "moment";
import TimeDatePicker from "root/src/components/dateTimePicker";

import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  LargeInput,
  Separator,
  ButtonText,
  ErrorMessage
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import useForm from "react-hook-form";

const INPUT = {
  DATE: "meetingDate",
  START: "startTime",
  END: "endTime"
};

const addMeeting = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  const {
    register,
    setValue,
    handleSubmit,
    errors,
    setError,
    clearError
  } = useForm({
    defaultValues: {
      date: new Date(),
      startTime: new Date(),
      endTime: new Date()
    }
  });
  useEffect(() => {
    register({ name: "date" });
    register({ name: "startTime" });
    register({ name: "endTime" });
    register({ name: "name" }, { required: true, minLength: 6 });
    register({ name: "venue" }, { required: true });
    register({ name: "agenda" }, { required: true });
  }, [register]);

  const [dateTime, setDateTime] = useState(new Date());
  const [meetingDate, setMeetingDate] = useState(new Date());
  const [startTime, setStartTime] = useState(new Date());
  const [endTime, setEndTime] = useState(new Date());
  const [mode, setMode] = useState();
  const [selectedInput, setSelectedInput] = useState();
  const [showTimeDatePicker, setShowTimeDatePicker] = useState(false);
  const cancelPicker = () => setShowTimeDatePicker(false);

  const showPicker = (input, mode) => {
    setSelectedInput(input);
    setMode(mode);
    setShowTimeDatePicker(true);
  };

  const changeDateTime = (event, date) => {
    date = date || dateTime;
    setDateTime(date);
    switch (selectedInput) {
      case INPUT.DATE:
        if (moment(date).isBefore(moment(new Date()).subtract(1, "day"))) {
          setMeetingDate(date);
          setError("date");
        } else {
          setMeetingDate(date);
          setValue("date", date);
          clearError("date");
        }
        break;

      case INPUT.START:
        setStartTime(date);
        setValue("startTime", date);
        break;

      case INPUT.END:
        setEndTime(date);
        setValue("endTime", date);
        break;
      default:
        break;
    }

    if (Platform.OS === "android") {
      setShowTimeDatePicker(false);
    }
  };

  const DTButtonContainer = styled.View`
    flex: 1;
    flex-direction: row;
    justify-content: space-between;
  `;

  const styles = StyleSheet.create({
    dateTimePickerStyle: {
      backgroundColor: theme.INPUT_COLOR,
      borderColor: theme.INPUT_COLOR
    },
    dateTimePickerTextStyle: {
      fontSize: 14,
      color: theme.PRIMARY_TEXT_COLOR
    },
    agendaInputStyle: {
      height: 300,
      textAlignVertical: "top"
    }
  });
  const onSubmit = data => {
    console.log("Form Data", data);
  };

  return (
    <Container>
      <Header back={true} title="Add Meeting" nav={props.navigation} />

      <ScrollView>
        <LargeInput
          width={1}
          placeholder="Meeting Title"
          onChangeText={text => setValue("name", text)}
        />
        {errors.name && <ErrorMessage>Invalid Meeting Title !</ErrorMessage>}
        <LargeInput
          width={1}
          placeholder="Venue"
          onChangeText={text => setValue("venue", text)}
        />
        {errors.venue && <ErrorMessage>Invalid Meeting Venue !</ErrorMessage>}
        <DTButtonContainer>
          <Button
            width={1}
            style={styles.dateTimePickerStyle}
            onPress={() => showPicker(INPUT.DATE, "date")}
          >
            <Text style={styles.dateTimePickerTextStyle}>
              Date: {moment(meetingDate).format("L")}
            </Text>
          </Button>
        </DTButtonContainer>
        {errors.date && <ErrorMessage>Invalid Date !</ErrorMessage>}
        <DTButtonContainer>
          <Button
            width={1 / 2.1}
            style={styles.dateTimePickerStyle}
            onPress={() => showPicker(INPUT.START, "time")}
          >
            <Text style={styles.dateTimePickerTextStyle}>
              Start: {moment(startTime).format("LT")}
            </Text>
          </Button>
          <Button
            width={1 / 2.1}
            style={styles.dateTimePickerStyle}
            onPress={() => showPicker(INPUT.END, "time")}
          >
            <Text style={styles.dateTimePickerTextStyle}>
              End: {moment(endTime).format("LT")}
            </Text>
          </Button>
        </DTButtonContainer>
        <LargeInput
          width={1}
          style={styles.agendaInputStyle}
          placeholder="Agenda"
          multiline={true}
          onChangeText={text => setValue("agenda", text)}
        />
        {errors.agenda && (
          <ErrorMessage>Meeting Agenda shouldn't be empty !</ErrorMessage>
        )}
        <Separator />

        <TimeDatePicker
          value={dateTime}
          show={showTimeDatePicker}
          cancel={cancelPicker}
          onChange={changeDateTime}
          mode={mode}
        />
        <Col>
          <ButtonSuccess width={1 / 1.5} onPress={handleSubmit(onSubmit)}>
            Done
          </ButtonSuccess>
          <ButtonGhost
            width={1 / 1.5}
            onPress={() => props.navigation.goBack()}
          >
            Back
          </ButtonGhost>
        </Col>
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default addMeeting;

let nameReg = /^[A-Za-z ]+$/i;
