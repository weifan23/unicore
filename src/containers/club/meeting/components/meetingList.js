import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { View, Text } from "react-native";
import moment from "moment";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  Card
} from "root/src/styles/components";
import { Feather } from "@expo/vector-icons";
import { StyleSheet } from "react-native";
import styled from "styled-components/native";
import FlipToggle from "react-native-flip-toggle-button";

const Row = styled.View`
  flex-direction: row;
`;

const DaysLeftContainer = styled.View`
  margin-horizontal: 10px;
  align-items: center;
`;

const DataContainer = styled.View`
  width: 80%;
`;

const styles = StyleSheet.create({
  card: {
    padding: 10,
    marginTop: 10,
    borderRadius: 10,
    flexShrink: 1
  },
  month: {
    marginBottom: -7
  },
  map: {
    marginRight: 7
  },
  clock: {
    marginLeft: 10,
    marginRight: 7
  },
  users: {
    marginRight: 5
  },
  userList: {
    fontFamily: "roboto-italic",
    marginLeft: 3,
    marginRight: 0
  },
  button: {
    marginBottom: 0
  },
  buttonContainer: {
    justifyContent: "space-around"
  },
  cardTextContainer: {
    marginTop: 5
  }
});

const checkZero = num => {
  return num >= 0 ? num : 0;
};

export const MeetingListItem = props => {
  const {
    id,
    description,
    date,
    venue,
    startTime,
    endTime,
    delegation,
    accept
  } = props.item;
  const theme = useSelector(state => state.themeReducer.theme);

  return (
    <Card key={id} style={styles.card}>
      <Row>
        <DaysLeftContainer>
          <H1 style={styles.month}>
            {moment(date, "DD/MM/YYYY").format("DD")}
          </H1>
          <P2>{moment(date, "DD/MM/YYYY").format("MMMM")}</P2>
        </DaysLeftContainer>
        <DataContainer>
          <Row>
            <H4 style={styles.description}>{description}</H4>
          </Row>
          <Row style={styles.cardTextContainer}>
            <Feather
              name="map-pin"
              size={10}
              style={styles.map}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            <P2>{venue}</P2>
            <Feather
              name="clock"
              size={10}
              style={styles.clock}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            <P2>
              {startTime} ~ {endTime}
            </P2>
          </Row>

          <Row style={styles.cardTextContainer}>
            <Feather
              name="users"
              size={10}
              style={styles.users}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            {delegation.slice(0, 3).map(i => (
              <P2 style={styles.userList} key={i}>
                {i}
              </P2>
            ))}
            {delegation.length > 3 && (
              <P2 style={styles.userList}>
                and {delegation.length - 3} others
              </P2>
            )}
          </Row>
        </DataContainer>
      </Row>
      <Row style={styles.buttonContainer}>
        <Button
          onPress={() => props.showAgenda(id)}
          style={styles.button}
          width={1 / 2.3}
        >
          View Agenda
        </Button>
        {accept === true ? (
          <ButtonDanger
            onPress={() => props.toggleJoin(id)}
            style={styles.button}
            width={1 / 2.3}
          >
            Cancel Join
          </ButtonDanger>
        ) : (
          <ButtonSuccess
            onPress={() => props.toggleJoin(id)}
            style={styles.button}
            width={1 / 2.3}
          >
            Join Meeting
          </ButtonSuccess>
        )}
      </Row>
    </Card>
  );
};

export const MeetingListItemComplete = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const { item } = props;

  return (
    <Card key={item.id} style={styles.card}>
      <Row>
        <DaysLeftContainer>
          <H1 style={styles.month}>
            {moment(item.date, "DD/MM/YYYY").format("DD")}
          </H1>
          <P2>{moment(item.date, "DD/MM/YYYY").format("MMMM")}</P2>
        </DaysLeftContainer>
        <DataContainer>
          <H4 style={styles.description}>{item.description}</H4>
          <Row style={styles.cardTextContainer}>
            <Feather
              name="map-pin"
              size={10}
              style={styles.map}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            <P2>{item.venue}</P2>
            <Feather
              name="clock"
              size={10}
              style={styles.clock}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            <P2>
              {item.startTime} ~ {item.endTime}
            </P2>
          </Row>

          <Row style={styles.cardTextContainer}>
            <Feather
              name="users"
              size={10}
              style={styles.users}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            {item.delegation.slice(0, 3).map(i => (
              <P2 style={styles.userList} key={i}>
                {i}
              </P2>
            ))}
            {item.delegation.length > 3 && (
              <P2 style={styles.userList}>
                and {item.delegation.length - 3} others
              </P2>
            )}
          </Row>
        </DataContainer>
      </Row>
      <Button onPress={() => props.showAgenda(item.id)} style={styles.button}>
        View Agenda
      </Button>
    </Card>
  );
};
