import React, { useState, useEffect } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import { View } from "react-native";
import moment from "moment";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import { FlatList, Text, StyleSheet } from "react-native";
import { Feather } from "@expo/vector-icons";
import Modal from "react-native-modal";

import {
  MeetingListItem,
  MeetingListItemComplete
} from "./components/meetingList";
import styled from "styled-components/native";

import ActionButton from "react-native-action-button";

const initialData = [
  {
    id: "233",
    date: "20/12/2019",
    description: "STC General Meeting",
    assignedBy: "Pete",
    delegation: ["Sam", "John"],
    progress: 0.2,
    venue: "Makerspace",
    complete: true,
    accept: true,
    startTime: "18:00",
    endTime: "19:00",
    agenda:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis libero a mi semper interdum ac nec ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tempor purus malesuada sapien sodales, nec congue nulla sagittis. Vivamus eleifend libero justo, vel semper velit suscipit eget."
  },
  {
    id: "232",
    date: "29/12/2019",
    description: "STC General Meeting",
    assignedBy: "John",
    delegation: ["Sam", "James", "John", "Jake"],
    progress: 0.2,
    accept: true,
    venue: "Library Pod 2",
    complete: false,
    startTime: "18:00",
    endTime: "19:00",
    agenda:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis libero a mi semper interdum ac nec ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tempor purus malesuada sapien sodales, nec congue nulla sagittis. Vivamus eleifend libero justo, vel semper velit suscipit eget."
  },
  {
    id: "323",
    date: "25/12/2019",
    description: "AWS Team Meeting",
    assignedBy: "James",
    delegation: ["Frank", "Pete", "Jack", "Sarah", "Paula"],
    progress: 0.2,
    venue: "Makerspace",
    accept: false,
    complete: false,
    startTime: "18:00",
    endTime: "19:00",
    agenda:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis libero a mi semper interdum ac nec ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tempor purus malesuada sapien sodales, nec congue nulla sagittis. Vivamus eleifend libero justo, vel semper velit suscipit eget. "
  },
  {
    id: "325",
    date: "2/12/2019",
    description: "AWS Rehearsal",
    assignedBy: "James",
    delegation: ["Frank", "Pete", "Jack", "Sarah", "Paula"],
    progress: 0.2,
    venue: "University Foyer",
    complete: true,
    accept: false,
    startTime: "18:00",
    endTime: "19:00",
    agenda:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis libero a mi semper interdum ac nec ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tempor purus malesuada sapien sodales, nec congue nulla sagittis. Vivamus eleifend libero justo, vel semper velit suscipit eget."
  },
  {
    id: "125",
    date: "1/12/2019",
    description: "Team Bonding Trip Meeting",
    assignedBy: "James",
    delegation: ["Frank", "Pete", "Jack", "Sarah", "Paula"],
    progress: 0.2,
    accept: false,
    venue: "University Foyer",
    complete: true,
    startTime: "18:00",
    endTime: "19:00",
    agenda:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis libero a mi semper interdum ac nec ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tempor purus malesuada sapien sodales, nec congue nulla sagittis. Vivamus eleifend libero justo, vel semper velit suscipit eget."
  }
];

const Row = styled.View`
  flex-direction: row;
`;
const meeting = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const dispatch = useDispatch();
  const [data, setData] = useState(initialData);
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedItem, setSelectedItem] = useState({});

  const toggleJoin = id => {
    let tempData = data;
    const objIndex = tempData.findIndex(obj => obj.id == id);
    tempData[objIndex].accept = !data[objIndex].accept;
    // accept true = add user, accept false = remove user
    if (data[objIndex].accept) {
      tempData[objIndex].delegation.unshift("Sam");
    } else {
      tempData[objIndex].delegation = tempData[objIndex].delegation.filter(
        e => e !== "Sam"
      );
    }

    setData([...tempData]);
  };

  const showAgenda = id => {
    let tempData = data;
    const objIndex = tempData.findIndex(obj => obj.id == id);
    setSelectedItem(data[objIndex]);
    setModalVisible(true);
  };

  const styles = StyleSheet.create({
    modal: {
      backgroundColor: theme.PRIMARY_BACKGROUND_COLOR,
      padding: 20,
      borderRadius: 15
    },
    map: {
      marginTop: 3,
      marginLeft: 5,
      marginRight: 7
    },
    clock: {
      marginTop: 3,
      marginLeft: 10,
      marginRight: 7
    }
  });

  return (
    <Container>
      <Header title="Meeting" nav={props.navigation} />

      <ScrollView>
        <H2>Upcoming Meeting</H2>
        {data
          .filter(a => !a.complete)
          .sort(
            (a, b) =>
              moment(a.date, "DD/MM/YYYY") - moment(b.date, "DD/MM/YYYY")
          )
          .map((item, index) => (
            <MeetingListItem
              toggleJoin={toggleJoin}
              showAgenda={showAgenda}
              item={item}
              index={index}
            />
          ))}
        <Separator />
        <H2>Past Meeting</H2>
        {data
          .filter(a => a.complete)
          .map(item => (
            <MeetingListItemComplete item={item} showAgenda={showAgenda} />
          ))}
        <Separator />
      </ScrollView>
      <ActionButton
        buttonColor={theme.PRIMARY_TEXT_COLOR}
        renderIcon={() => (
          <Feather
            name="plus"
            size={20}
            color={theme.PRIMARY_BACKGROUND_COLOR}
          />
        )}
        onPress={() => {
          props.navigation.navigate("AddMeeting");
        }}
      />
      <Modal
        onBackdropPress={() => setModalVisible(false)}
        isVisible={modalVisible}
      >
        <View style={styles.modal}>
          <H2>{selectedItem.description}</H2>
          <Row>
            <Feather
              name="map-pin"
              size={10}
              style={styles.map}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            <P2> {selectedItem.venue}</P2>
            <Feather
              name="clock"
              size={10}
              style={styles.clock}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            <P2>
              {selectedItem.startTime} ~ {selectedItem.endTime}
            </P2>
          </Row>
          <H4>{selectedItem.agenda}</H4>
          <Button onPress={() => setModalVisible(false)}>Close</Button>
        </View>
      </Modal>
    </Container>
  );
};

export default meeting;
