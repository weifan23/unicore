import React from "react";
import { Image } from "react-native";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";

const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const dispatch = useDispatch();
  return (
    <Container>
      <Header title="Club" nav={props.navigation} />

      <ScrollView contentContainerStyle={{ alignItems: "center" }}>
        <Image
          style={{
            height: 150,
            width: 150,
            borderRadius: 10
          }}
          source={require("root/src/assets/img/temp/logo.png")}
        />
        <H1>Sunway Tech Club</H1>
      </ScrollView>
    </Container>
  );
};

export default login;
