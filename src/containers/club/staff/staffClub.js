import React, { useState } from "react";
import {
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Text,
  View,
  List,
  FlatList
} from "react-native";
import { useSelector } from "react-redux";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  Card
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { SearchBar } from "react-native-elements";
import styled from "styled-components/native";
import { MenuProvider } from "react-native-popup-menu";
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from "react-native-popup-menu";
import { Feather } from "@expo/vector-icons";

const staffClub = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  const initialData = [
    {
      category: "A",
      clubs: [
        {
          id: "124",
          name: "Sunway Analytics Society",
          proposal: 2
        },
        {
          id: "126",
          name: "Sunway Anime Club",
          proposal: 1
        }
      ]
    },
    { category: "B", clubs: [{}] },
    {
      category: "C",
      clubs: [
        {
          id: "127",
          name: "Sunway University Cheerleading",
          proposal: 0
        },
        {
          id: "125",
          name: "Sunway Chinese Society"
        }
      ]
    },
    {
      category: "E",
      clubs: [
        {
          id: "128",
          name: "Sunway Economics Society"
        },
        { id: "129", name: "Sunway Engineering Club" }
      ]
    },
    { category: "F", clubs: [{}] },
    { category: "G", clubs: [{}] },
    { category: "D", clubs: [{}] },
    { category: "H", clubs: [{}] },
    { category: "I", clubs: [{}] },
    { category: "J", clubs: [{}] },
    { category: "K", clubs: [{}] },
    { category: "L", clubs: [{}] },
    { category: "M", clubs: [{}] },
    { category: "N", clubs: [{}] },
    { category: "O", clubs: [{}] },
    { category: "P", clubs: [{}] },
    { category: "Q", clubs: [{}] },
    { category: "R", clubs: [{}] },
    { category: "S", clubs: [{}] },
    {
      category: "T",
      clubs: [
        {
          id: "123",
          name: "Sunway Tech Club"
        }
      ]
    },
    { category: "U", clubs: [{}] },
    { category: "V", clubs: [{}] },
    { category: "W", clubs: [{}] },
    { category: "X", clubs: [{}] },
    { category: "Y", clubs: [{}] },
    { category: "Z", clubs: [{}] }
  ];

  const [data, setData] = useState(initialData);
  const [searchState, setSearchState] = useState("");
  const [imageState, setImageState] = useState(false);

  const updateSearch = search => {
    const temp = search.toUpperCase();
    setSearchState(temp);
    console.log(searchState);
  };

  const { search } = searchState;

  const styles = StyleSheet.create({
    card: {
      padding: 10,
      marginTop: 10,
      borderRadius: 10
    },
    title: {
      paddingLeft: 10
    },
    searchBarStyle: {
      backgroundColor: theme.PRIMARY_BACKGROUND_COLOR
    },
    proposalCirlce: {
      height: 30,
      width: 30,
      borderRadius: 30,
      backgroundColor: "#ff7c7c",
      justifyContent: "center",
      alignItems: "center"
    },
    clubCardContainer: {
      flex: 1,
      justifyContent: "space-between"
    }
  });

  const Circle = styled.View`
    height: 40px;
    width: 40px;
    border-radius: 40px;
    background-color: rgba(0, 0, 0, 0.3);
  `;

  function ClubCard({ name }) {
    return (
      <TouchableOpacity>
        <Card style={styles.card}>
          <Row>
            {imageState ? (
              <H1>Will delegate logo for each club in the future</H1>
            ) : (
              <Circle />
            )}
            <H4 style={styles.title}>{name}</H4>
          </Row>
        </Card>
      </TouchableOpacity>
    );
  }

  function ProposalCount({ proposal }) {
    return (
      <View style={styles.proposalCirlce}>
        <H4>{proposal}</H4>
      </View>
    );
  }

  function FeaturedClubCard({ name, proposal }) {
    return (
      <TouchableOpacity>
        <MenuProvider>
          <Card style={styles.card}>
            <Row>
              {imageState ? (
                <H1>Will delegate logo for each club in the future</H1>
              ) : (
                <Circle />
              )}
              <Row style={styles.clubCardContainer}>
                <H4 style={styles.title}>{name}</H4>
                <Menu>
                  <MenuTrigger>
                    <ProposalCount proposal={proposal} />
                  </MenuTrigger>
                  <MenuOptions>
                    <MenuOption
                      onSelect={() => alert(`New page not ready yet`)}
                      text="View Proposal"
                    />
                  </MenuOptions>
                </Menu>
              </Row>
            </Row>
          </Card>
        </MenuProvider>
      </TouchableOpacity>
    );
  }

  return (
    <Container>
      <Header title="Staff Interface" nav={props.navigation} />
      <ScrollView>
        <SearchBar
          placeholder="Search for clubs..."
          onChangeText={updateSearch}
          value={search}
          containerStyle={styles.searchBarStyle}
          inputContainerStyle={styles.searchBarStyle}
        />

        {initialData.map(section =>
          section.clubs
            .filter(item => item.proposal !== 0 && item.proposal !== undefined)
            .map(item => {
              if (item.name !== undefined) {
                return (
                  <FeaturedClubCard name={item.name} proposal={item.proposal} />
                );
              } else {
              }
            })
        )}

        {searchState === ""
          ? initialData.map(section =>
              section.clubs
                .filter(
                  item => item.proposal === 0 || item.proposal === undefined
                )
                .map(item => {
                  if (item.name !== undefined) {
                    return <ClubCard name={item.name} />;
                  } else {
                  }
                })
            )
          : initialData
              .filter(section => section.category === searchState)
              .map(section =>
                section.clubs.map(item => {
                  if (item.name !== undefined) {
                    return <ClubCard name={item.name} />;
                  } else {
                  }
                })
              )}
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default staffClub;
