import React, { useState } from "react";
import {
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Text
} from "react-native";
import { useSelector } from "react-redux";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  Card
} from "root/src/styles/components";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";
import moment from "moment";

const currentDate = new Date();
const currentMonth = date => moment(date).format("MMM");

const styles = StyleSheet.create({
  cardContainer: {
    paddingBottom: 15
  },
  chart: {
    borderRadius: 12
  }
});

const screenWidth = Dimensions.get("window").width / 1.11;

const MembersChart = () => {
  const theme = useSelector(state => state.themeReducer.theme);
  const memberData = {
    labels: [
      currentMonth(moment(currentDate).subtract(5, "M")),
      currentMonth(moment(currentDate).subtract(4, "M")),
      currentMonth(moment(currentDate).subtract(3, "M")),
      currentMonth(moment(currentDate).subtract(2, "M")),
      currentMonth(moment(currentDate).subtract(1, "M")),
      currentMonth(currentDate)
    ],
    datasets: [
      {
        data: [13, 12, 16, 10, 20, 17]
      }
    ]
  };
  const memberChartConfig = {
    backgroundGradientFrom: theme.DARK_BUTTON_TEXT_COLOR,
    backgroundGradientTo: theme.DARK_BUTTON_TEXT_COLOR,
    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    propsForDots: {
      r: "4",
      strokeWidth: "2"
    }
  };
  return (
    <TouchableOpacity>
      <Row style={styles.cardContainer}>
        <LineChart
          data={memberData}
          width={screenWidth} // from react-native
          height={220}
          chartConfig={memberChartConfig}
          fromZero={true}
          bezier
          style={styles.chart}
        />
      </Row>
    </TouchableOpacity>
  );
};

const FollowersChart = () => {
  const theme = useSelector(state => state.themeReducer.theme);
  const FollowersData = {
    labels: [
      currentMonth(moment(currentDate).subtract(5, "M")),
      currentMonth(moment(currentDate).subtract(4, "M")),
      currentMonth(moment(currentDate).subtract(3, "M")),
      currentMonth(moment(currentDate).subtract(2, "M")),
      currentMonth(moment(currentDate).subtract(1, "M")),
      currentMonth(currentDate)
    ],
    datasets: [
      {
        data: [5, 7, 3, 2, 5, 8]
      }
    ]
  };
  const FollowersChartConfig = {
    backgroundGradientFrom: theme.DARK_BUTTON_TEXT_COLOR,
    backgroundGradientTo: theme.DARK_BUTTON_TEXT_COLOR,
    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`
  };

  return (
    <TouchableOpacity>
      <Row style={styles.cardContainer}>
        <LineChart
          data={FollowersData}
          width={screenWidth} // from react-native
          height={220}
          chartConfig={FollowersChartConfig}
          fromZero={true}
          bezier
          style={styles.chart}
        />
      </Row>
    </TouchableOpacity>
  );
};

const ActivitiesChart = () => {
  const theme = useSelector(state => state.themeReducer.theme);
  const activitiesData = {
    labels: [
      currentMonth(moment(currentDate).subtract(5, "M")),
      currentMonth(moment(currentDate).subtract(4, "M")),
      currentMonth(moment(currentDate).subtract(3, "M")),
      currentMonth(moment(currentDate).subtract(2, "M")),
      currentMonth(moment(currentDate).subtract(1, "M")),
      currentMonth(currentDate)
    ],
    datasets: [
      {
        data: [5, 7, 3, 2, 5, 8]
      }
    ]
  };
  const activitiesChartConfig = {
    backgroundGradientFrom: theme.DARK_BUTTON_TEXT_COLOR,
    backgroundGradientTo: theme.DARK_BUTTON_TEXT_COLOR,
    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`
  };

  return (
    <TouchableOpacity>
      <Row style={styles.cardContainer}>
        <BarChart
          style={styles.chart}
          data={activitiesData}
          width={screenWidth}
          height={220}
          chartConfig={activitiesChartConfig}
          fromZero={true}
        />
      </Row>
    </TouchableOpacity>
  );
};

const MeetingsChart = () => {
  const theme = useSelector(state => state.themeReducer.theme);
  const MeetingsData = {
    labels: [
      currentMonth(moment(currentDate).subtract(5, "M")),
      currentMonth(moment(currentDate).subtract(4, "M")),
      currentMonth(moment(currentDate).subtract(3, "M")),
      currentMonth(moment(currentDate).subtract(2, "M")),
      currentMonth(moment(currentDate).subtract(1, "M")),
      currentMonth(currentDate)
    ],
    datasets: [
      {
        data: [13, 12, 16, 10, 20, 17]
      }
    ]
  };
  const MeetingsChartConfig = {
    backgroundGradientFrom: theme.DARK_BUTTON_TEXT_COLOR,
    backgroundGradientTo: theme.DARK_BUTTON_TEXT_COLOR,
    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`
  };

  return (
    <TouchableOpacity>
      <Row style={styles.cardContainer}>
        <BarChart
          style={styles.chart}
          data={MeetingsData}
          width={screenWidth}
          height={220}
          chartConfig={MeetingsChartConfig}
          fromZero={true}
        />
      </Row>
    </TouchableOpacity>
  );
};

export { MembersChart, FollowersChart, ActivitiesChart, MeetingsChart };
