import React, { useState } from "react";
import {
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Text,
  View
} from "react-native";
import { useSelector } from "react-redux";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  Card
} from "root/src/styles/components";
import Header from "root/src/components/header/header";

import {
  MembersChart,
  FollowersChart,
  ActivitiesChart,
  MeetingsChart
} from "./components/charts";
import moment from "moment";

const data = {
  clubProfilePic:
    "https://logopond.com/logos/e981dec654f6bf182dd4fcf707901508.png",
  clubName: "Fish Taco Club",
  activityDone: 20,
  member: 30,
  clubCategory: "Food & Beverage",
  isFollow: true
};

const memberData = [
  {
    id: "123",
    name: "Ding Nick Hong"
  },
  {
    id: "124",
    name: "Tommeh"
  },
  {
    id: "125",
    name: "Lew Jia Yi"
  }
];

const followerData = [
  {
    id: "123",
    name: "Ding Nick Hong"
  },
  {
    id: "124",
    name: "Tommeh"
  },
  {
    id: "125",
    name: "Lew Jia Yi"
  }
];

const upcomings = {
  meeting: {
    date: moment(new Date()).format("D/M"),
    title: "Career Tech Talk"
  },
  event: {
    date: moment(new Date()).format("D/M"),
    title: "Basic Java Workshop 2019"
  },
  task: {
    date: moment(new Date()).format("D/M"),
    title: "Finish Poster for Python Workshop"
  }
};

const dashboard = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  const styles = StyleSheet.create({
    clubHeaderContainerStyle: {
      paddingBottom: 30
    },
    memberFollowerCount: {
      justifyContent: "space-around",
      paddingBottom: 30
    },
    clubName: {
      paddingLeft: 20
    },
    clubPhotoStyle: {
      width: 40,
      height: 40,
      borderRadius: 50,
      borderColor: theme.PRIMARY_TEXT_COLOR,
      borderWidth: 1
    },
    card: {
      height: 150,
      width: Dimensions.get("window").width / 3.5,
      alignItems: "center"
    },
    cardContainer: {
      justifyContent: "space-between",
      textAlign: "center",
      paddingBottom: 15
    },
    cardHeader: {
      paddingTop: 10
    },
    cardDate: {
      paddingTop: 30
    },
    cardTitle: {
      paddingTop: 20,
      textAlign: "center",
      marginHorizontal: 5
    },
    sectionHeader: {
      paddingTop: 15,
      paddingLeft: 6,
      paddingBottom: 15
    }
  });

  return (
    <Container>
      <Header title="Dashboard" nav={props.navigation} />
      <ScrollView>
        <Row style={styles.clubHeaderContainerStyle}>
          <Image
            style={styles.clubPhotoStyle}
            source={{ uri: data.clubProfilePic }}
          />
          <H2 style={styles.clubName}>{data.clubName}</H2>
        </Row>
        <Row style={styles.memberFollowerCount}>
          <TouchableOpacity>
            <Col>
              <H4>{memberData.length}</H4>
            </Col>
            <Col>
              <H4>Members</H4>
            </Col>
          </TouchableOpacity>
          <TouchableOpacity>
            <Col>
              <H4>{followerData.length}</H4>
            </Col>
            <Col>
              <H4>Followers</H4>
            </Col>
          </TouchableOpacity>
        </Row>
        <H4 style={styles.sectionHeader}>Upcoming</H4>
        <Row style={styles.cardContainer}>
          <TouchableOpacity>
            <Card style={styles.card}>
              <H4 style={styles.cardHeader}>Meeting</H4>
              <H4 style={styles.cardDate}>{upcomings.meeting.date}</H4>
              <S1 style={styles.cardTitle}>{upcomings.meeting.title}</S1>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity>
            <Card style={styles.card}>
              <H4 style={styles.cardHeader}>Event</H4>
              <H4 style={styles.cardDate}>{upcomings.event.date}</H4>
              <S1 style={styles.cardTitle}>{upcomings.event.title}</S1>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity>
            <Card style={styles.card}>
              <H4 style={styles.cardHeader}>Task</H4>
              <H4 style={styles.cardDate}>{upcomings.task.date}</H4>
              <S1 style={styles.cardTitle}>{upcomings.task.title}</S1>
            </Card>
          </TouchableOpacity>
        </Row>
        <H4 style={styles.sectionHeader}>Member Increase</H4>
        <Row style={styles.cardContainer}>
          <MembersChart />
        </Row>
        <H4 style={styles.sectionHeader}>Follower Increase</H4>
        <Row style={styles.cardContainer}>
          <FollowersChart />
        </Row>
        <H4 style={styles.sectionHeader}>Activities</H4>
        <Row style={styles.cardContainer}>
          <ActivitiesChart />
        </Row>
        <H4 style={styles.sectionHeader}>Meetings</H4>
        <Row style={styles.cardContainer}>
          <MeetingsChart />
        </Row>
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default dashboard;
