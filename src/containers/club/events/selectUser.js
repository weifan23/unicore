import React, { useState, useEffect } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { View, Alert, Platform, TouchableOpacity } from "react-native";
import Modal from "react-native-modal";
import { storeToken, switchTheme } from "root/src/redux/actions";
import DateTimePicker from "@react-native-community/datetimepicker";
import styled from "styled-components/native";
import { space, layout, typography, color } from "styled-system";
import moment from "moment";
import { Feather } from "@expo/vector-icons";

import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  LargeInput,
  Separator,
  ButtonText
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import useForm from "react-hook-form";

const initialData = [
  {
    id: 1,
    name: "Sioux Wickmann",
    role: "Core Committee",
    selected: false
  },
  {
    id: 2,
    name: "Godart Theobald",
    role: "Core Committee",
    selected: false
  },
  {
    id: 3,
    name: "Athene Glassopp",
    role: "Normal Committee",
    selected: false
  },
  {
    id: 4,
    name: "Nat Milroy",
    role: "Norma Committee",
    selected: false
  },
  {
    id: 5,
    name: "Cassondra Loan",
    role: "Core Committee",
    selected: false
  },
  {
    id: 6,
    name: "Sayres Pedrollo",
    role: "Core Committee",
    selected: false
  },
  {
    id: 7,
    name: "Elfrieda Smalley",
    role: "Bennie Scotts",
    selected: false
  },
  {
    id: 8,
    name: "Gates Gorrie",
    role: "Vitia Heathcoat",
    selected: false
  },
  {
    id: 9,
    name: "Ansley Redington",
    role: "Seymour Pilipyak",
    selected: false
  },
  {
    id: 10,
    name: "Edmon Giroldo",
    role: "Angelico Prandi",
    selected: false
  }
];
const Horizontal = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const addTask = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const dispatch = useDispatch();
  const [data, setData] = useState(initialData);
  const selectUser = index => {
    let tempData = data;
    tempData[index].selected = !data[index].selected;
    setData([...tempData]);
  };

  return (
    <Container>
      <Header title="Add Task" nav={props.navigation} />

      <ScrollView>
        <H4>Members</H4>
        {data.map((item, index) => (
          <TouchableOpacity onPress={() => selectUser(index)}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                paddingVertical: 10,
                borderBottomWidth: 1,
                borderBottomColor: theme.PRIMARY_TEXT_COLOR
              }}
            >
              <P1 key={item.id}>{item.name}</P1>
              {item.selected && (
                <Feather
                  name="check"
                  color={theme.PRIMARY_TEXT_COLOR}
                  size={15}
                />
              )}
            </View>
          </TouchableOpacity>
        ))}
        <Separator />

        {Platform.OS === "android" && dateState.show && (
          <DateTimePicker
            value={new Date()}
            mode={dateState.mode}
            is24Hour={true}
            display="default"
            onChange={setDate}
          />
        )}
        <View style={{ alignItems: "center" }}>
          <Button width={1 / 1.5} onPress={() => props.navigation.goBack()}>
            Done
          </Button>
          <ButtonGhost
            width={1 / 1.5}
            onPress={() => props.navigation.goBack()}
          >
            Back
          </ButtonGhost>
        </View>
      </ScrollView>
    </Container>
  );
};

export default addTask;
