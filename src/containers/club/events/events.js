import React from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import moment from "moment";
import { Feather } from "@expo/vector-icons";

import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import {
  EventListItem,
  TaskListItemComplete
} from "./components/eventListItem";
import ActionButton from "react-native-action-button";

const data = [
  {
    id: "2111",
    image: require("root/src/assets/img/temp/poster2.png"),
    name: "Hacktoberfest 2019",
    date: "29/12/2019",
    startTime: "18:00",
    endTime: "20:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: false,
    club: "Sunway Tech Club",
    venue: "Graduate Hall 2"
  },
  {
    id: "232",
    image: require("root/src/assets/img/temp/poster3.png"),
    name: "Career Tech Talk",
    date: "30/12/2019",
    startTime: "18:00",
    endTime: "20:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: true,
    club: "Sunway Tech Club",
    venue: "Makerspace"
  },
  {
    id: "323",
    image: require("root/src/assets/img/temp/poster2.png"),
    name: "React Workshop",
    date: "30/1/2020",
    startTime: "18:00",
    endTime: "20:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: false,
    club: "Sunway Tech Club",
    venue: "Graduate Hall 1"
  },
  {
    id: "325",
    image: require("root/src/assets/img/temp/poster.png"),
    name: "WebLaunch 2019",
    date: "25/12/2019",
    startTime: "18:00",
    endTime: "20:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: true,
    club: "Sunway Tech Club",
    venue: "Graduate Hall 2"
  },
  {
    id: "125",
    image: require("root/src/assets/img/temp/poster2.png"),
    name: "Android Workshop",
    date: "31/12/2019",
    startTime: "18:00",
    endTime: "20:00",
    description:
      "App development workshop, learning basics of Android Studio and deploying a mobile application",
    public: false,
    club: "Sunway Tech Club",
    venue: "Graduate Hall 2"
  }
];

const events = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const token = useSelector(state => state.tokenReducer.token);

  const dispatch = useDispatch();
  return (
    <Container>
      <Header title="Events" nav={props.navigation} />
      <ScrollView>
        {data
          .filter(a => !a.complete)
          .sort(
            (a, b) =>
              moment(a.date, "DD/MM/YYYY") - moment(b.date, "DD/MM/YYYY")
          )
          .map(item => EventListItem(item, theme, props))}
        <Separator />
      </ScrollView>
      <ActionButton
        buttonColor={theme.PRIMARY_TEXT_COLOR}
        renderIcon={() => (
          <Feather
            name="plus"
            size={20}
            color={theme.PRIMARY_BACKGROUND_COLOR}
          />
        )}
        onPress={() => {
          props.navigation.navigate("AddEvent");
        }}
      />
    </Container>
  );
};

export default events;
