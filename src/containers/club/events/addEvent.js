import React, { useState, useEffect } from "react";
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  LargeInput
} from "root/src/styles/components";
import {
  Text,
  Platform,
  View,
  Dimensions,
  StyleSheet,
  Image
} from "react-native";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components/native";
import useForm from "react-hook-form";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import { Feather } from "@expo/vector-icons";
import Modal from "react-native-modal";

const addEvent = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  const { register, handleSubmit, setValue, errors } = useForm();
  useEffect(() => {
    register({ name: "eventName" }, { required: true });
    register({ name: "date" }, { required: true });
    register({ name: "venue" }, { required: true });

    register({ name: "startTime" }, { required: true });
    register({ name: "endTime" }, { required: true });
    register({ name: "eventDetails" }, { required: true, pattern: nameReg });
  }, [register]);
  const [photoUri, setPhotoUri] = useState("");
  const [pickerState, setPickerState] = useState({
    mode: ""
  });

  const [dateState, setDateState] = useState({
    date: new Date(),
    startTime: new Date(),
    endTime: new Date(),
    mode: "date",
    show: false,
    input: "date",
    showModal: false,
    dateValue: new Date()
  });

  const cancelPicker = () => {
    setDateState(prev => ({
      ...prev,
      showModal: Platform.OS === "ios" ? false : true
    }));
  };

  const setDate = (event, date) => {
    date = date || dateState.date;

    switch (dateState.input) {
      case "date":
        setDateState(prev => ({
          ...prev,
          show: Platform.OS === "ios" ? true : false,
          date,
          dateValue: date
        }));
        setValue("date", date, true);
        break;
      case "start":
        setDateState(prev => ({
          ...prev,
          show: Platform.OS === "ios" ? true : false,
          startTime: date,
          dateValue: date
        }));
        break;
      case "end":
        setDateState(prev => ({
          ...prev,
          show: Platform.OS === "ios" ? true : false,
          endTime: date,
          dateValue: date
        }));
        setValue("endTime", date, true);
        break;
      default:
        break;
    }
  };

  const show = (mode, input) => {
    setDateState(prev => ({
      ...prev,
      show: true,
      showModal: Platform.OS === "ios" ? true : false,
      mode,
      input
    }));
  };

  const dateTimePicker = (mode, input) => {
    setPickerState({
      mode
    });
    show(mode, input);
  };

  const onSubmit = async ({
    eventName,
    date,
    startTime,
    endTime,
    eventDetails
  }) => {
    try {
      setIsLoading(true);
      const [res, valid] = await registerUser(email, name, password);
      console.log(res);
      if (valid) {
        setIsLoading(false);
      } else {
        Alert.alert("Error Creating Event");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const uploadPoster = async () => {
    const status = await getPermissionAsync();
    if (status === "granted") {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1
      });
      setPhotoUri(result.uri);
    }
  };

  const getPermissionAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== "granted") {
      alert(
        "Please go to Privacy Settings to Allow Permission for Photo Album."
      );
      return status;
    }
    return "granted";
  };

  const DTButtonContainer = styled.View`
    flex: 1;
    flex-direction: row;
    justify-content: space-between;
  `;

  const ImageUploadButton = styled.TouchableOpacity`
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 200px;
    margin-bottom: 30px;
  `;

  const DateTimeModal = styled.View`
    padding: 10px;
    border-radius: 15px;
    height: 52%;
    background-color: ${props => props.theme.PRIMARY_BACKGROUND_COLOR};
  `;

  const styles = StyleSheet.create({
    imageStyle: {
      width: 300,
      height: 200
    },
    defaultImageStyle: {
      padding: 30,
      alignItems: "center",
      borderWidth: 2,
      borderColor: theme.PRIMARY_TEXT_COLOR,
      borderRadius: 15,
      opacity: 0.5,
      borderStyle: "dashed"
    },
    dateTimePickerStyle: {
      backgroundColor: theme.INPUT_COLOR,
      borderColor: theme.INPUT_COLOR
    },
    dateTimePickerTextStyle: {
      fontSize: 14,
      color: theme.PRIMARY_TEXT_COLOR
    },
    eventDetailsInputStyle: {
      height: 150,
      textAlignVertical: "top",
      paddingTop: 15
    }
  });

  return (
    <Container>
      <Header back={true} title="Create Event" nav={props.navigation} />
      <ScrollView>
        <ImageUploadButton onPress={() => uploadPoster()}>
          {photoUri ? (
            <Image style={styles.imageStyle} source={{ uri: photoUri }} />
          ) : (
            <View style={styles.defaultImageStyle}>
              <Feather
                name="image"
                size={30}
                color={theme.PRIMARY_TEXT_COLOR}
              />
              <H4>Upload Poster</H4>
            </View>
          )}
        </ImageUploadButton>
        <LargeInput
          placeholder={"Event name"}
          onChangeText={text => setValue("eventName", text, true)}
        />
        <LargeInput
          placeholder={"Venue"}
          onChangeText={text => setValue("venue", text, true)}
        />
        {errors.eventName && <S2>Event name shouldn't be empty !</S2>}
        <DTButtonContainer>
          <Button
            width={1}
            style={styles.dateTimePickerStyle}
            onPress={() => dateTimePicker("date", "date")}
          >
            <Text style={styles.dateTimePickerTextStyle}>
              Date: {moment(dateState.date).format("DD/MM/YYYY")}
            </Text>
          </Button>
        </DTButtonContainer>
        <DTButtonContainer>
          <Button
            width={1 / 2.1}
            style={styles.dateTimePickerStyle}
            onPress={() => dateTimePicker("time", "start")}
          >
            <Text style={styles.dateTimePickerTextStyle}>
              Start: {moment(dateState.startTime).format("hh:mm a")}
            </Text>
          </Button>
          <Button
            width={1 / 2.1}
            style={styles.dateTimePickerStyle}
            onPress={() => dateTimePicker("time", "end")}
          >
            <Text style={styles.dateTimePickerTextStyle}>
              End: {moment(dateState.endTime).format("hh:mm a")}
            </Text>
          </Button>
        </DTButtonContainer>

        <LargeInput
          placeholder={"Event Details"}
          style={styles.eventDetailsInputStyle}
          multiline={true}
        />
        {errors.eventDetails && <S2>Event details shouldn't be empty !</S2>}
        <Separator />
        <Col>
          <ButtonSuccess width={1 / 1.5} onPress={handleSubmit(onSubmit)}>
            Create Event
          </ButtonSuccess>
          <ButtonGhost
            width={1 / 1.5}
            onPress={() => props.navigation.goBack()}
          >
            Back
          </ButtonGhost>
        </Col>
        <Separator />
        <Modal
          animationIn="slideInUp"
          animationOut="slideOutDown"
          onBackdropPress={cancelPicker}
          isVisible={dateState.showModal}
        >
          <DateTimeModal>
            <View style={{ alignItems: "center" }}>
              <H2>Choose {dateState.mode}</H2>
            </View>
            <DateTimePicker
              value={dateState.dateValue}
              mode={dateState.mode}
              display="default"
              onChange={setDate}
            />
            <View style={{ alignItems: "center" }}>
              <Button width={1 / 2} onPress={cancelPicker}>
                Done
              </Button>
              <ButtonGhost onPress={() => cancelPicker()} width={1 / 2}>
                Cancel
              </ButtonGhost>
            </View>
          </DateTimeModal>
        </Modal>
        {Platform.OS === "android" && dateState.show && (
          <DateTimePicker
            value={new Date()}
            mode={dateState.mode}
            is24Hour={true}
            display="default"
            onChange={setDate}
          />
        )}
      </ScrollView>
    </Container>
  );
};

export default addEvent;

let nameReg = /^[A-Za-z ]+$/i;
