import React, { useEffect, useState } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import { Image, Modal, View, TouchableOpacity, StyleSheet } from "react-native";
import {
  TaskListItem,
  TaskListItemComplete
} from "../task/components/taskListItem";
import { initialData } from "../task/listTask";
import moment from "moment";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import ActionButton from "react-native-action-button";
import { Feather } from "@expo/vector-icons";
import ImageViewer from "react-native-image-zoom-viewer";

const styles = StyleSheet.create({
  card: {
    marginBottom: 25,
    borderRadius: 15,
    flexShrink: 1
  },
  map: {
    marginTop: 3,
    marginLeft: 10,
    marginRight: 4
  },
  clock: {
    marginTop: 3,
    marginLeft: 10,
    marginRight: 4
  },
  calendar: {
    marginTop: 3,
    marginLeft: 10,
    marginRight: 4
  },
  image: {
    height: 150,
    width: "100%",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  dataContainer: {
    padding: 10
  },
  details: {
    marginTop: 2,
    fontFamily: "roboto-italic"
  },
  header: {
    paddingTop: Platform.OS === "ios" ? 40 : 10
  },
  image: {
    height: 400,
    width: "100%"
  },
  container: {
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    marginTop: -50
  },
  details: {
    margin: 20,
    backgroundColor: "rgba(0,0,0,0)"
  },
  club: {
    marginLeft: 10,
    marginBottom: 5
  },
  desc: {
    marginLeft: 10,
    marginBottom: 10
  }
});

const eventDetails = props => {
  let item = props.navigation.getParam("item", "NO-ID");
  const [imgVisible, setImgVisible] = useState(false);
  const [data, setData] = useState(initialData);
  const theme = useSelector(state => state.themeReducer.theme);
  const dispatch = useDispatch();
  const toggleComplete = id => {
    let tempData = data;
    const objIndex = tempData.findIndex(obj => obj.id == id);
    tempData[objIndex].complete = !data[objIndex].complete;

    setData([...tempData]);
  };
  const images = [
    {
      props: {
        source: item.image
      }
    }
  ];
  return (
    <>
      <ParallaxScrollView
        parallaxHeaderHeight={400}
        stickyHeaderHeight={Platform.OS === "ios" ? 80 : 60}
        backgroundColor={theme.PRIMARY_BACKGROUND_COLOR}
        renderStickyHeader={() => (
          <Header
            styles={styles.header}
            title={item.name}
            nav={props.navigation}
          />
        )}
        renderForeground={() => (
          <TouchableOpacity onPress={() => setImgVisible(true)}>
            <View style={styles.image} />
          </TouchableOpacity>
        )}
        renderBackground={() => (
          <TouchableOpacity onPress={() => setImgVisible(true)}>
            <Image style={styles.image} source={item.image} />
          </TouchableOpacity>
        )}
      >
        <Container style={styles.container}>
          <View style={styles.details}>
            <H1>{item.name}</H1>
            <S2 style={styles.club}>{item.club}</S2>
            <P1 style={styles.desc}>{item.description}</P1>
            <Row>
              <Feather
                name="calendar"
                size={10}
                style={styles.calendar}
                color={theme.PRIMARY_TEXT_COLOR}
              />

              <P1>{item.date}</P1>
            </Row>
            <Row>
              <Feather
                name="clock"
                size={10}
                style={styles.clock}
                color={theme.PRIMARY_TEXT_COLOR}
              />
              <P1>
                {item.startTime} ~ {item.endTime}
              </P1>
            </Row>
            <Row>
              <Feather
                name="map-pin"
                size={10}
                style={styles.map}
                color={theme.PRIMARY_TEXT_COLOR}
              />
              <P1>{item.venue}</P1>
            </Row>

            <Separator />
            <H2>Incomplete</H2>
            {data
              .filter(a => !a.complete)
              .sort(
                (a, b) =>
                  moment(a.dueDate, "DD/MM/YYYY") -
                  moment(b.dueDate, "DD/MM/YYYY")
              )
              .map(item => (
                <TaskListItem
                  item={item}
                  theme={theme}
                  toggleComplete={toggleComplete}
                />
              ))}
            <Separator />
            <H2>Completed</H2>
            {data
              .filter(a => a.complete)
              .map(item => TaskListItemComplete(item, theme))}
            <Separator />
          </View>
        </Container>
      </ParallaxScrollView>
      <Modal visible={imgVisible} transparent={true}>
        <ImageViewer
          enableSwipeDown
          onSwipeDown={() => setImgVisible(false)}
          imageUrls={images}
          renderIndicator={() => <></>}
        />
      </Modal>
      <ActionButton
        buttonColor={theme.PRIMARY_TEXT_COLOR}
        renderIcon={() => (
          <Feather
            name="plus"
            size={20}
            color={theme.PRIMARY_BACKGROUND_COLOR}
          />
        )}
        onPress={() => {
          props.navigation.navigate("AddTask");
        }}
      />
    </>
  );
};

export default eventDetails;
