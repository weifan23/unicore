import React, { useState, useEffect } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { View, Alert, Platform } from "react-native";
import Modal from "react-native-modal";
import { storeToken, switchTheme } from "root/src/redux/actions";
import DateTimePicker from "@react-native-community/datetimepicker";
import styled from "styled-components/native";
import { space, layout, typography, color } from "styled-system";
import moment from "moment";

import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  LargeInput,
  Separator,
  ButtonText
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import useForm from "react-hook-form";
const Horizontal = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const addTask = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const dispatch = useDispatch();
  const DateTimeInput = styled.TouchableOpacity`
    background-color: ${theme.INPUT_COLOR};
    font-family: roboto-slab-bold;
    font-size: 10px;
    color: ${theme.PRIMARY_TEXT_COLOR};
    padding: 5px;
    border-radius: 8px;
    align-items: center;
    margin-vertical: 10px;
    ${space}

    ${layout}
  `;

  const DateTimeModal = styled.View`
    padding: 10px;
    border-radius: 15px;
    height: 52%;
    background-color: ${props => props.theme.PRIMARY_BACKGROUND_COLOR};
  `;

  const { register, setValue, handleSubmit, errors } = useForm();
  const onSubmit = data => console.log("Form Data", data);
  useEffect(() => {
    register({ name: "date" });
    register({ name: "time" });
    register({ name: "name" }, { required: true, minLength: 6 });
    register({ name: "venue" }, { required: true });
    register({ name: "agenda" }, { required: true });
  }, [register]);

  const [dateState, setDateState] = useState({
    date: new Date(),
    mode: "date",
    show: false,
    showModal: false,
    dateValue: new Date()
  });

  const cancelPicker = () => {
    setDateState(prev => ({
      ...prev,
      showModal: Platform.OS === "ios" ? false : true
    }));
  };

  const setDate = (event, date) => {
    date = date || dateState.date;

    setDateState(prev => ({
      ...prev,
      show: Platform.OS === "ios" ? true : false,
      date,
      dateValue: date
    }));
  };

  const show = () => {
    setDateState(prev => ({
      ...prev,
      show: true,
      showModal: Platform.OS === "ios" ? true : false
    }));
  };

  return (
    <Container>
      <Header title="Add Task" back={true} nav={props.navigation} />

      <ScrollView>
        <LargeInput
          width={1}
          placeholder="Task Title"
          onChangeText={text => setValue("name", text)}
        />
        <DateTimeInput width={1} onPress={show}>
          <H4>{moment(dateState.date).format("DD/MM/YYYY")}</H4>
        </DateTimeInput>
        <Modal
          animationIn="slideInUp"
          animationOut="slideOutDown"
          onBackdropPress={cancelPicker}
          isVisible={dateState.showModal}
        >
          <DateTimeModal>
            <View style={{ alignItems: "center" }}>
              <H2>Choose {dateState.mode}</H2>
            </View>
            <DateTimePicker
              value={dateState.dateValue}
              mode={dateState.mode}
              is24Hour={true}
              display="default"
              onChange={setDate}
            />
            <View style={{ alignItems: "center" }}>
              <Button width={1 / 2} onPress={() => cancelPicker()}>
                Done
              </Button>
              <ButtonGhost onPress={() => cancelPicker()} width={1 / 2}>
                Cancel
              </ButtonGhost>
            </View>
          </DateTimeModal>
        </Modal>
        <Button
          width={1}
          onPress={() => props.navigation.navigate("SelectUser")}
        >
          Assign Members
        </Button>
        <Separator />

        {Platform.OS === "android" && dateState.show && (
          <DateTimePicker
            value={new Date()}
            mode={dateState.mode}
            is24Hour={true}
            display="default"
            onChange={setDate}
          />
        )}
        <View style={{ alignItems: "center" }}>
          <ButtonSuccess width={1 / 1.5} onPress={handleSubmit(onSubmit)}>
            Done
          </ButtonSuccess>
          <ButtonGhost
            width={1 / 1.5}
            onPress={() => props.navigation.goBack()}
          >
            Back
          </ButtonGhost>
        </View>
      </ScrollView>
    </Container>
  );
};

export default addTask;
