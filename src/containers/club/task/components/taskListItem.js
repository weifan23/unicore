import React from "react";
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import moment from "moment";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  Card,
  Col
} from "root/src/styles/components";
import { Feather } from "@expo/vector-icons";
import * as Progress from "react-native-progress";
import { MenuProvider } from "react-native-popup-menu";
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from "react-native-popup-menu";

import styled from "styled-components/native";

const Row = styled.View`
  flex-direction: row;
`;

const DaysLeftContainer = styled.View`
  margin-horizontal: 10px;
  align-items: center;
`;

const DataContainer = styled.View`
  width: 80%;
`;

const checkZero = num => {
  return num >= 0 ? num : 0;
};

const styles = StyleSheet.create({
  title: {
    marginTop: 3
  },
  tasksTopList: {
    justifyContent: "space-between"
  },
  card: {
    padding: 10,
    marginTop: 10,
    borderRadius: 10,
    flexShrink: 1
  },
  month: {
    marginBottom: -7
  },
  users: {
    marginRight: 5
  },
  userList: {
    fontFamily: "roboto-italic",
    marginLeft: 3,
    marginRight: 0
  },
  button: {
    marginBottom: 0
  },
  progBar: {
    marginVertical: 5
  },
  cardTextContainer: {
    marginTop: 5
  }
});

export const TaskListItem = props => {
  const { item, theme } = props;
  return (
    <Card key={item.id} style={styles.card}>
      <Row style={styles.cardTextContainer}>
        <DaysLeftContainer>
          <H1 style={styles.month}>
            {checkZero(
              moment(item.dueDate, "DD/MM/YYYY").diff(
                moment(new Date()),
                "days"
              )
            )}
          </H1>
          <P2>days left</P2>
        </DaysLeftContainer>
        <DataContainer>
          <MenuProvider>
            <Row style={styles.tasksTopList}>
              <S2>{item.under}</S2>
              <Col>
                <Menu>
                  <MenuTrigger>
                    <Feather
                      name="more-vertical"
                      size={15}
                      color={theme.PRIMARY_TEXT_COLOR}
                    />
                  </MenuTrigger>
                  <MenuOptions>
                    <MenuOption
                      onSelect={() => alert(`Added to your calendar.`)}
                      text="Remind Me"
                    />
                  </MenuOptions>
                </Menu>
              </Col>
            </Row>

            <H4>{item.description}</H4>
            <Row style={styles.cardTextContainer}>
              <Feather
                name="users"
                size={10}
                style={styles.users}
                color={theme.PRIMARY_TEXT_COLOR}
              />
              {item.delegation.slice(0, 3).map(i => (
                <P2 style={styles.userList} key={i}>
                  {i}
                </P2>
              ))}
              {item.delegation.length > 3 && (
                <P2 style={styles.userList}>
                  and {item.delegation.length - 3} others
                </P2>
              )}
            </Row>
          </MenuProvider>
        </DataContainer>
      </Row>
      {!item.complete ? (
        <Button
          style={styles.button}
          onPress={() => props.toggleComplete(item.id)}
        >
          Complete Task
        </Button>
      ) : (
        <ButtonOutline style={styles.button}>Completed</ButtonOutline>
      )}
    </Card>
  );
};

export const TaskListItemComplete = (item, theme) => {
  return (
    <Card key={item.id} style={styles.card}>
      <Row>
        <DaysLeftContainer>
          <H1 style={styles.month}>
            {checkZero(
              moment(item.dueDate, "DD/MM/YYYY").diff(
                moment(new Date()),
                "days"
              )
            )}
          </H1>
          <P2>days left</P2>
        </DaysLeftContainer>
        <DataContainer>
          <S2>{item.under}</S2>

          <H4>{item.description}</H4>
          <Row style={styles.cardTextContainer}>
            <Feather
              name="users"
              size={10}
              style={styles.users}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            {item.delegation.slice(0, 3).map(i => (
              <P2 style={styles.userList} key={i}>
                {i}
              </P2>
            ))}
            {item.delegation.length > 3 && (
              <P2 style={styles.userList}>
                and {item.delegation.length - 3} others
              </P2>
            )}
          </Row>
        </DataContainer>
      </Row>
    </Card>
  );
};

export const AssignTaskListItem = props => {
  const { item, theme } = props;
  return (
    <Card key={item.id} style={styles.card}>
      <Row>
        <DaysLeftContainer>
          <H1 style={styles.month}>
            {checkZero(
              moment(item.dueDate, "DD/MM/YYYY").diff(
                moment(new Date()),
                "days"
              )
            )}
          </H1>
          <P2>days left</P2>
        </DaysLeftContainer>
        <DataContainer>
          <S2>{item.under}</S2>

          <H4>{item.description}</H4>
          <Row style={styles.cardTextContainer}>
            <Feather
              name="users"
              size={10}
              style={styles.users}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            {item.delegation.slice(0, 3).map(i => (
              <P2 style={styles.userList} key={i}>
                {i}
              </P2>
            ))}
            {item.delegation.length > 3 && (
              <P2 style={styles.userList}>
                and {item.delegation.length - 3} others
              </P2>
            )}
          </Row>
          {item.completed.length / item.delegation.length === 1 ? (
            <Button
              style={styles.button}
              onPress={() => props.toggleComplete(item.id)}
            >
              Approve Completion
            </Button>
          ) : (
            <View style={styles.progBar}>
              <Progress.Bar
                color={theme.PRIMARY_TEXT_COLOR}
                progress={item.completed.length / item.delegation.length}
                height={10}
                width={Dimensions.get("window").width * 0.65}
              />
            </View>
          )}
        </DataContainer>
      </Row>
    </Card>
  );
};
