import React, { useState, Fragment } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import { View, StyleSheet } from "react-native";
import moment from "moment";

import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";

import {
  TaskListItem,
  TaskListItemComplete,
  AssignTaskListItem
} from "./components/taskListItem";
import styled from "styled-components/native";

import ActionButton from "react-native-action-button";
import { TouchableOpacity } from "react-native-gesture-handler";

// Normal member & Core/Admin
const credential = {
  user: "James",
  roleClub: "committee"
};

export const initialData = [
  {
    id: "233",
    assignDate: "3/12/2019",
    dueDate: "30/12/2019",
    description: "Design Event Poster",
    assignedBy: "James",
    delegation: ["Sam", "John"],
    progress: 0.2,
    under: "Career Tech Talk",
    complete: false,
    completed: ["John"]
  },
  {
    id: "244",
    assignDate: "3/12/2019",
    dueDate: "30/12/2019",
    description: "Design Event Poster",
    assignedBy: "James",
    delegation: ["Sam", "John"],
    progress: 0.2,
    under: "Career Tech Talk",
    complete: false,
    completed: ["Sam", "John"]
  },
  {
    id: "232",
    assignDate: "3/12/2019",
    dueDate: "29/12/2019",
    description: "Prepare Copywriting for Event",
    assignedBy: "John",
    delegation: ["James", "Sam"],
    progress: 0.2,
    under: "Android Workshop",
    complete: false,
    completed: ["James"]
  },
  {
    id: "323",
    assignDate: "1/12/2019",
    dueDate: "25/12/2019",
    description: "Book Venue for Rehearsal",
    assignedBy: "Sarah",
    delegation: ["Frank", "Pete", "Jack", "Sarah", "Paula", "James"],
    progress: 0.2,
    under: "Android Workshop",
    complete: false,
    completed: ["Frank", "Jack", "James"]
  },
  {
    id: "325",
    assignDate: "1/12/2019",
    dueDate: "2/12/2019",
    description: "Write and Submit Proposal",
    assignedBy: "James",
    delegation: ["Frank", "Pete", "Jack", "Sarah", "Paula", "James"],
    progress: 0.2,
    under: "Android Workshop",
    complete: true,
    completed: ["Frank", "Jack", "James"]
  },
  {
    id: "125",
    assignDate: "1/12/2019",
    dueDate: "1/12/2019",
    description: "Create Event Registration Form",
    assignedBy: "James",
    delegation: ["Frank", "Pete", "Jack", "Sarah", "Paula", "James"],
    progress: 0.2,
    under: "Android Workshop",
    complete: true,
    completed: ["Frank", "Jack", "James"]
  }
];

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: "row",
    alignSelf: "center"
  },
  button: {
    marginBottom: 0
  }
});

const task = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const [expandComplete, setExpandComplete] = useState(false);
  const [expandAssignedComplete, setExpandAssignedComplete] = useState(false);
  const [data, setData] = useState(initialData);
  const toggleComplete = id => {
    let tempData = data;
    const objIndex = tempData.findIndex(obj => obj.id == id);
    tempData[objIndex].complete = !data[objIndex].complete;

    setData([...tempData]);
  };
  const SwitchRender = () => {
    switch (credential.roleClub) {
      case "member":
        return (
          <Fragment>
            <H2>Your Task</H2>
            {data
              .filter(a => !a.complete)
              .filter(a => a.delegation.includes(credential.user))
              .sort(
                (a, b) =>
                  moment(a.dueDate, "DD/MM/YYYY") -
                  moment(b.dueDate, "DD/MM/YYYY")
              )
              .map(item => (
                <TaskListItem
                  item={item}
                  theme={theme}
                  toggleComplete={toggleComplete}
                />
              ))}

            <View style={styles.buttonContainer}>
              {expandComplete ? (
                <ButtonOutline
                  width={1 / 2}
                  style={styles.button}
                  onPress={() => setExpandComplete(false)}
                >
                  Hide completed
                </ButtonOutline>
              ) : (
                <ButtonOutline
                  width={1 / 2}
                  style={styles.button}
                  onPress={() => setExpandComplete(true)}
                >
                  Show completed
                </ButtonOutline>
              )}
            </View>

            {expandComplete ? (
              data
                .filter(a => a.delegation.includes(credential.user))
                .filter(a => a.complete)
                .sort(
                  (a, b) =>
                    moment(b.dueDate, "DD/MM/YYYY") -
                    moment(a.dueDate, "DD/MM/YYYY")
                )
                .map(item => TaskListItemComplete(item, theme))
            ) : (
              <Fragment />
            )}
          </Fragment>
        );

      case "committee":
        return (
          <Fragment>
            <H2>Your Task</H2>

            {data
              .filter(a => !a.complete)
              .filter(a => a.delegation.includes(credential.user))
              .sort(
                (a, b) =>
                  moment(a.dueDate, "DD/MM/YYYY") -
                  moment(b.dueDate, "DD/MM/YYYY")
              )
              .map(item => (
                <TaskListItem
                  item={item}
                  theme={theme}
                  toggleComplete={toggleComplete}
                />
              ))}

            <View style={styles.buttonContainer}>
              {expandComplete ? (
                <ButtonOutline
                  width={1 / 2}
                  style={styles.button}
                  onPress={() => setExpandComplete(false)}
                >
                  Hide completed
                </ButtonOutline>
              ) : (
                <ButtonOutline
                  width={1 / 2}
                  style={styles.button}
                  onPress={() => setExpandComplete(true)}
                >
                  Show completed
                </ButtonOutline>
              )}
            </View>

            {expandComplete ? (
              data
                .filter(a => a.delegation.includes(credential.user))
                .filter(a => a.complete)
                .sort(
                  (a, b) =>
                    moment(b.dueDate, "DD/MM/YYYY") -
                    moment(a.dueDate, "DD/MM/YYYY")
                )
                .map(item => TaskListItemComplete(item, theme))
            ) : (
              <Fragment />
            )}

            <Separator />

            <H2>Assigned Task</H2>
            {data
              .filter(a => !a.complete)
              .filter(a => a.assignedBy == credential.user)
              .sort(
                (a, b) =>
                  moment(b.dueDate, "DD/MM/YYYY") -
                  moment(a.dueDate, "DD/MM/YYYY")
              )
              .map(item => (
                <AssignTaskListItem
                  item={item}
                  theme={theme}
                  toggleComplete={toggleComplete}
                />
              ))}

            <View style={styles.buttonContainer}>
              {expandAssignedComplete ? (
                <ButtonOutline
                  width={1 / 2}
                  style={styles.button}
                  onPress={() => setExpandAssignedComplete(false)}
                >
                  Hide completed
                </ButtonOutline>
              ) : (
                <ButtonOutline
                  width={1 / 2}
                  style={styles.button}
                  onPress={() => setExpandAssignedComplete(true)}
                >
                  Show completed
                </ButtonOutline>
              )}
            </View>

            {expandAssignedComplete ? (
              data
                .filter(a => a.assignedBy == credential.user)
                .filter(a => a.complete)
                .sort(
                  (a, b) =>
                    moment(b.dueDate, "DD/MM/YYYY") -
                    moment(a.dueDate, "DD/MM/YYYY")
                )
                .map(item => TaskListItemComplete(item, theme))
            ) : (
              <Fragment />
            )}
          </Fragment>
        );
        break;
    }
  };

  return (
    <Container>
      <Header title="Tasks" nav={props.navigation} />
      <ScrollView>
        <SwitchRender />
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default task;
