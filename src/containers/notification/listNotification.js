import React, { useState, Fragment } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import { View, StyleSheet } from "react-native";
import moment from "moment";

import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";

import styled from "styled-components/native";

import ActionButton from "react-native-action-button";
import { TouchableOpacity } from "react-native-gesture-handler";

import { NotificationListItem } from "./components/notificationListItem";

const initialData = [
  {
    id: "123",
    notificationType: "task",
    dueDate: "6/1/2020",
    title: "Book Venue for Rehearsal",
    description: "Book the venue for rehearsal",
    under: "Android Workshop",
    link: "#"
  },
  {
    id: "124",
    notificationType: "event",
    dueDate: "7/1/2020",
    title: "Event is coming soon",
    description: "The event that you are going to is coming soon",
    under: "iOS Workshop",
    link: "#"
  },
  {
    id: "125",
    notificationType: "update",
    dueDate: "14/1/2020",
    title: "New event announced!",
    description:
      "Fish Taco Club is proud to present you the Taco Making Workshop, the best workshop to ever exist to guide and teach you how to make the best Fish Taco in the whole world.",
    under: "Fish Taco Club",
    link: "#"
  },
  {
    id: "126",
    notificationType: "urgent",
    dueDate: "6/1/2020",
    title: "Payment system compromised",
    description:
      "The payment gateway provider that we are using had reported that it was compromised on 5th of January 2020, therefore, we have suspended all the payment system until the problem is rectified.",
    under: "XXX Financial Department",
    link: "#"
  },
  {
    id: "127",
    notificationType: "due",
    dueDate: "14/2/2020",
    title: "An assignment is due soon!",
    description: "PRG1014 assignment 1 is due soon.",
    under: "Programming Principles",
    link: "#"
  }
];

const notification = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const [data, _] = useState(initialData);

  const NotificationRender = () => {
    return (
      <Fragment>
        {/* Display urgent stuff first */}
        {data
          .filter(a => a.notificationType.includes("urgent"))
          .map(item => (
            <NotificationListItem item={item} theme={theme} urgent={true} />
          ))}
        {/* Everything else that is not urgent */}
        {data
          .filter(a => !a.notificationType.includes("urgent"))
          .map(item => (
            <NotificationListItem item={item} theme={theme} />
          ))}
      </Fragment>
    );
  };

  return (
    <Container>
      <Header title="Notifications" nav={props.navigation} />
      <ScrollView>
        <NotificationRender />
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default notification;
