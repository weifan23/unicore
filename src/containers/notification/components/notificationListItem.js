import React, { useState } from "react";
import { View, Text, Dimensions, StyleSheet } from "react-native";
import moment from "moment";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  Card
} from "root/src/styles/components";
import { Feather } from "@expo/vector-icons";
import * as Progress from "react-native-progress";

import styled from "styled-components/native";
import { colorCode } from "root/src/styles/theme.js";

const Row = styled.View`
  flex-direction: row;
`;

const DaysLeftContainer = styled.View`
  margin-horizontal: 10px;
  align-items: center;
`;

const DataContainer = styled.View`
  width: 80%;
`;

const checkZero = num => {
  return num >= 0 ? num : 0;
};

const styles = StyleSheet.create({
  title: {
    marginTop: 3
  },
  card: {
    padding: 12,
    marginTop: 15,
    borderRadius: 10,
    flexShrink: 1
  },
  month: {
    marginBottom: -7
  },
  users: {
    marginRight: 5
  },
  userList: {
    fontFamily: "roboto-italic",
    marginLeft: 3,
    marginRight: 0
  },
  button: {
    marginBottom: 0
  },
  progBar: {
    marginVertical: 5
  },
  cardTextContainer: {
    marginTop: 5
  },
  urgentCard: {
    backgroundColor: colorCode.DANGER_COLOR
  }
});

export const NotificationListItem = props => {
  const { item, theme, urgent } = props;

  let cardStyle = [styles.card];

  if (urgent) {
    cardStyle.push(styles.urgentCard);
  }

  return (
    <Card key={item.id} style={cardStyle}>
      <Row style={styles.cardTextContainer}>
        <DataContainer>
          <S2>{item.under}</S2>
          <H4>{item.title}</H4>
          <P1 numberOfLines={1}>{item.description}</P1>
          <Row style={styles.cardTextContainer}>
            <Feather
              name="clock"
              size={10}
              style={styles.users}
              color={theme.PRIMARY_TEXT_COLOR}
            />
            <P2 style={styles.userList}>
              {moment(item.dueDate, "D/M/YYYY").fromNow()}
            </P2>
          </Row>
        </DataContainer>
      </Row>
      {/* <Button>View</Button> */}
    </Card>
  );
};
