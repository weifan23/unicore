import React from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";

const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const dispatch = useDispatch();

  return (
    <Container>
      <Header title="Academics" nav={props.navigation} />

      <ScrollView>
        <H1>H1 Home 32px</H1>
        <H2>H2 Headline 24px</H2>
        <H4>H4 Headline 16px</H4>
        <S1>S1 Subtitle 12px</S1>
        <S2>S2 Subtitle 10px</S2>
        <P1>P1 Paragraph 12px</P1>
        <P2>P2 Paragraph 10px</P2>

        <Separator />

        <Input width={1 / 4} placeholder="Input Text" />

        <Input width={1 / 2} placeholder="Input Text" />

        <Input width={1} placeholder="Input Text" />

        <Separator />
        <Button width={1 / 4}>Normal</Button>

        <ButtonSuccess width={1 / 4}> Success</ButtonSuccess>

        <ButtonDanger width={1 / 4}> Danger </ButtonDanger>
        <ButtonOutline width={1 / 4}> Outline</ButtonOutline>
        <ButtonGhost width={1 / 4}> Ghost</ButtonGhost>

        <Separator />

        <Button width={1 / 4}>25%</Button>
        <Button width={1 / 2}>50%</Button>
        <Button width={1}>100%</Button>

        <Separator />

        {theme.mode === "light" ? (
          <Button
            width={1 / 2}
            onPress={() => dispatch(switchTheme(darkTheme))}
          >
            Switch to Dark Theme
          </Button>
        ) : (
          <ButtonOutline
            width={1 / 2}
            onPress={() => dispatch(switchTheme(lightTheme))}
          >
            Switch to Light Theme
          </ButtonOutline>
        )}

        <Separator />
      </ScrollView>
    </Container>
  );
};

export default login;
