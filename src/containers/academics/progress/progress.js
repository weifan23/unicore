import React, { useState } from "react";
import ProgressCircle from "react-native-progress-circle";
import { View, StyleSheet } from "react-native";
import Accordion from "react-native-collapsible/Accordion";
import SubjectListItem from "./components/subjectList";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H3,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import { Feather } from "@expo/vector-icons";
import styled from "styled-components";
import { TouchableOpacity } from "react-native-gesture-handler";

const userData = {
  name: "Ding Ding",
  semester: "21",
  program: "BSc (Hons) in Juggling"
};

const data = [
  {
    title: "In Progress",
    id: 0,
    content: [
      {
        id: "125",
        subjectcode: "CSC 1222",
        name: "Computer Building",
        credit: 4,
        score: "",
        courseWork: "",
        finals: "",
        grade: ""
      },
      {
        id: "126",
        subjectcode: "JUG 1224",
        name: "Juggling Principles ",
        credit: 4,
        score: "",
        courseWork: "",
        finals: "",
        grade: ""
      }
    ]
  },
  {
    title: "Incomplete",
    id: 1,
    content: [
      {
        id: "127",
        subjectcode: "CSC 1202",
        name: "Computer Organisation",
        credit: 4,
        score: "",
        courseWork: "",
        finals: "",
        grade: ""
      },
      {
        id: "128",
        subjectcode: "CSC 1024",
        name: "Programming Principles",
        credit: 4,
        score: "",
        courseWork: "",
        finals: "",
        grade: ""
      }
    ]
  },
  {
    title: "Completed",
    id: 2,
    content: [
      {
        id: "129",
        subjectcode: "ENG 1044",
        name: "English For Computer Technology Studies",
        credit: 4,
        score: "75",
        courseWork: "35",
        finals: "40",
        grade: "B"
      },
      {
        id: "130",
        subjectcode: "MTH 1114",
        name: "Computer Mathematics",
        credit: 4,
        score: "75",
        courseWork: "45",
        finals: "30",
        grade: "B"
      }
    ]
  }
];

const calculateProgress = (
  (100 * data[1].content.length) /
  (data[0].content.length + data[1].content.length + data[2].content.length)
).toFixed(1);

function completedCredits() {
  var completed = 0;
  data[2].content.forEach(subject => (completed += subject.credit));
  return completed;
}

function incompleteCredits() {
  var incomplete = 0;
  data[0].content.forEach(subject => (incomplete += subject.credit));
  data[1].content.forEach(subject => (incomplete += subject.credit));
  return incomplete;
}

function calculateOverallGrade() {
  var score = 0;
  data[2].content.forEach(subject => (score += parseInt(subject.score)));
  var average = score / data[1].content.length;
  return average;
}

const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const [activeSections, setActiveSections] = useState([0]);
  const [showAllScore, setShowAllScore] = useState(false);
  const [enrollment, setEnrollment] = useState(true);

  const GreenBox = styled.View`
    width: 14px;
    height: 14px;
    background-color: #baebc2;
    margin-left: 7px;
    margin-right: 7px;
  `;

  const GreyBox = styled.View`
    width: 14px;
    height: 14px;
    background-color: #9e9e9e;
    margin-left: 7px;
    margin-right: 7px;
  `;

  const _renderHeader = section => {
    return (
      <View style={styles.accordionContainer}>
        <H2 style={styles.accordion}>{section.title}</H2>
        {activeSections.includes(section.id) ? (
          <Feather
            name={"chevron-up"}
            color={theme.PRIMARY_TEXT_COLOR}
            size={24}
          />
        ) : (
          <Feather
            name={"chevron-down"}
            color={theme.PRIMARY_TEXT_COLOR}
            size={24}
          />
        )}
      </View>
    );
  };

  const _renderContent = section => {
    return (
      <View>
        {section.content.map(item => (
          <SubjectListItem
            item={item}
            title={section.title}
            showAllScore={showAllScore}
          />
        ))}
      </View>
    );
  };

  const _updateSections = _activeSections => {
    setActiveSections(_activeSections);
  };

  const styles = StyleSheet.create({
    accordionContainer: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center"
    },
    accordion: {
      paddingLeft: 10
    },
    progCircle: {
      justifyContent: "space-around",
      paddingVertical: 5
    },
    progPercent: {
      color: "#baebc2",
      fontSize: 24
    },
    sectionContainer: {
      marginVertical: 10,
      paddingBottom: 5,
      paddingTop: 5,
      borderRadius: 10
    },
    legendsContainer: {
      marginTop: 10
    },
    legendsTextCompleted: {
      color: "#baebc2",
      fontSize: 15
    },
    legendsTextIncomplete: {
      fontSize: 15
    },
    courseTitle: {
      alignSelf: "center"
    },
    overallGradesContainer: {
      borderRadius: 10,
      borderColor: theme.PRIMARY_TEXT_COLOR,
      borderWidth: 2,
      alignItems: "center",
      marginHorizontal: 50,
      paddingVertical: 5,
      marginVertical: 5
    }
  });

  return (
    <Container>
      <Header title="Progress" nav={props.navigation} />

      <ScrollView>
        <View>
          <H2 style={styles.courseTitle}>{userData.program}</H2>

          <Row style={styles.progCircle}>
            <ProgressCircle
              percent={parseInt(calculateProgress)}
              radius={80}
              borderWidth={10}
              color="#baebc2"
              bgColor={
                theme.mode === "light"
                  ? "rgba(249, 251, 255, 1)"
                  : "rgba(65, 72, 83, 1)"
              }
            >
              <H2 style={styles.progPercent}>{calculateProgress} %</H2>
            </ProgressCircle>
            <View>
              <H4>Module Credits</H4>
              <Row style={styles.legendsContainer}>
                <GreenBox />
                <S1 style={styles.legendsTextCompleted}>
                  Completed: {completedCredits()}
                </S1>
              </Row>
              <Row style={styles.legendsContainer}>
                <GreyBox />
                <S1 style={styles.legendsTextIncomplete}>
                  Incomplete: {incompleteCredits()}
                </S1>
              </Row>
            </View>
          </Row>
          {showAllScore ? (
            <View>
              <TouchableOpacity
                style={styles.overallGradesContainer}
                onPress={() => setShowAllScore(false)}
              >
                <Col>
                  <H4>Your Grade</H4>
                  <H3>{calculateOverallGrade()}</H3>
                  <P2>Click to hide</P2>
                </Col>
              </TouchableOpacity>
              {enrollment ? (
                <Col>
                  <ButtonSuccess width={1 / 2.1}>Enroll Subjects</ButtonSuccess>
                </Col>
              ) : (
                <></>
              )}
            </View>
          ) : (
            <Row style={{ justifyContent: "space-around" }}>
              <Button
                width={1 / 2.1}
                style={{ alignSelf: "center" }}
                onPress={() => setShowAllScore(true)}
              >
                Show All Grades
              </Button>
              {enrollment ? (
                <ButtonSuccess width={1 / 2.1}>Enroll Subjects</ButtonSuccess>
              ) : (
                <></>
              )}
            </Row>
          )}
        </View>
        <Accordion
          sections={data}
          activeSections={activeSections}
          renderHeader={_renderHeader}
          renderContent={_renderContent}
          onChange={_updateSections}
          sectionContainerStyle={styles.sectionContainer}
          underlayColor={theme.INPUT_COLOR}
        />
        <Separator />
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default login;
