import React, { useState } from "react";
import { View, Text, StyleSheet, Dimensions } from "react-native";
import moment from "moment";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H3,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  Card
} from "root/src/styles/components";
import { Feather } from "@expo/vector-icons";
import styled from "styled-components/native";
import { isEmpty } from "root/src/components/isEmpty";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";

const DataContainer = styled.View`
  width: 100%;
  padding-horizontal: 10px;
`;

const SubjectListItem = ({ item, title, showAllScore }) => {
  const [showSubjectGrade, setShowSubjectGrade] = useState(false);
  const styles = StyleSheet.create({
    card: {
      padding: 5,
      marginTop: 5
    },
    dataRowContainer: {
      marginTop: 5,
      paddingHorizontal: 10,
      justifyContent: "space-between"
    },
    dataColContainer: {
      width: 100
    },
    titleStyle: {
      justifyContent: "space-between"
    },
    codeStyle: {
      paddingRight: 20
    },
    subjectNameStyle: {
      width: Dimensions.get("window").width * 0.58
    },
    revealButton: {
      marginTop: 0,
      marginBottom: 0
    }
  });

  return (
    <TouchableWithoutFeedback
      onPressIn={() => setShowSubjectGrade(true)}
      onPressOut={() => setShowSubjectGrade(false)}
    >
      <Card key={item.id} style={styles.card}>
        <Row>
          <DataContainer>
            <Row style={styles.titleStyle}>
              <H4 style={styles.codeStyle}>{item.subjectcode}</H4>
              <H4 style={styles.subjectNameStyle}>{item.name}</H4>
            </Row>
            <Row style={styles.dataRowContainer}>
              <Col>
                <H4>{item.credit}</H4>
                <P1>Credit</P1>
              </Col>
              {title === "Completed" ? (
                showAllScore || showSubjectGrade ? (
                  <>
                    <Col>
                      <H4>{isEmpty(item.grade) ? "N/A" : item.grade}</H4>
                      <P1>Grade</P1>
                    </Col>
                    <Col>
                      <H4>{isEmpty(item.score) ? "N/A" : item.score}</H4>
                      <P1>Score</P1>
                    </Col>
                  </>
                ) : (
                  <>
                    <Button style={styles.revealButton} width={1 / 2}>
                      Tap to reveal score
                    </Button>
                  </>
                )
              ) : (
                <>
                  <Col>
                    <H4>{isEmpty(item.grade) ? "N/A" : item.grade}</H4>
                    <P1>Grade</P1>
                  </Col>
                  <Col>
                    <H4>{isEmpty(item.score) ? "N/A" : item.score}</H4>
                    <P1>Score</P1>
                  </Col>
                </>
              )}
            </Row>
          </DataContainer>
        </Row>
      </Card>
    </TouchableWithoutFeedback>
  );
};

export default SubjectListItem;
