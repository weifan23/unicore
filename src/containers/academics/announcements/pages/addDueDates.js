import React, { useState, useEffect, Fragment } from "react";
import {
  Container,
  ScrollView,
  Button,
  Row,
  Col,
  H1,
  H2,
  H3,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  LargeInput,
  ErrorMessage
} from "root/src/styles/components";
import { useSelector } from "react-redux";
import { StyleSheet, View, Text } from "react-native";
import styled from "styled-components/native";
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import useForm from "react-hook-form";

const initialData = [
  {
    id: "23213a",
    title: "Planning Documentation",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Ms Charis",
    subject: "Capstone Project 1",
    postDate: "01/01/2020 9:00pm",
    dueDate: "10/01/2020 3:00pm"
  },
  {
    id: "2321a",
    title: "First Draft",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Ms Charis",
    subject: "Capstone Project 1",
    postDate: "10/01/2020 3:00pm",
    dueDate: "12/01/2020 7:30pm"
  },
  {
    id: "s3213a",
    title: "Story Board",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Ms Charis",
    subject: "Web Programming",
    postDate: "10/01/2020 9:30pm",
    dueDate: "15/01/2020 12:00pm"
  },
  {
    id: "23s3a",
    title: "Building Blueprint",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Ms Charis",
    subject: "Web Programming",
    postDate: "10/01/2020 9:00pm",
    dueDate: "02/02/2020 11:59pm"
  },
  {
    id: "232da",
    title: "Juggling Video",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Mr Clowny",
    subject: "Advance Juggling",
    postDate: "12/02/2020 11:00am",
    dueDate: "10/03/2020 9:00pm"
  }
];

const DateTimeModal = styled.View`
  padding: 10px;
  border-radius: 15px;
  height: 52%;
  background-color: ${props => props.theme.PRIMARY_BACKGROUND_COLOR};
`;

const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const [dateState, setDateState] = useState({
    dueDate: new Date(),
    dueTime: new Date(),
    mode: "date",
    showAndroidDTPicker: false,
    input: "date",
    showIOSDTPicker: false
  });

  const { register, handleSubmit, getValues, setValue, errors } = useForm({
    defaultValues: {
      date: new Date(),
      time: new Date()
    }
  });
  useEffect(() => {
    register({ name: "subject" }, { required: true });
    register({ name: "title" }, { required: true });
    register({ name: "date" });
    register({ name: "time" });
  }, [register]);
  const onSubmit = () => {
    props.navigation.state.params._setAddDue({
      added: true,
      subject: getValues().subject,
      title: getValues().title,
      dueDate: moment(getValues().date).format("DD/MM/YYYY"),
      dueTime: moment(getValues().time).format("hh:mma")
    });
    props.navigation.goBack();
  };

  const dateTimePicker = (mode, input) => {
    setDateState(prev => ({
      ...prev,
      showAndroidDTPicker: true,
      showIOSDTPicker: true,
      mode,
      input
    }));
  };

  const setDate = (event, date) => {
    switch (dateState.input) {
      case "date":
        date = date || dateState.dueDate;
        setDateState(prev => ({
          ...prev,
          dueDate: date,
          showIOSDTPicker: false,
          showAndroidDTPicker: false
        }));
        setValue("date", date, true);

        break;
      case "time":
        date = date || dateState.dueTime;
        setDateState(prev => ({
          ...prev,
          dueTime: date,
          showIOSDTPicker: false,
          showAndroidDTPicker: false
        }));
        setValue("time", date, true);
        break;
    }
  };

  const TimeModal = () => {
    return (
      <Fragment>
        {Platform.OS === "android" && dateState.showAndroidDTPicker && (
          <DateTimePicker
            value={
              dateState.input === "date" ? dateState.dueDate : dateState.dueTime
            }
            mode={dateState.mode}
            display="default"
            onChange={setDate}
          />
        )}
      </Fragment>
    );
  };

  const cancelForm = () => {
    props.navigation.goBack();
  };

  const styles = StyleSheet.create({
    dateTimePickerStyle: {
      backgroundColor: theme.INPUT_COLOR,
      borderColor: theme.INPUT_COLOR
    },
    formButtons: {
      marginHorizontal: 10
    },
    dateTimePickerTextStyle: {
      fontSize: 14,
      color: theme.PRIMARY_TEXT_COLOR
    }
  });

  return (
    <Container>
      <TimeModal />
      <View style={{ flex: 1, justifyContent: "center" }}>
        <Col>
          <LargeInput
            placeholder={"Subject"}
            width={0.8}
            onChangeText={text => {
              setValue("subject", text, true);
            }}
          />
          {errors.subject && <ErrorMessage>Subject is required</ErrorMessage>}
          <LargeInput
            placeholder={"Title"}
            width={0.8}
            onChangeText={text => {
              setValue("title", text, true);
            }}
          />
          {errors.title && <ErrorMessage>Title is required</ErrorMessage>}

          <Row>
            <Button
              width={1 / 2}
              style={styles.dateTimePickerStyle}
              onPress={() => dateTimePicker("date", "date")}
            >
              <Text style={styles.dateTimePickerTextStyle}>
                Date: {moment(dateState.dueDate).format("DD/MM/YYYY")}
              </Text>
            </Button>
          </Row>
          <Row>
            <Button
              width={1 / 2}
              style={styles.dateTimePickerStyle}
              onPress={() => dateTimePicker("time", "time")}
            >
              <Text style={styles.dateTimePickerTextStyle}>
                Time: {moment(dateState.dueTime).format("hh:mm a")}
              </Text>
            </Button>
          </Row>
        </Col>
        <Separator />

        <Col>
          <ButtonSuccess
            width={1 / 1.5}
            style={styles.formButtons}
            onPress={handleSubmit(onSubmit)}
          >
            Done
          </ButtonSuccess>
          <ButtonGhost
            width={1 / 1.5}
            style={styles.formButtons}
            onPress={cancelForm}
          >
            Cancel
          </ButtonGhost>
        </Col>
      </View>
    </Container>
  );
};

export default login;
