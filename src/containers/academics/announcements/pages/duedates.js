import React, { useState, useEffect, Fragment } from "react";
import {
  Container,
  ScrollView,
  Button,
  Row,
  Col,
  H1,
  H2,
  H3,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  LargeInput,
  Card
} from "root/src/styles/components";
import { useSelector } from "react-redux";
import { View, StyleSheet } from "react-native";
import { Feather } from "@expo/vector-icons";
import styled from "styled-components/native";
import ActionButton from "react-native-action-button";
import moment from "moment";
import Modal from "react-native-modal";

const initialData = [
  {
    id: "23213a",
    title: "Planning Documentation",
    subject: "Capstone Project 1",
    dueDate: "10/04/2020 3:00pm"
  },
  {
    id: "2321a",
    title: "First Draft",
    subject: "Capstone Project 1",
    dueDate: "12/01/2020 7:30pm"
  },
  {
    id: "s3213a",
    title: "Story Board",
    subject: "Web Programming",
    dueDate: "15/01/2020 12:00pm"
  },
  {
    id: "23s3a",
    title: "Building Blueprint",
    subject: "Web Programming",
    dueDate: "02/02/2020 11:59pm"
  },
  {
    id: "232da",
    title: "Juggling Video",
    subject: "Advance Juggling",
    dueDate: "10/03/2020 9:00pm"
  }
];

const DaysLeftContainer = styled.View`
  margin-horizontal: 15px;
  align-items: center;
`;
const DataContainer = styled.View`
  margin-horizontal: 10px;
  margin-bottom: 2px;
  width: 80%;
`;

const checkZero = num => {
  return num >= 0 ? num : 0;
};

const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const [selectedID, setSelectedID] = useState();
  const [data, setData] = useState(initialData);
  const [isCompleteModalOpen, setIsCompleteModalOpen] = useState(false);
  const [addDue, setAddDue] = useState({
    added: false,
    subject: "",
    title: "",
    dueDate: new Date(),
    dueTime: new Date()
  });

  const styles = StyleSheet.create({
    dayTextStyle: {
      marginVertical: 3
    },
    cardStyle: {
      paddingHorizontal: 10,
      paddingTop: 10,
      marginBottom: 15
    },
    dataTextStyle: {
      marginVertical: 1
    },
    modalStyle: {
      backgroundColor: theme.PRIMARY_BACKGROUND_COLOR,
      borderRadius: 10
    }
  });

  // Remove completed from list
  const completeDue = () => {
    var tempData = data.filter(a => a.id != selectedID);
    setData([...tempData]);
  };

  // Add new due to list
  const addNewDue = () => {
    var tempData = data;
    tempData.push({
      id: (data.length + 1).toString(),
      title: addDue.title,
      subject: addDue.subject,
      dueDate: addDue.dueDate + " " + addDue.dueTime
    });
    setData([...tempData]);
    setAddDue({ added: false });
  };

  // Confirm Complete Modal
  const ConfirmModal = () => {
    return (
      <Modal
        isVisible={isCompleteModalOpen}
        onBackdropPress={() => setIsCompleteModalOpen(false)}
      >
        <Col style={styles.modalStyle}>
          <H2 style={{ marginVertical: 10 }}>Complete task?</H2>
          <Row>
            <View style={{ flex: 1 }}>
              <ButtonGhost
                onPress={() => {
                  setIsCompleteModalOpen(false);
                  completeDue();
                }}
              >
                <H4 style={{ color: theme.PRIMARY_TEXT_COLOR }}>Yes</H4>
              </ButtonGhost>
            </View>
            <View style={{ flex: 1 }}>
              <ButtonGhost
                onPress={() => {
                  setIsCompleteModalOpen(false);
                }}
              >
                <H4 style={{ color: theme.PRIMARY_TEXT_COLOR }}>No</H4>
              </ButtonGhost>
            </View>
          </Row>
        </Col>
      </Modal>
    );
  };

  return (
    <Container>
      {addDue.added ? addNewDue() : <Fragment />}
      <ScrollView>
        <ConfirmModal />
        {data
          .sort(
            (a, b) =>
              moment(a.dueDate, "DD/MM/YYYY h:mma") -
              moment(b.dueDate, "DD/MM/YYYY h:mma")
          )
          .map((item, index, arr) => (
            <Card key={item.id} style={styles.cardStyle}>
              <Row>
                <DaysLeftContainer>
                  <H1 style={styles.dayTextStyle}>
                    {checkZero(
                      moment(item.dueDate, "DD/MM/YYYY").diff(
                        moment(new Date()),
                        "days"
                      )
                    )}
                  </H1>
                  <P2>days left</P2>
                </DaysLeftContainer>

                <DataContainer>
                  <S1 style={styles.dataTextStyle}>{item.subject}</S1>
                  <H3 style={styles.dataTextStyle}>{item.title}</H3>
                  <Row style={styles.dataTextStyle}>
                    <Feather
                      name="calendar"
                      size={10}
                      color={theme.PRIMARY_TEXT_COLOR}
                    />
                    <P1>{item.dueDate}</P1>
                  </Row>
                </DataContainer>
              </Row>
              <Button
                onPress={() => {
                  setIsCompleteModalOpen(true);
                  setSelectedID(item.id);
                }}
              >
                Complete
              </Button>
            </Card>
          ))}
        <Separator />
      </ScrollView>

      <ActionButton
        buttonColor={theme.PRIMARY_TEXT_COLOR}
        renderIcon={() => (
          <Feather
            name="plus"
            size={20}
            color={theme.PRIMARY_BACKGROUND_COLOR}
          />
        )}
        onPress={() => {
          props.navigation.navigate("AddDueDatesScreen", {
            _setAddDue: data => setAddDue(data)
          });
        }}
      />
    </Container>
  );
};

export default login;
