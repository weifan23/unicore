import React, { useState, Fragment } from "react";
import {
  Container,
  ScrollView,
  Button,
  Col,
  Row,
  H1,
  H2,
  H3,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import { useSelector, useDispatch } from "react-redux";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Feather } from "@expo/vector-icons";
import Modal from "react-native-modal";
import moment from "moment";
import styled from "styled-components/native";
import { textAlign } from "styled-system";

const data = [
  {
    id: "23213a",
    title: "Class cancelled",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Ms Charis",
    subject: "Web Programming",
    dateTime: "03/01/2020 6:00pm"
  },
  {
    id: "2321a",
    title: "Class Replacement",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Ms Charis",
    subject: "Web Programming",
    dateTime: "10/01/2020 9:20pm"
  },
  {
    id: "s3213a",
    title: "Assignment Updates",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Ms Charis",
    subject: "Web Programming",
    dateTime: "22/10/2020 10:00pm"
  },
  {
    id: "23s3a",
    title: "Mid Term Date",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Ms Charis",
    subject: "Web Programming",
    dateTime: "12/02/2020 10:00am"
  },
  {
    id: "232da",
    title: "Class Revision",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Ms Charis",
    subject: "Web Programming",
    dateTime: "03/02/2020 9:30am"
  },
  {
    id: "432da",
    title: "Class Revision",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Ms Charis",
    subject: "Web Programming",
    dateTime: "03/02/2020 9:30am"
  },
  {
    id: "262da",
    title: "Class Revision",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Ms Charis",
    subject: "Web Programming",
    dateTime: "03/02/2020 9:30am"
  },
  {
    id: "292da",
    title: "Class Revision",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis egestas erat sed sagittis. Nam fringilla nisi quis nibh convallis luctus. Curabitur dictum turpis in pellentesque mattis. Nunc vel finibus nisi. In ut erat eget neque vestibulum fermentum. Nunc facilisis et lorem a maximus. Fusce blandit laoreet tortor, quis imperdiet nunc luctus eu. Praesent a tempus lectus. Pellentesque consectetur, lorem sed hendrerit feugiat, erat tortor ultrices erat",
    author: "Ms Charis",
    subject: "Web Programming",
    dateTime: "03/02/2020 9:30am"
  }
];

const announcement = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedID, setSelectedID] = useState();

  const CardTitle = styled.View`
    background-color: ${theme.mode === "light"
      ? "rgba(0, 0, 0, 0.15)"
      : "rgba(0, 0, 0, 0.3)"};
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    padding-vertical: 5px;

    padding-horizontal: 10px;
  `;

  const Card = styled.View`
    background-color: rgba(0, 0, 0, 0.08);
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    padding-bottom: 10px;
    margin-bottom: 10px;
    padding-horizontal: 10px;
  `;

  const styles = StyleSheet.create({
    modalStyle: {
      backgroundColor: theme.PRIMARY_BACKGROUND_COLOR,
      alignSelf: "center",
      flexDirection: "column",
      padding: 10,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10
    },
    modalTitleStyle: {
      alignSelf: "center",
      color: theme.PRIMARY_TEXT_COLOR,
      marginTop: -20,
      marginBottom: 10
    },
    modalDetailStyle: {
      color: theme.PRIMARY_TEXT_COLOR,
      fontSize: 16,
      marginLeft: 5,
      paddingTop: 0
    },
    modalContentStyle: {
      fontSize: 15,
      marginVertical: 5
    },
    cardDetailStyle: {
      marginHorizontal: 5
    },
    cardContentStyle: {
      marginLeft: 5,
      marginVertical: 10
    }
  });

  const RenderModal = () => {
    const item = data[selectedID];
    return (
      <Modal
        isVisible={isModalOpen}
        onBackdropPress={() => setIsModalOpen(false)}
      >
        <Card style={styles.modalStyle}>
          <View>
            <TouchableOpacity
              onPress={() => setIsModalOpen(false)}
              style={{ alignSelf: "flex-end" }}
            >
              <Feather name={"x"} size={20} color={theme.PRIMARY_TEXT_COLOR} />
            </TouchableOpacity>
            <H2 style={styles.modalTitleStyle}>{item.title}</H2>
          </View>

          <View style={{ marginLeft: 10 }}>
            <Row>
              <Feather
                name="calendar"
                size={14}
                color={theme.PRIMARY_TEXT_COLOR}
              />

              <S1 style={styles.modalDetailStyle}>{item.dateTime}</S1>
            </Row>
            <Row>
              <Feather name="book" size={14} color={theme.PRIMARY_TEXT_COLOR} />

              <S1 style={styles.modalDetailStyle}>{item.subject}</S1>
            </Row>
            <Row>
              <Feather name="user" size={14} color={theme.PRIMARY_TEXT_COLOR} />
              <S1 style={styles.modalDetailStyle}>{item.author}</S1>
            </Row>
          </View>
          <P1 style={styles.modalContentStyle}>{item.content}</P1>
        </Card>
      </Modal>
    );
  };

  return (
    <Container>
      <ScrollView>
        {isModalOpen ? <RenderModal /> : <Fragment />}
        {data
          .sort(
            (a, b) =>
              moment(b.dateTime, "DD/MM/YYYY h:mma") -
              moment(a.dateTime, "DD/MM/YYYY h:mma")
          )
          .map((item, index, arr) => (
            <TouchableOpacity
              key={item.id}
              onPress={() => {
                setSelectedID(arr.findIndex(obj => obj.id == item.id));
                setIsModalOpen(true);
              }}
            >
              <CardTitle>
                <H4>{item.title}</H4>
              </CardTitle>
              <Card>
                <P1 style={styles.cardContentStyle}>
                  {item.content.substring(0, 150)}...
                </P1>
                <Row>
                  <Row style={styles.cardDetailStyle}>
                    <Feather
                      name="user"
                      size={12}
                      color={theme.PRIMARY_TEXT_COLOR}
                    />
                    <S1 style={styles.cardDetailStyle}>{item.author}</S1>
                  </Row>
                  <Row style={styles.cardDetailStyle}>
                    <Feather
                      name="book"
                      size={12}
                      color={theme.PRIMARY_TEXT_COLOR}
                    />
                    <S1 style={styles.cardDetailStyle}>{item.subject}</S1>
                  </Row>
                </Row>
              </Card>
            </TouchableOpacity>
          ))}
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default announcement;
