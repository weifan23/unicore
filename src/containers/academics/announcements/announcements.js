import React from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { Dimensions, View } from "react-native";
import AnnouncementsScreen from "./pages/announcement";
import DueDatesScreen from "./pages/duedates";
import AddDueDatesScreen from "./pages/addDueDates";

const initialLayout = { width: Dimensions.get("window").width };

const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const dispatch = useDispatch();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: "first", title: "Announcements" },
    { key: "second", title: "Due Dates" }
  ]);

  const renderScene = SceneMap({
    first: AnnouncementsScreen,
    second: createAppContainer(
      createStackNavigator(
        {
          DueDatesScreen,
          AddDueDatesScreen
        },
        {
          initialRouteName: "DueDatesScreen",
          defaultNavigationOptions: {
            header: null
          }
        }
      )
    )
  });

  return (
    <Container>
      <Header title="Announcements" nav={props.navigation} />

      <TabView
        renderTabBar={props => (
          <TabBar
            {...props}
            indicatorStyle={{
              backgroundColor: theme.SECONDARY_TEXT_COLOR,
              opacity: 0.2
            }}
            activeColor={theme.PRIMARY_TEXT_COLOR}
            inactiveColor={theme.PRIMARY_TEXT_COLOR}
            style={{
              backgroundColor: "rgba(0,0,0,0)",
              color: theme.PRIMARY_TEXT_COLOR
            }}
          />
        )}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
      />
    </Container>
  );
};

export default login;
