import React, { useState, Fragment } from "react";
import { colorCode } from "root/src/styles/theme";
import {
  Container,
  ScrollView,
  Button,
  Row,
  Col,
  H1,
  H2,
  H3,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  LargeInput,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  Card
} from "root/src/styles/components";
import { Feather } from "@expo/vector-icons";
import { useSelector } from "react-redux";
import { View, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  overallDataContainerStyle: {
    marginTop: 20,
    justifyContent: "space-around"
  },
  overallDataVerticalBlockStyle: {
    justifyContent: "space-around",
    marginHorizontal: 10
  },
  overallDataBlockStyle: {
    marginBottom: 10
  },
  overallDataTextStyle: {
    marginRight: 20
  },
  iconStyle: {
    marginHorizontal: 5
  },
  cardStyle: {
    padding: 10,
    marginBottom: 10
  },
  alertContainer: {
    marginBottom: 15
  },
  subjecTitleStyle: {
    justifyContent: "space-between",
    paddingHorizontal: 5,
    marginBottom: 15
  }
});

export const AbsenceIcon = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  return (
    <Feather
      name={"x-circle"}
      color={theme.PRIMARY_TEXT_COLOR}
      size={16}
      style={styles.iconStyle}
    />
  );
};

export const AttendedIcon = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  return (
    <Feather
      name={"check-circle"}
      color={theme.PRIMARY_TEXT_COLOR}
      size={16}
      style={styles.iconStyle}
    />
  );
};

export const UpcomingIcon = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  return (
    <Feather
      name={"circle"}
      color={theme.PRIMARY_TEXT_COLOR}
      size={16}
      style={styles.iconStyle}
    />
  );
};

export const TotalIcon = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  return (
    <Feather
      name={"clipboard"}
      color={theme.PRIMARY_TEXT_COLOR}
      size={16}
      style={styles.iconStyle}
    />
  );
};

export const AlertIcon = () => {
  return (
    <Feather
      name={"alert-circle"}
      color={colorCode.DANGER_COLOR}
      size={18}
      style={styles.iconStyle}
    />
  );
};

export const CardTitle = data => {
  const subject = data.item;
  const attendanceRate = (
    (100 * subject.classAttended) /
    subject.classOccurred
  ).toFixed(1);

  if (
    subject.classOccurred / subject.classTotal > 0.5 &&
    subject.classAttended / subject.classOccurred >= 0.8
  ) {
    return (
      <View>
        <Row style={styles.subjecTitleStyle}>
          <View>
            <H3 numberOfLines={1}>{subject.subjectName}</H3>
          </View>
          <View>
            <H3>{attendanceRate}%</H3>
          </View>
        </Row>
      </View>
    );
  } else {
    return (
      <View>
        <Row style={styles.subjecTitleStyle}>
          <View>
            <H3 numberOfLines={1}>{subject.subjectName}</H3>
          </View>
          <View>
            <H3 style={{ color: colorCode.DANGER_COLOR }}>{attendanceRate}%</H3>
          </View>
        </Row>
        <Row style={styles.alertContainer}>
          <AlertIcon />
          <H4 style={{ color: colorCode.DANGER_COLOR }}>
            Attendance required to be above 80%
          </H4>
        </Row>
      </View>
    );
  }
};
