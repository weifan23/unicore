import React, { useState, Fragment } from "react";
import { colorCode } from "root/src/styles/theme";
import ProgressCircle from "react-native-progress-circle";
import {
  TotalIcon,
  AlertIcon,
  AttendedIcon,
  UpcomingIcon,
  AbsenceIcon,
  CardTitle
} from "./components/attendanceComponent";
import {
  Container,
  ScrollView,
  Button,
  Row,
  Col,
  H1,
  H2,
  H3,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  LargeInput,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  Card
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import { View, StyleSheet } from "react-native";

const data = {
  semester: "March 2019",
  subjects: [
    {
      subjectName: "Chicken Taco Making",
      classTotal: 14,
      classOccurred: 9,
      classAttended: 5
    },
    {
      subjectName: "Fish Taco Making",
      classTotal: 14,
      classOccurred: 9,
      classAttended: 8
    },
    {
      subjectName: "Beef Taco Making",
      classTotal: 14,
      classOccurred: 9,
      classAttended: 7
    }
  ]
};

function calcOverallAttendance() {
  var overallAttendance = 0;
  var overallClasses = 0;
  var overallOccurred = 0;
  var overallAttended = 0;
  data.subjects.forEach(subject => {
    overallAttendance += subject.classAttended / subject.classOccurred;
    overallAttended += subject.classAttended;
    overallOccurred += subject.classOccurred;
    overallClasses += subject.classTotal;
  });
  overallAttendance = (100 * overallAttendance) / data.subjects.length;
  return [
    overallAttendance.toFixed(1),
    overallAttended,
    overallOccurred,
    overallClasses
  ];
}

const attendance = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  var [
    overallAttendance,
    overallAttended,
    overallOccurred,
    overallClasses
  ] = calcOverallAttendance();

  const styles = StyleSheet.create({
    overallDataContainerStyle: {
      marginTop: 20,
      justifyContent: "space-around"
    },
    overallDataVerticalBlockStyle: {
      justifyContent: "space-around",
      marginHorizontal: 10
    },
    overallDataBlockStyle: {
      marginBottom: 10
    },
    overallDataTextStyle: {
      marginRight: 20
    },
    iconStyle: {
      marginHorizontal: 5
    },
    cardStyle: {
      padding: 10,
      marginBottom: 10
    },
    subjectDataContainer: {
      justifyContent: "space-around"
    }
  });

  return (
    <Container>
      <Header title="Attendance" nav={props.navigation} />
      <ScrollView>
        <Col>
          <H4 style={{ marginBottom: 15 }}>
            {data.semester} - Overall Attendance
          </H4>
          <ProgressCircle
            percent={parseInt(overallAttendance)}
            radius={80}
            borderWidth={10}
            color={colorCode.SUCCESS_COLOR}
            bgColor={
              theme.mode === "light"
                ? "rgba(249, 251, 255, 1)"
                : "rgba(65, 72, 83, 1)"
            }
          >
            <H2 style={{ color: colorCode.SUCCESS_COLOR }}>
              {overallAttendance}%
            </H2>
          </ProgressCircle>

          {/* Overall Data */}
          <Row style={styles.overallDataContainerStyle}>
            <View style={styles.overallDataVerticalBlockStyle}>
              <Row style={styles.overallDataBlockStyle}>
                <AbsenceIcon />
                <P1>Absence: </P1>
                <S1 style={styles.overallDataTextStyle}>
                  {overallClasses - overallOccurred}
                </S1>
              </Row>
              <Row style={styles.overallDataBlockStyle}>
                <UpcomingIcon />
                <P1>Upcoming: </P1>
                <S1 style={styles.overallDataTextStyle}>
                  {overallClasses - overallOccurred}
                </S1>
              </Row>
            </View>
            <View style={styles.overallDataVerticalBlockStyle}>
              <Row style={styles.overallDataBlockStyle}>
                <AttendedIcon />
                <P1>Attended: </P1>
                <S1 style={styles.overallDataTextStyle}>{overallAttended}</S1>
              </Row>

              <Row style={styles.overallDataBlockStyle}>
                <TotalIcon />
                <P1>Total Class: </P1>
                <S1 style={styles.overallDataTextStyle}>{overallClasses}</S1>
              </Row>
            </View>
          </Row>
        </Col>

        {/* Subjects Attendance */}
        <View style={{ marginTop: 10 }}>
          {data.subjects.map(item => (
            <Card key={item.subjectName} style={styles.cardStyle}>
              <CardTitle item={item} />
              <Row style={styles.subjectDataContainer}>
                <Row>
                  <AbsenceIcon />
                  <P1>{item.classOccurred - item.classAttended}</P1>
                </Row>
                <Row>
                  <AttendedIcon />
                  <P1>{item.classAttended}</P1>
                </Row>
                <Row>
                  <UpcomingIcon />
                  <P1>{item.classTotal - item.classOccurred}</P1>
                </Row>
                <Row>
                  <TotalIcon />
                  <P1>{item.classTotal}</P1>
                </Row>
              </Row>
            </Card>
          ))}
        </View>

        <Separator />
      </ScrollView>
    </Container>
  );
};

export default attendance;
