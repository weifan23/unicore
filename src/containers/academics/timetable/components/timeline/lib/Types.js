/** @format */
export var Preset;
(function (Preset) {
    Preset[Preset["SingleColumnLeft"] = 0] = "SingleColumnLeft";
    Preset[Preset["SingleColumnRight"] = 1] = "SingleColumnRight";
})(Preset || (Preset = {}));
//# sourceMappingURL=Types.js.map