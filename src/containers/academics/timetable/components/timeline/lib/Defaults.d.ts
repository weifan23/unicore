/**
 * @format
 **/
export declare const DEFAULT_LINE_WIDTH = 2;
export declare const DEFAULT_LINE_COLOR = "#000";
export declare const DEFAULT_CIRCLE_COLOR = "#000";
export declare const DEFAULT_DOT_COLOR = "#FFF";
