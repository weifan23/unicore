/**
 * @format
 **/
/// <reference types="react" />
import { RowProps, TimeProps, EventProps, TitleProps, DescriptionProps, VerticalProps, LineProps, CircleProps, DotProps } from './Types';
export declare function Row({ children, style }: RowProps): JSX.Element;
export declare function Time({ children, style, textStyle }: TimeProps): JSX.Element;
export declare function Event({ children, style }: EventProps): JSX.Element;
export declare function Title({ children, textStyle }: TitleProps): JSX.Element;
export declare function Description({ children, textStyle }: DescriptionProps): JSX.Element;
export declare function VerticalSeparator({ children, style }: VerticalProps): JSX.Element;
export declare function Line({ width, color, style }: LineProps): JSX.Element;
export declare function Circle({ color, children, style }: CircleProps): JSX.Element;
export declare function Dot({ color, style }: DotProps): JSX.Element;
