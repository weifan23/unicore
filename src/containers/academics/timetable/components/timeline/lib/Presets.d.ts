/**
 * @format
 **/
/// <reference types="react" />
import { RenderProps, Preset } from './Types';
export declare const SingleColumnLeft: ({ item, index, isLast, props }: RenderProps) => JSX.Element;
export declare const SingleColumnRight: ({ item, index, isLast, props }: RenderProps) => JSX.Element;
declare const _default: {
    [Preset.SingleColumnLeft]: ({ item, index, isLast, props }: RenderProps) => JSX.Element;
    [Preset.SingleColumnRight]: ({ item, index, isLast, props }: RenderProps) => JSX.Element;
};
export default _default;
