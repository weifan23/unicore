/**
 * @format
 **/
export const DEFAULT_LINE_WIDTH = 2;
export const DEFAULT_LINE_COLOR = '#fff';
export const DEFAULT_CIRCLE_COLOR = '#fff';
export const DEFAULT_DOT_COLOR = 'rgba(0,0,0,0)';
//# sourceMappingURL=Defaults.js.map