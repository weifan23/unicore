/**
 * @format
 **/
var __rest =
  (this && this.__rest) ||
  function(s, e) {
    var t = {};
    for (var p in s)
      if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
      for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++)
        if (e.indexOf(p[i]) < 0) t[p[i]] = s[p[i]];
    return t;
  };
import * as React from "react";
import { FlatList } from "react-native";
import {
  Row,
  Time,
  Event,
  Title,
  Description,
  VerticalSeparator,
  Line,
  Circle,
  Dot
} from "./Components";
import {
  DEFAULT_CIRCLE_COLOR,
  DEFAULT_DOT_COLOR,
  DEFAULT_LINE_COLOR,
  DEFAULT_LINE_WIDTH
} from "./Defaults";
import Presets from "./Presets";
import { Preset } from "./Types";
function uuidv4() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}
class Timeline extends React.PureComponent {
  constructor() {
    super(...arguments);
    this.renderItem = ({ item, index }) => {
      const { renderItem, preset, data } = this.props;
      const isLast = data.length - 1 === index;
      const renderProps = {
        item,
        index,
        isLast,
        props: this.props
      };
      if (renderItem) {
        return renderItem(renderProps);
      }
      const Component = Presets[preset];
      if (Component == null) {
        console.warn(
          `Invalid preset (${preset}) specified. See 'Presets' for more options.`
        );
        return null;
      }
      return <Component key={uuidv4()} {...renderProps} />;
    };
  }
  render() {
    const otherProps = __rest(this.props, []);
    return (
      <FlatList
        keyExtractor={item => uuidv4()}
        automaticallyAdjustContentInsets={false}
        {...otherProps}
        renderItem={this.renderItem}
      />
    );
  }
}
Timeline.defaultProps = {
  lineWidth: DEFAULT_LINE_WIDTH,
  lineColor: DEFAULT_LINE_COLOR,
  circleColor: DEFAULT_CIRCLE_COLOR,
  dotColor: DEFAULT_DOT_COLOR,
  endWithCircle: false,
  preset: Preset.SingleColumnLeft
};
Timeline.Row = Row;
Timeline.Time = Time;
Timeline.Event = Event;
Timeline.Title = Title;
Timeline.Description = Description;
Timeline.VerticalSeparator = VerticalSeparator;
Timeline.Line = Line;
Timeline.Circle = Circle;
Timeline.Dot = Dot;
export default Timeline;
//# sourceMappingURL=Timeline.js.map
