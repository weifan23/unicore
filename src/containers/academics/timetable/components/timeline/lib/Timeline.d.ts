/**
 * @format
 **/
import * as React from 'react';
import { ListRenderItemInfo } from 'react-native';
import { Row, Time, Event, Title, Description, VerticalSeparator, Line, Circle, Dot } from './Components';
import { TimelineProps, Preset, ItemProps } from './Types';
declare class Timeline extends React.PureComponent<TimelineProps> {
    static defaultProps: {
        lineWidth: number;
        lineColor: string;
        circleColor: string;
        dotColor: string;
        endWithCircle: boolean;
        preset: Preset;
    };
    static Row: typeof Row;
    static Time: typeof Time;
    static Event: typeof Event;
    static Title: typeof Title;
    static Description: typeof Description;
    static VerticalSeparator: typeof VerticalSeparator;
    static Line: typeof Line;
    static Circle: typeof Circle;
    static Dot: typeof Dot;
    render(): JSX.Element;
    renderItem: ({ item, index }: ListRenderItemInfo<ItemProps>) => JSX.Element | null;
}
export default Timeline;
