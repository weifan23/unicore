import React, { useState } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import { View, TouchableOpacity, StyleSheet, Dimensions } from "react-native";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H3,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  LargeInput
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import Accordion from "react-native-collapsible/Accordion";
import styled from "styled-components/native";
import { Feather } from "@expo/vector-icons";

const Row = styled.View`
  flex-direction: row;
`;

const data = [
  {
    title: "Monday",
    content: [
      {
        time: "8:00am",
        title: "Software Management",
        venue: "UW-7-3",
        type: "Lecture",
        duration: 2
      },
      {
        time: "10:00am",
        title: "Operating System",
        venue: "UE-2-16",
        type: "Practical",
        duration: 1.5
      },
      {
        time: "1:00pm",
        title: "Data Structures",
        venue: "JC Hall 1",
        type: "Lab",
        duration: 2
      },
      {
        time: "2:30pm",
        title: "Operating System",
        venue: "JC Hall 3",
        type: "Lecture",
        duration: 1
      }
    ]
  },
  {
    title: "Tuesday",
    content: [
      {
        time: "8:00am",
        title: "Software Management",
        venue: "UW-7-3",
        type: "Lecture",
        duration: 2
      },
      {
        time: "10:00am",
        title: "Operating System",
        venue: "UE-2-16",
        type: "Practical",
        duration: 1.5
      },
      {
        time: "1:00pm",
        title: "Data Structures",
        venue: "JC Hall 1",
        type: "Lab",
        duration: 2
      },
      {
        time: "2:30pm",
        title: "Operating System",
        venue: "JC Hall 3",
        type: "Lecture",
        duration: 1
      }
    ]
  },
  {
    title: "Wednesday",
    content: [
      {
        time: "8:00am",
        title: "Software Management",
        venue: "UW-7-3",
        type: "Lecture",
        duration: 2
      },
      {
        time: "10:00am",
        title: "Operating System",
        venue: "UE-2-16",
        type: "Practical",
        duration: 1.5
      },
      {
        time: "1:00pm",
        title: "Data Structures",
        venue: "JC Hall 1",
        type: "Lab",
        duration: 2
      },
      {
        time: "2:30pm",
        title: "Operating System",
        venue: "JC Hall 3",
        type: "Lecture",
        duration: 1
      }
    ]
  },
  {
    title: "Thursday",
    content: [
      {
        time: "8:00am",
        title: "Software Management",
        venue: "UW-7-3",
        type: "Lecture",
        duration: 2
      },
      {
        time: "10:00am",
        title: "Operating System",
        venue: "UE-2-16",
        type: "Practical",
        duration: 1.5
      },
      {
        time: "1:00pm",
        title: "Data Structures",
        venue: "JC Hall 1",
        type: "Lab",
        duration: 2
      },
      {
        time: "2:30pm",
        title: "Operating System",
        venue: "JC Hall 3",
        type: "Lecture",
        duration: 1
      }
    ]
  },
  {
    title: "Friday",
    content: [
      {
        time: "8:00am",
        title: "Software Management",
        venue: "UW-7-3",
        type: "Lecture",
        duration: 2
      },
      {
        time: "10:00am",
        title: "Operating System",
        venue: "UE-2-16",
        type: "Practical",
        duration: 1.5
      },
      {
        time: "1:00pm",
        title: "Data Structures",
        venue: "JC Hall 1",
        type: "Lab",
        duration: 2
      },
      {
        time: "2:30pm",
        title: "Operating System",
        venue: "JC Hall 3",
        type: "Lecture",
        duration: 1
      }
    ]
  }
];

const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  const styles = StyleSheet.create({
    icons: {
      marginTop: 2,
      marginLeft: 5,
      marginRight: 5
    },
    button: {
      marginBottom: 0
    },
    day: {
      backgroundColor: "rgba(0,0,0,0.08)",
      padding: 10
    },
    title: {
      paddingLeft: 20
    },
    dayCard: {
      paddingVertical: 10
    },
    timeContainer: {
      width: 100,
      justifyContent: "center"
    },
    time: {
      marginLeft: 30,
      fontSize: 14
    },
    subject: {
      width: Dimensions.get("window").width * 0.65,
      fontSize: 18
    },
    iconContainer: {
      alignItems: "center",
      paddingTop: 5
    },
    detailsContainer: {
      width: 70,
      flexDirection: "row"
    },
    details: {
      marginRight: 20
    },
    sectionContainer: {
      marginBottom: 10,
      paddingBottom: 5,
      paddingTop: 5,
      width: "150%",
      marginLeft: -20,
      backgroundColor: "rgba(0,0,0,0)"
    },
    horizontalLine: {
      alignSelf: "center",
      backgroundColor: theme.PRIMARY_TEXT_COLOR,
      height: 1,
      opacity: 0.1,
      width: "100%"
    }
  });

  const dispatch = useDispatch();
  const [activeSections, setActiveSections] = useState([
    new Date().getDay() - 1
  ]);

  const _renderHeader = section => {
    return (
      <View style={styles.day}>
        <H3 style={styles.title}>{section.title}</H3>
      </View>
    );
  };

  const _renderContent = section => {
    return (
      <View>
        {section.content.map((item, index) => (
          <View key={index}>
            <Row style={styles.dayCard}>
              <View style={styles.timeContainer}>
                <H4 style={styles.time}>{item.time}</H4>
              </View>
              <TouchableOpacity>
                <H4 style={styles.subject}>{item.title}</H4>
                <Row style={styles.iconContainer}>
                  <Row style={styles.detailsContainer}>
                    <Feather
                      name="map-pin"
                      size={12}
                      color={theme.PRIMARY_TEXT_COLOR}
                      style={styles.icons}
                    />
                    <P1 style={styles.details}>{item.venue}</P1>
                  </Row>

                  <Row style={styles.detailsContainer}>
                    <Feather
                      name="clipboard"
                      size={12}
                      color={theme.PRIMARY_TEXT_COLOR}
                      style={styles.icons}
                    />
                    <P1 style={styles.details}>{item.type}</P1>
                  </Row>

                  <Row style={styles.detailsContainer}>
                    <Feather
                      name="clock"
                      size={12}
                      color={theme.PRIMARY_TEXT_COLOR}
                      style={styles.icons}
                    />
                    <P1 style={styles.details}>{item.duration} hour</P1>
                  </Row>
                </Row>
              </TouchableOpacity>
            </Row>
            <View style={styles.horizontalLine} />
          </View>
        ))}
      </View>
    );
  };

  const _updateSections = _activeSections => {
    setActiveSections(_activeSections);
  };

  return (
    <Container>
      <Header title="Timetable" nav={props.navigation} />
      <ScrollView>
        <Accordion
          sections={data}
          activeSections={activeSections}
          expandMultiple={true}
          renderHeader={_renderHeader}
          renderContent={_renderContent}
          onChange={_updateSections}
          sectionContainerStyle={styles.sectionContainer}
          underlayColor={theme.INPUT_COLOR}
        />

        <Separator />

        <Separator />
      </ScrollView>
    </Container>
  );
};

export default login;
