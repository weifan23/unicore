import React, { useState, useEffect } from "react";
import { storeToken } from "root/src/redux/actions";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  LargeInput,
  ButtonGhost,
  Separator
} from "root/src/styles/components";
import { Alert, Image, KeyboardAvoidingView } from "react-native";
import { loginUser } from "root/src/services/api";
import useForm from "react-hook-form";
import { MaterialIndicator } from "react-native-indicators";

import { useSelector, useDispatch } from "react-redux";
const logos = [
  {
    path: require("root/src/assets/img/logo/lg-black.png")
  },
  {
    path: require("root/src/assets/img/logo/lg-white.png")
  }
];
const login = props => {
  const { register, setValue, handleSubmit, errors } = useForm();
  useEffect(() => {
    register({ name: "email" }, { required: true, pattern: emailReg });
    register({ name: "password" }, { required: true, minLength: 6 });
  }, [register]);
  const theme = useSelector(state => state.themeReducer.theme);
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(false);
  const onSubmit = async ({ email, password }) => {
    try {
      setIsLoading(true);
      const [res, valid] = await loginUser(email, password);
      console.log(JSON.stringify(res));
      if (valid) {
        setIsLoading(false);
        dispatch(storeToken(res.data.token));
        props.navigation.navigate("App");
      } else {
        setIsLoading(false);

        Alert.alert("Incorrect Email or Password!");
      }
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <Container>
      <ScrollView
        contentContainerStyle={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        {isLoading ? (
          <MaterialIndicator color="white" />
        ) : (
          <KeyboardAvoidingView
            style={{ width: "100%", alignItems: "center" }}
            behavior="padding"
            enabled
          >
            <Image
              style={{ width: 250, height: 150 }}
              source={theme.mode === "light" ? logos[0].path : logos[1].path}
            />
            <H1>Login</H1>

            <Separator />

            <LargeInput
              onChangeText={text => setValue("email", text, true)}
              width={1 / 1.2}
              placeholder="E-mail"
              autoCapitalize="none"
              autoCompleteType="email"
              keyboardType="email-address"
            />
            <LargeInput
              onChangeText={text => setValue("password", text, true)}
              width={1 / 1.2}
              placeholder="Password"
              autoCapitalize="none"
              secureTextEntry
              onSubmitEditing={handleSubmit(onSubmit)}
            />
            <Separator />
            <Button width={1 / 4} onPress={handleSubmit(onSubmit)}>
              Login
            </Button>
            <ButtonGhost
              width={1 / 4}
              onPress={() => props.navigation.navigate("SignUp")}
            >
              Sign Up
            </ButtonGhost>
          </KeyboardAvoidingView>
        )}
      </ScrollView>
    </Container>
  );
};

export default login;

let emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
