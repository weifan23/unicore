import React, { useState, useEffect } from "react";
import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Input,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  LargeInput
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector, useDispatch } from "react-redux";
import { KeyboardAvoidingView, Image, Alert } from "react-native";
import { registerUser } from "root/src/services/api";
import useForm from "react-hook-form";

const logos = [
  {
    path: require("root/src/assets/img/logo/lg-black.png")
  },
  {
    path: require("root/src/assets/img/logo/lg-white.png")
  }
];
const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const [isLoading, setIsLoading] = useState(false);
  const { register, setValue, handleSubmit, errors } = useForm();
  useEffect(() => {
    register({ name: "name" }, { required: true, pattern: nameReg });
    register({ name: "email" }, { required: true, pattern: emailReg });
    register({ name: "password" }, { required: true, minLength: 6 });
    register({ name: "password2" }, { required: true, minLength: 6 });
  }, [register]);

  const onSubmit = async ({ name, email, password, password2 }) => {
    if (password !== password2) {
      Alert.alert("Invalid Credentials!");
    } else {
      try {
        setIsLoading(true);
        const [res, valid] = await registerUser(email, name, password);
        console.log(res);
        if (valid) {
          setIsLoading(false);

          Alert.alert(
            "Registration",
            "Account Successfully Created!",
            [{ text: "OK", onPress: () => props.navigation.navigate("Login") }],
            { cancelable: false }
          );
        } else {
          Alert.alert("Error registering account");
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <Container>
      <ScrollView
        contentContainerStyle={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <KeyboardAvoidingView
          style={{ width: "100%", alignItems: "center" }}
          behavior="padding"
          enabled
        >
          <Image
            style={{ width: 250, height: 150 }}
            source={theme.mode === "light" ? logos[0].path : logos[1].path}
          />
          <H1>Sign Up</H1>
          <Separator />
          <LargeInput
            width={1 / 1.2}
            onChangeText={text => setValue("name", text, true)}
            placeholder="Full Name"
            autoCapitalize="words"
          />
          {errors.name && <S2>Invalid name</S2>}

          <LargeInput
            width={1 / 1.2}
            onChangeText={text => setValue("email", text, true)}
            placeholder="E-mail"
            autoCapitalize="none"
            autoCompleteType="email"
            keyboardType="email-address"
          />
          {errors.email && <S2>Invalid Email</S2>}

          <LargeInput
            width={1 / 1.2}
            secureTextEntry
            onChangeText={text => setValue("password", text, true)}
            placeholder="Password"
            autoCapitalize="none"
          />
          {errors.password && <S2>At least 6 characters</S2>}

          <LargeInput
            width={1 / 1.2}
            placeholder="Confirm Password"
            secureTextEntry
            onChangeText={text => setValue("password2", text, true)}
            onSubmitEditing={handleSubmit(onSubmit)}
            autoCapitalize="none"
          />
          {errors.password2 && <S2>At least 6 characters</S2>}

          <Separator />
          <Button width={1 / 4} onPress={handleSubmit(onSubmit)}>
            Register
          </Button>
          <ButtonGhost
            width={1 / 4}
            onPress={() => props.navigation.navigate("Login")}
          >
            Back
          </ButtonGhost>
        </KeyboardAvoidingView>
      </ScrollView>
    </Container>
  );
};

export default login;

let emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
let nameReg = /^[A-Za-z ]+$/i;
