import React, { useEffect } from "react";
import { ActivityIndicator, View } from "react-native";
import { useSelector } from "react-redux";

const loading = props => {
  useEffect(() => {
    checkToken();
  });
  const token = useSelector(state => state.tokenReducer.token);

  const checkToken = () => {
    console.log(JSON.stringify(token));
    props.navigation.navigate(token ? "App" : "Auth");
  };

  return (
    <View>
      <ActivityIndicator />
    </View>
  );
};

export default loading;
