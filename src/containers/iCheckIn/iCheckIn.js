import React, { useEffect, useState } from "react";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  LargeInput,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  ErrorMessage
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector } from "react-redux";
import useForm from "react-hook-form";
import { StyleSheet, View, Dimensions } from "react-native";
import moment from "moment";
import { isEmpty } from "root/src/components/isEmpty";
import Modal from "react-native-modal";

const data = [
  {
    class: "Taco Making",
    dateTime: "03/09/2019 8:34AM"
  },
  {
    class: "Skinning Fish",
    dateTime: "04/06/2019 8:12AM"
  },
  {
    class: "Sushi Making",
    dateTime: "04/22/2019 6:12AM"
  },
  {
    class: "Burning Peanut",
    dateTime: "04/06/2019 1:22PM"
  },
  {
    class: "Pottery Making",
    dateTime: "03/08/2019 8:16AM"
  },
  {
    class: "Juggling ",
    dateTime: "09/16/2019 12:12PM"
  }
];

const codeLength = 5;
let codeReg = /^[0-9]{0,}$/;

const login = props => {
  const theme = useSelector(state => state.themeReducer.theme);
  const defaultValues = {
    checkInCode: "1"
  };
  const { register, handleSubmit, setValue, errors, reset, watch } = useForm();
  useEffect(() => {
    register(
      { name: "checkInCode" },
      { minLength: 5, required: true, pattern: codeReg }
    );
  }, [register]);
  const values = watch();

  const [isModalOpen, setIsModalOpen] = useState(false);

  const MessageModal = () => {
    return (
      <Modal
        isVisible={isModalOpen}
        onBackdropPress={() => setIsModalOpen(false)}
      >
        <View
          style={{
            backgroundColor: theme.PRIMARY_BACKGROUND_COLOR,
            borderRadius: 10,
            alignSelf: "center",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <H2
            style={{
              marginVertical: 10,
              marginHorizontal: 20,
              color: theme.PRIMARY_TEXT_COLOR,
              fontSize: 20
            }}
          >
            Check in Success!
          </H2>
          <View style={{ flexDirection: "row" }}>
            <ButtonGhost
              style={{ flex: 1 }}
              onPress={() => reset(setIsModalOpen(false))}
            >
              <S1
                style={{
                  textAlign: "center",
                  color: theme.PRIMARY_TEXT_COLOR
                }}
              >
                OK
              </S1>
            </ButtonGhost>
          </View>
        </View>
      </Modal>
    );
  };

  const onSubmit = () => {
    if (isEmpty(errors)) setIsModalOpen(true);
  };

  const styles = StyleSheet.create({
    inputContainerStyle: {
      paddingHorizontal: 50
    },
    checkInButtonContainerStyle: {
      alignSelf: "center"
    },
    historyTitleStyle: {
      textAlign: "center",
      paddingBottom: 15,
      paddingTop: 30
    },
    historyContentContainerColStyle: {
      width: Dimensions.get("window").width / 2 - 30
    },
    historyContentContainerRowStyle: {
      justifyContent: "space-between",
      marginBottom: 3
    },
    errorMessageStyle: {
      alignSelf: "center"
    }
  });
  return (
    <Container>
      <Header title="iCheck-In" nav={props.navigation} />
      <ScrollView>
        <MessageModal />
        <View style={styles.inputContainerStyle}>
          <LargeInput
            placeholder={"Check-in Code"}
            autoFocus={true}
            onChangeText={text => {
              setValue("checkInCode", text, true);
            }}
            textAlign={"center"}
            value={values.checkInCode}
            keyboardType={"number-pad"}
            maxLength={codeLength}
            onSubmitEditing={handleSubmit(onSubmit)}
          />
        </View>
        {errors.checkInCode && (
          <ErrorMessage style={styles.errorMessageStyle}>
            5 digits required
          </ErrorMessage>
        )}

        <Row style={styles.checkInButtonContainerStyle}>
          <Button width={1 / 4} onPress={handleSubmit(onSubmit)}>
            Check In
          </Button>
        </Row>

        <View>
          <H4 style={styles.historyTitleStyle}>Check-In History</H4>
          {data
            .sort(
              (a, b) =>
                moment(b.dateTime, "DD/MM/YYYY h:mma") -
                moment(a.dateTime, "DD/MM/YYYY h:mma")
            )
            .map(item => (
              <Row
                key={item.dateTime}
                style={styles.historyContentContainerRowStyle}
              >
                <View style={styles.historyContentContainerColStyle}>
                  <P1 style={{ textAlign: "right" }}>{item.class}</P1>
                </View>
                <View style={styles.historyContentContainerColStyle}>
                  <P1>{item.dateTime}</P1>
                </View>
              </Row>
            ))}
        </View>
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default login;
