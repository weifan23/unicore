import React, { useEffect, useState } from "react";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  LargeInput,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  ErrorMessage
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector } from "react-redux";
import { StyleSheet, View, Dimensions, TouchableOpacity } from "react-native";
import SubjectIcons from "./components/subjectIcons";
import ClassRecords from "./components/classRecords";
import DayButtons from "./components/dayButtons";
import Modal from "react-native-modal";
import ScrollableTabView, {
  ScrollableTabBar,
  DefaultTabBar
} from "react-native-scrollable-tab-view";

const iCheckInStaff = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  const [dayState, setDayState] = useState("Monday");

  const styles = StyleSheet.create({
    inputContainerStyle: {
      paddingHorizontal: 50
    },
    checkInButtonContainerStyle: {
      alignSelf: "center"
    },
    listBox: {
      backgroundColor: theme.INPUT_COLOR,
      borderRadius: 15,
      marginTop: 45
    },
    followButtonContainerStyle: {
      alignSelf: "center"
    }
  });

  return (
    <Container>
      <Header title="iCheck-In" nav={props.navigation} />
      <ScrollView>
        <SubjectIcons />
        <DayButtons setDayState={setDayState} />
        <ClassRecords dayState={dayState} />
        <Separator />
      </ScrollView>
    </Container>
  );
};

export default iCheckInStaff;
