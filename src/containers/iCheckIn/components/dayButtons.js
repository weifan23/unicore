import React, { useEffect, useState } from "react";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  LargeInput,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  ErrorMessage,
  Card
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector } from "react-redux";
import { StyleSheet, View, Dimensions, TouchableOpacity } from "react-native";
import { Feather } from "@expo/vector-icons";
import styled from "styled-components/native";

const dayButtons = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  const dayList = [
    {
      day: "Mon",
      value: "Monday"
    },
    {
      day: "Tues",
      value: "Tuesday"
    },
    {
      day: "Wed",
      value: "Wednesday"
    },
    {
      day: "Thurs",
      value: "Thursday"
    },
    {
      day: "Fri",
      value: "Friday"
    }
  ];

  const styles = StyleSheet.create({
    center: {
      justifyContent: "center",
      alignItems: "center"
    },
    buttonContainer: {
      marginVertical: 20,
      justifyContent: "space-around"
    }
  });

  const Circle = styled.View`
    height: 60px;
    width: 60px;
    border-radius: 60px;
    background-color: ${props => props.theme.BUTTON_TEXT_COLOR};
  `;

  function DayButtons({ day, value }) {
    return (
      <TouchableOpacity onPress={() => props.setDayState(value)}>
        <Circle style={styles.center}>
          <P1>{day}</P1>
        </Circle>
      </TouchableOpacity>
    );
  }

  return (
    <Row style={styles.buttonContainer}>
      {dayList.map(day => (
        <DayButtons day={day.day} value={day.value} />
      ))}
    </Row>
  );
};

export default dayButtons;
