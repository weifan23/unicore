import React, { useEffect, useState } from "react";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  LargeInput,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  ErrorMessage,
  Card
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector } from "react-redux";
import { StyleSheet, View, Dimensions, TouchableOpacity } from "react-native";
import { Feather } from "@expo/vector-icons";
import styled from "styled-components/native";

const classRecords = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  const [subjectState, setSubjectState] = useState([
    {
      day: "Monday",
      records: [
        {
          id: "1",
          students: 150,
          attendees: 138,
          absents: 12,
          date: "3/1/2020",
          venue: "GC 2"
        },
        {
          id: "2",
          students: 150,
          attendees: 138,
          absents: 12,
          date: "10/1/2020",
          venue: "GC 2"
        },
        {
          id: "3",
          students: 150,
          attendees: 140,
          absents: 10,
          date: "17/1/2020",
          venue: "JC 2"
        },
        {
          id: "4",
          students: 150,
          attendees: 138,
          absents: 12,
          date: "24/1/2020",
          venue: "GC 2"
        },
        {
          id: "5",
          students: 150,
          attendees: 138,
          absents: 12,
          date: "31/2/2020",
          venue: "GC 2"
        },
        {
          id: "6",
          students: 150,
          attendees: 138,
          absents: 12,
          date: "7/2/2020",
          venue: "GC 2"
        }
      ]
    },
    {
      day: "Tuesday",
      records: [{}]
    },
    {
      day: "Wednesday",
      records: [{}]
    },
    {
      day: "Thursday",
      records: [
        {
          id: "1",
          students: 75,
          attendees: 70,
          absents: 5,
          date: "6/1/2020",
          venue: "JC 2"
        },
        {
          id: "2",
          students: 75,
          attendees: 72,
          absents: 3,
          date: "13/1/2020",
          venue: "LT 3"
        },
        {
          id: "3",
          students: 75,
          attendees: 70,
          absents: 5,
          date: "20/1/2020",
          venue: "JC 2"
        },
        {
          id: "4",
          students: 75,
          attendees: 72,
          absents: 3,
          date: "27/1/2020",
          venue: "LT 3"
        },
        {
          id: "5",
          students: 75,
          attendees: 70,
          absents: 5,
          date: "3/2/2020",
          venue: "JC 2"
        },
        {
          id: "6",
          students: 75,
          attendees: 72,
          absents: 3,
          date: "10/2/2020",
          venue: "LT 3"
        }
      ]
    },
    {
      day: "Friday",
      records: [{}]
    }
  ]);

  const styles = StyleSheet.create({
    card: {
      padding: 10,
      marginTop: 10,
      borderRadius: 10
    },
    lowerBar: {
      flex: 1,
      justifyContent: "space-between"
    },
    attendance: {
      width: 40,
      justifyContent: "space-around"
    },
    absents: {
      marginRight: 10,
      width: 35,
      justifyContent: "space-between"
    }
  });

  function SubjectRecords({ date, day, venue, absents, attendees }) {
    return (
      <TouchableOpacity>
        <Card style={styles.card}>
          <Row>
            <P1>{date}</P1>
          </Row>
          <Row>
            <H4>{day}</H4>
          </Row>
          <Row style={styles.lowerBar}>
            <P1>{venue}</P1>
            <Row>
              <Row style={styles.absents}>
                <Feather
                  name={"x-circle"}
                  color={theme.PRIMARY_TEXT_COLOR}
                  size={16}
                  style={styles.iconStyle}
                />
                <P1>{absents}</P1>
              </Row>
              <Row style={styles.attendance}>
                <Feather
                  name={"check-circle"}
                  color={theme.PRIMARY_TEXT_COLOR}
                  size={16}
                  style={styles.iconStyle}
                />
                <P1>{attendees}</P1>
              </Row>
            </Row>
          </Row>
        </Card>
      </TouchableOpacity>
    );
  }

  return (
    <View>
      {subjectState.map(subject =>
        subject.records
          .filter(
            records =>
              records.id !== undefined && subject.day === props.dayState
          )
          .map(records => (
            <SubjectRecords
              date={records.date}
              day={subject.day}
              venue={records.venue}
              absents={records.absents}
              attendees={records.attendees}
            />
          ))
      )}
    </View>
  );
};

export default classRecords;
