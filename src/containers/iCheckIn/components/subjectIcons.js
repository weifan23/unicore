import React, { useEffect, useState } from "react";
import {
  Container,
  ScrollView,
  Button,
  H1,
  H2,
  H4,
  P1,
  P2,
  S1,
  S2,
  Row,
  Col,
  Input,
  LargeInput,
  ButtonOutline,
  ButtonSuccess,
  ButtonDanger,
  ButtonGhost,
  Separator,
  ErrorMessage
} from "root/src/styles/components";
import Header from "root/src/components/header/header";
import { useSelector } from "react-redux";
import { StyleSheet, View, Dimensions, TouchableOpacity } from "react-native";
import { Feather } from "@expo/vector-icons";
import styled from "styled-components/native";

const subjectIcons = props => {
  const theme = useSelector(state => state.themeReducer.theme);

  const [subjectState, setSubjectState] = useState([
    {
      id: "123",
      subjectCode: "CSC1202",
      subjectName: "Computer Organisation",
      icon: "hard-drive",
      bgColor: "#1abc9c"
    },
    {
      id: "124",
      subjectCode: "ENG1044",
      subjectName: "English for Computer Technology Studies",
      icon: "book",
      bgColor: "#2ecc71"
    },
    {
      id: "125",
      subjectCode: "CSC1024",
      subjectName: "Programming Principles",
      icon: "activity",
      bgColor: "#3498db"
    },
    {
      id: "126",
      subjectCode: "MTH1114",
      subjectName: "Computer Mathematics",
      icon: "bar-chart-2",
      bgColor: "#9b59b6"
    }
  ]);
  const [titleState, setTitleState] = useState(subjectState[0].subjectName);

  const styles = StyleSheet.create({
    subjectIconContainer: {
      flex: 1,
      justifyContent: "space-around",
      paddingBottom: 30
    },
    subjectIcon: {
      justifyContent: "center",
      alignItems: "center",
      marginBottom: 10
    },
    icon: {
      color: theme.PRIMARY_TEXT_COLOR
    }
  });

  const Circle = styled.View`
    height: 50px;
    width: 50px;
    border-radius: 50px;
  `;

  function SubjectIcon({ subjectCode, subjectName, icon, bgColor }) {
    const changeTitle = () => {
      setTitleState(subjectName);
    };
    return (
      <TouchableOpacity onPress={changeTitle}>
        <Col>
          <Circle style={[styles.subjectIcon, { backgroundColor: bgColor }]}>
            <Feather size={20} name={icon} style={styles.icon} />
          </Circle>
          <S1>{subjectCode}</S1>
        </Col>
      </TouchableOpacity>
    );
  }

  return (
    <View>
      <Row style={styles.subjectIconContainer}>
        {subjectState.map(subject => (
          <SubjectIcon
            subjectCode={subject.subjectCode}
            subjectName={subject.subjectName}
            icon={subject.icon}
            bgColor={subject.bgColor}
          />
        ))}
      </Row>
      <Row>
        <H4>{titleState}</H4>
      </Row>
    </View>
  );
};

export default subjectIcons;
