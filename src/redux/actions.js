// define type
export const SWITCH_THEME = "SWITCH_THEME";
export const STORE_TOKEN = "STORE_TOKEN";
// dispatch actions
export const switchTheme = BaseTheme => {
  return dispatch => {
    dispatch({
      type: SWITCH_THEME,
      baseTheme: BaseTheme
    });
  };
};

export const storeToken = UserToken => {
  return dispatch => {
    dispatch({
      type: STORE_TOKEN,
      userToken: UserToken
    });
  };
};
