import themeReducer from "./themeReducer";
import tokenReducer from "./tokenReducer";

import { AsyncStorage } from "react-native";
import { persistStore, persistReducer } from "redux-persist";
import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";

import createSagaMiddleware from "redux-saga";
import mySaga from "./sagas";

const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
  key: "root",
  storage: AsyncStorage
};
const persistedReducer = persistReducer(
  persistConfig,
  combineReducers({ themeReducer, tokenReducer })
);

export const store = createStore(
  persistedReducer,
  applyMiddleware(thunk, sagaMiddleware)
);
export const persistor = persistStore(store);

sagaMiddleware.run(mySaga);
