import { STORE_TOKEN } from "./actions";
const initialState = {
  token: ""
};
const tokenReducer = (state = initialState, action) => {
  switch (action.type) {
    case STORE_TOKEN:
      let newState = {
        ...state,
        token: action.userToken
      };
      return newState;
    default:
      return state;
  }
};
export default tokenReducer;
