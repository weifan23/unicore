import React from "react";
import {
  Keyboard,
  View,
  TouchableOpacity,
  Dimensions,
  Platform
} from "react-native";
import { H2, H1 } from "root/src/styles/components";
import styled from "styled-components/native";
import { Feather } from "@expo/vector-icons";
import { useSelector, useDispatch } from "react-redux";

import { darkTheme, lightTheme } from "root/src/styles/theme";
import { switchTheme } from "root/src/redux/actions";

const Bar = styled.View`
  justify-content: space-between;
  flex-direction: row;
  align-items: center;
`;

const BarButtons = styled.View`
  width: 80px;
  height: 50px;
  padding-horizontal: 20px;
  justify-content: center;
`;
export default header = ({ title, nav, styles, back }) => {
  const theme = useSelector(state => state.themeReducer.theme);
  const dispatch = useDispatch();
  const onPressMenu = isBack => {
    if (isBack) {
      nav.goBack();
    } else {
      Keyboard.dismiss();
      nav.openDrawer();
    }
  };

  return (
    <Bar style={[{ paddingTop: Platform.OS === "ios" ? 0 : 10 }, styles]}>
      <TouchableOpacity onPress={() => onPressMenu(back)}>
        <BarButtons style={{ alignItems: "flex-start" }}>
          <Feather
            name={back ? "chevron-left" : "menu"}
            size={25}
            color={theme.mode === "light" ? "#414853" : "#F9FBFF"}
          />
        </BarButtons>
      </TouchableOpacity>

      <View style={{ maxWidth: Dimensions.get("window").width * 0.6 }}>
        <H2>{title}</H2>
      </View>

      <TouchableOpacity onPress={() => nav.navigate("Notifications")}>
        <BarButtons style={{ alignItems: "flex-end" }}>
          <Feather
            name={theme.mode === "light" ? "bell" : "bell"}
            size={25}
            color={theme.mode === "light" ? "#414853" : "#F9FBFF"}
          />
        </BarButtons>
      </TouchableOpacity>
    </Bar>
  );
};
