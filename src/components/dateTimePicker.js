import React, { useState, useEffect } from "react";
import { Platform, View } from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import Modal from "react-native-modal";
import styled from "styled-components/native";

import { Button, ButtonGhost } from "root/src/styles/components";

const TimeDatePicker = ({ value, mode, show, onChange, cancel }) => {
  return (
    <PickerModal
      show={show}
      cancel={Platform.OS === "ios" ? () => cancel() : cancel()}
    >
      {show && (
        <DateTimePicker
          value={value ? value : new Date()}
          mode={mode}
          is24Hour={true}
          display="spinner"
          onChange={onChange}
        />
      )}
    </PickerModal>
  );
};

const PickerModal = ({ show, children, cancel }) => {
  if (Platform.OS === "ios") {
    return (
      <Modal
        animationIn="slideInUp"
        animationOut="slideOutDown"
        // onBackdropPress={cancelPicker}
        isVisible={show}
        onBackdropPress={() => cancel()}
      >
        <DateTimeModal>
          {children}

          <View style={{ alignItems: "center" }}>
            <Button width={1 / 2} onPress={() => cancel()}>
              Done
            </Button>
            <ButtonGhost onPress={() => cancel()} width={1 / 2}>
              Cancel
            </ButtonGhost>
          </View>
        </DateTimeModal>
      </Modal>
    );
  } else {
    return children;
  }
};

export default TimeDatePicker;

const DateTimeModal = styled.View`
  padding: 10px;
  border-radius: 15px;
  background-color: ${props => props.theme.PRIMARY_BACKGROUND_COLOR};
`;
