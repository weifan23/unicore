import axios from "axios";
const API_URL = "http://unicore-api-test.misakamikoto.faith/api";
export const api = {
  register() {},
  getUser() {}
};

export const registerUser = async (email, name, password) => {
  const path = "/register";
  console.log("" + email + name + password);

  try {
    const res = await axios.post(
      API_URL + path,
      `email=${email}&password=${password}&name=${name}`
    );
    console.log(JSON.stringify(res));

    return [res, true];
  } catch (error) {
    console.log(JSON.stringify(error));
    return [error, false];
  }
};

export const loginUser = async (email, password) => {
  const path = "/login";
  try {
    const res = await axios.post(
      API_URL + path,
      `email=${email}&password=${password}`
    );
    return [res, true];
  } catch (error) {
    return [error, false];
  }
};
